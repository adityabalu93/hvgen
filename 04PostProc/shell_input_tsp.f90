!--------------------------------------------------------------------
! program to read in t-spline mesh from M.A. Scott
!--------------------------------------------------------------------
subroutine shell_input_tsp(NSD, NPATCH, mTSP, mBEZ, TSP, BEZ, maxP, maxQ)

  use types_shell
  implicit none

  type(mesh_mp), intent(in) :: mTSP, mBEZ
  type(mesh), intent(inout) :: TSP, BEZ

  integer, intent(in) :: NSD, NPATCH, maxP, maxQ

  integer :: ier, i, j, ip, p, q, mcp, ncp, nnode, nel, nshl, eloc, eglob, nbor
  integer :: pb, qb, nshlb, tnel
  ! IEN matches element number a local node number with
  ! patch node number
  integer, allocatable :: IEN_SH(:,:)

  ! INN relate global node number to the (i,j) "NURBS coordinates"
  integer, allocatable :: INN_SH(:,:)

  integer :: offset

  allocate(TSP%P(TSP%NEL), TSP%Q(TSP%NEL), TSP%NSHL(TSP%NEL), &
           TSP%IEN(TSP%NEL,TSP%maxNSHL), TSP%INN(TSP%NEL,2), &
           TSP%elementPatchNumbers(TSP%NEL),&
           TSP%elementNeighbors(TSP%nel,8),&
           TSP%PTYPE(TSP%NEL),&
           stat=ier)

  if (ier /= 0) stop 'Allocation Error: TSP%IEN'
  
  allocate(BEZ%EXT(TSP%NEL, TSP%maxNSHL, TSP%maxNSHL), &
           BEZ%P(TSP%NEL), BEZ%Q(TSP%NEL), BEZ%NSHL(TSP%NEL), &
           stat=ier)
  if (ier /= 0) stop 'Allocation Error: BEZ%EXC'
  
  TSP%P = 0; TSP%Q = 0; TSP%NSHL = 0
  TSP%IEN = 0
  TSP%elementNeighbors = 0
  TSP%PTYPE = 0
    
  BEZ%EXT = 0.0
  BEZ%P = 0
  BEZ%Q = 0
  BEZ%NSHL = 0  
  
  eglob = 0
  offset = 0
   
  do ip = NPATCH, 1, -1

    p     = mTSP%P(ip)
    q     = mTSP%Q(ip)
    nnode = mTSP%NNODE(ip)  ! number of local nodes
    nel   = mTSP%NEL(ip)    ! number of local elements

!     allocate(IEN_SH(nel,nshl), stat=ier)
!     if (ier /= 0) stop 'Allocation Error: INN_SH'
!     IEN_SH = 0
  
    ! generate IEN and Coordinates
!     call genIEN_INN_shell(p, q, nshl, nnode, nel, mcp, ncp, &
!                           INN_SH, IEN_SH)
    do eloc = 1, nel
      ! NSHL for local element
      nshl = mTSP%NSHL(ip, eloc)
      
      ! BEZ definition for each element
      pb    = mBEZ%Pb(ip, eloc)
      qb    = mBEZ%Qb(ip, eloc)      
      nshlb = mBEZ%NSHL(ip, eloc)
      !  global element number
      eglob = eglob + 1      
      
      ! TSP%IEN and IEN_SH can have different numbers of shape functions
      ! so it is necessary to indicate 1:nshl
      TSP%IEN(eglob,1:nshl) = mTSP%MAP(ip, mTSP%IEN(ip, eloc,1:nshl))

      ! BEZ Extraction for global element
      BEZ%EXT(eglob,1:nshl, 1:mBEZ%NSHL(ip, eloc)) = mBEZ%EXT(ip, &
                                 eloc,1:nshl, 1:mBEZ%NSHL(ip, eloc))
                                 
!       BEZ%P(eglob)= mBEZ%Pb(ip, eloc)
!       BEZ%Q(eglob)= mBEZ%Qb(ip, eloc)
!       
!       BEZ%NSHL(eglob)= mBEZ%Qb(ip, eloc)
      ! store the patch number of each global element
      TSP%PTYPE(eglob)=mTSP%TPTYPE(ip,eloc)
      TSP%elementPatchNumbers(eglob) = TSP%PTYPE(eglob)
      
      ! build the global elements data 
      TSP%P(eglob)    = p
      TSP%Q(eglob)    = q
      TSP%NSHL(eglob) = nshl
      
      BEZ%P(eglob)    = pb
      BEZ%Q(eglob)    = qb
      BEZ%NSHL(eglob) = nshlb
!       ! offset extant neighbors and add to global array
!       do i=1,8
!          nbor = mTSP%elementNeighbors(ip,eloc,i)
!          if(nbor > 0) then
!             TSP%elementNeighbors(eglob,i) = nbor + offset
!          endif
!       enddo

    end do

    offset = offset + nel

!     deallocate(IEN_SH)  
  end do ! End loop over patches
  
  allocate(TSP%B_NET(  TSP%NNODE,NSD+1), &
           TSP%B_NET_U(TSP%NNODE,NSD+1), &
           TSP%B_NET_D(TSP%NNODE,NSD+1), &
           TSP%IBC(    TSP%NNODE,NSD))
  TSP%B_NET   = 0.0d0    ! reference config
  TSP%B_NET_U = 0.0d0    ! Undeformed (used in pre-bend)
  TSP%B_NET_D = 0.0d0    ! current config (deformed)
  TSP%IBC     = 0

  ! build the reduced node information
  do ip = 1, NPATCH

    do i = 1, mTSP%NNODE(ip)
      TSP%B_NET(mTSP%MAP(ip,i),:) = mTSP%B_NET(ip,i,:)
      TSP%IBC(  mTSP%MAP(ip,i),:) = mTSP%IBC(  ip,i,:)
    end do

  end do

  TSP%B_NET_U = TSP%B_NET
  TSP%B_NET_D = TSP%B_NET
  
  ! Build nIBC
  TSP%nIBC    = 0
  do ip = 1, NPATCH
    TSP%nIBC = mTSP%nIBC(ip)+TSP%nIBC
  end do
  
!   if (myid == mpi_master) then
    write(*,*) "Maximum number of boundary points",TSP%nIBC
  ! end if
  allocate(TSP%boundary(TSP%nIBC))
  TSP%boundary = 0
  
  TSP%nIBC    = 0
  do i = 1, TSP%NNODE
    if (TSP%IBC (i,1) /= 0) then
        TSP%nIBC = TSP%nIBC + 1
    end if
  end do
!   do ip = 1, NPATCH
!     do i = 1, mTSP%nIBC(ip)
!       j = j + 1
!       TSP%boundary(j) = mTSP%MAP(ip,mTSP%boundary(ip,i))
!     end do
!   end do  

!   if (myid == mpi_master) then
    write(*,*) "Total Number of boundary points",TSP%nIBC
 
    tnel = 0
    if (eglob == TSP%NEL) then
      do i = 1, TSP%NEL
        if (TSP%PTYPE(i) == 0) then
          tnel = tnel + 1
        end if
      end do
      if (tnel == 0) then
        write(*,*) "All Elements ptype are defined in TSP"
      else
        write(*,*) "Number of Elements ptype not defined:", tnel
        stop
      end if
    else
      write(*,*) "Not All elements ptype are defined "
      stop
    end if
!   end if
end subroutine shell_input_tsp
