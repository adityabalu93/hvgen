!======================================================================
! subroutine to read in the parameters defined in 'param.dat'          
!======================================================================
subroutine getparam(SUR)
  
  use types_shell
  use commonvar_shell
  
  implicit none

  type(mesh), intent(inout) :: SUR
  
  call rread("Thickness_Shell", SUR%Thickness_Shell)
 
  call rread("E_Shell", SUR%E_Shell)
  call rread("nu_Shell", SUR%nu_Shell)
  
  call rread("Thickness_Stent", SUR%Thickness_Stent)
 
  call rread("E_Stent", SUR%E_Stent)
  call rread("nu_Stent", SUR%nu_Stent)
  call iread("NBC_set", NBC_set)
     
end subroutine getparam
  


!======================================================================
!
!======================================================================
subroutine rread(vname, val)
  implicit none

  character(len=*), intent(in)  :: vname
  real(8),          intent(out) :: val
  character(len=20) :: buf
  
  call cread(vname, buf)
  read(buf,*) val

  write(*,"(a20,x,' = ',x,ES12.4)")  vname, val
end subroutine rread



!======================================================================
!
!======================================================================
subroutine iread(vname, val)
  implicit none

  character(len=*), intent(in)  :: vname
  integer,          intent(out) :: val
  character(len=20) :: buf
  
  call cread(vname, buf)
  read(buf,*) val
  
  write(*,"(a20,x,' = ',x,I12)")  vname, val  
end subroutine iread
  


!======================================================================
!
!======================================================================
subroutine iread3(vname, val)

  implicit none

  character(len=*), intent(in)  :: vname
  integer,          intent(out) :: val(3)
  character(len=20) :: buf
  
  call cread(vname, buf)
  read(buf,*) val(1:3)
  
  write(*,"(a20,x,' = ',x,3I4)")  vname, val
end subroutine iread3




!======================================================================
! subroutine to read in a variable name, check with input file
! and read in the corresponding value.
!======================================================================
subroutine cread(vname, val)

  implicit none

  character(len=*),  intent(in)  :: vname
  character(len=20), intent(out) :: val
  character(len=40) :: buf, buf1
  integer :: paramf, ios, found, pos
  
  found  = 0
  paramf = 45
  open(paramf, file='param.dat', status='old', iostat=ios)
  
  do while ((ios == 0).and.(found == 0))
    read(paramf,'(A)',iostat=ios) buf
	 
    ! find the position of character "="
    pos  = scan(buf,'=')

    ! string before "=" will be checked with input
    buf1 = buf(1:pos-1)

    ! string after "=" is the output value
    val  = buf(pos+1:)
	   
    if (buf1 == vname) found = 1
  end do

  close(paramf)

  if (found == 0) then
    write(*,*) "Could not find ", vname , " in param.dat"
    stop	
  end if        
end subroutine cread
