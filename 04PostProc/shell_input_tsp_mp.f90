!--------------------------------------------------------------------
! program to extract the shell T-mesh
!--------------------------------------------------------------------
subroutine shell_input_tsp_mp(NP, NSD, maxP, maxQ, &
                              maxNNODE, maxNSHL, maxNEL, mTSP, mBEZ)

  use types_shell
  use commonvar_shell
  implicit none

  type(mesh_mp), intent(out) :: mTSP, mBEZ

  integer, intent(in)  :: NP, NSD
  integer, intent(out) :: maxP, maxQ, maxNNODE, maxNSHL, maxNEL

  integer :: i, j, k, l, mf, ip, ier, tmp, ct, nelu, nelv, Reason, pos, &
       iel, niel, nborindex, ptype, mp_nel, total_nel, tnel, nparts, iNSHL

  character(len=30) :: fname, cname, tname, tname2, tname3, partname
  
  integer, allocatable :: Npats(:), tempArray(:)

  allocate(mTSP%P(NP),     mTSP%Q(NP), &
           mTSP%MCP(NP), &
           mTSP%NNODE(NP), mTSP%NEL(NP), Npats(NP))

! The degree of the T-spline basis functions in each 
! parametric direction. (not used in the code)
  mTSP%P     = 3
  mTSP%Q     = 3
  
  ! MCP is used for calculating the maxNSHL
  mTSP%MCP   = 0

  mTSP%NNODE = 0
  mTSP%NEL   = 0
  
  ! first loop through all the patches to find the max number of
  ! parameter. This will be used for allocating other arrays.
  do ip = 1, NP

    mf = 11

    ! Read in preliminary information
    write(cname,'(I8)') ip
    fname = 'tmesh.'//trim(adjustl(cname))//'.iga'
    open(mf, file=fname, status='old')
    ! number of spatial dimensions. Usually NSD = 3
    read(mf,*)   
    ! The number of global T-spline basis functions or control
    ! points in the T-mesh.
    read(mf,*) tname, mTSP%NNODE(ip)

    ! The number of bezier elements which constitute the T-spline.
    read(mf,*) tname, mTSP%NEL(ip)

    ! These two entries are added by me for allocating arrays
    ! The maximum number of global T-spline basis functions which 
    ! are non-zero over eacg element
    !read(mf,*) tname, mTSP%MCP(ip)
	
	! skip nodes
    do i = 1, mTSP%NNODE(ip)
      read(mf,*) 
    end do 
	
	mTSP%MCP(ip) = 0
    do iel = 1, mTSP%NEL(ip)
		
	  do
       	 read(mf,*) tname, tname2
		 if (tname == 'belem') then
			 read(tname2,*) iNSHL
			 !write(*,*) tname2, iNSHL
			 if ( mTSP%MCP(ip) < iNSHL ) then
				 mTSP%MCP(ip) = iNSHL
			 end if
			 exit
		 end if
	  end do
      
    end do
	
	write(*,*) fname, ": maxNSHL = ", mTSP%MCP(ip)
	
	Npats(ip) = 0
	do 
		read(mf,*, IOSTAT=Reason) tname
		if (Reason == 0) then
			if (tname == 'set') then
				Npats(ip) = Npats(ip) + 1
			end if
		else
			exit
		end if
	end do
	
	write(*,*) fname, ": N Sets = ", Npats(ip)

    close(mf)
  end do ! end loop patches

  
  ! these maximum values incluid bending stripes
  maxP     = maxval(mTSP%P)
  maxQ     = maxval(mTSP%Q)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!need to change
  maxNSHL   = maxval(mTSP%MCP)

  
  
  maxNNODE = maxval(mTSP%NNODE)
  maxNEL = maxval(mTSP%NEL)
  if (maxNSHL  <= (maxP+1)*(maxQ+1)) then 
    maxNSHL =(maxP+1)*(maxQ+1)
  end if

  ! Allocate arrays for control net
  allocate(mTSP%B_NET(NP,maxNNODE,NSD+1))
  allocate(mTSP%IBC(  NP,maxNNODE,NSD))
  allocate(mTSP%IEN(  NP,maxNEL,maxNSHL))
  allocate(mTSP%NSHL(  NP,maxNEL))
  allocate(mTSP%NIBC(  NP))
  allocate(mTSP%boundary( NP, maxNNODE))
  allocate(mTSP%bound_elm( NP, maxNEL))
  allocate(mTSP%TPTYPE(NP, maxNEL))
  
  
  if (maxNNODE > maxNEL) then
	  allocate(tempArray(maxNNODE))
  else
	  allocate(tempArray(maxNEL))
  end if
  
  mTSP%B_NET  = 0.0d0
  mTSP%IBC    = 0
  mTSP%IEN    = 0
  mTSP%NSHL   = 0
  mTSP%nIBC   = 0 
  mTSP%boundary = 0
  mTSP%bound_elm = 0
  mTSP%TPTYPE  = 0
            
  allocate(mBEZ%Pb(NP, maxNEL), mBEZ%Qb(NP,maxNEL), &
           mBEZ%NSHL(NP, maxNEL), &
           mBEZ%EXT(NP, maxNEL, maxNSHL, maxNSHL), stat=ier)
  if (ier /= 0) stop 'Allocation Error: TSP%IEN'
  mBEZ%Pb = 0; mBEZ%Qb = 0; mBEZ%NSHL = 0
  mBEZ%EXT = 0.0d0
  
  ! now loop through the patches again to read in the rest of 
  ! the information
  do ip = 1, NP
    mf = 12  
    
    ! Read in preliminary information
    write(cname,'(I8)') ip
    fname = 'tmesh.'//trim(adjustl(cname))//'.iga'
    open(mf, file=fname, status='old') 
    ! skip "3" lines
	do i = 1, 3
        read(mf,*)  
    end do
    ! skip "4" lines
    ! do i = 1, 4
!         read(mf,*)
!     end do
    
    ! The control point (x, y, z, w) associated with each global 
    ! T-spline basis function in the T-mesh. The ith T-spline  denoted
    ! control point is by the token "gi". NOTE: These control points
    ! are NOT in homogeneous form i.e in homogeneous form the control
    ! points have the form (xw, yw, zw, w).
    do i = 1, mTSP%NNODE(ip)
      read(mf,*) tname, (mTSP%B_NET(ip, i, j), j = 1, NSD+1)
    end do  
    
    do iel = 1, mTSP%NEL(ip)

      ! The first two integers specify the degree of the bezier 
      ! element in s followed by the degree in t. The third number 
      ! denotes the number of global T-spline basis functions which 
      ! are non-zero over this element. Note that this number often
      ! varies from element to element in a T-spline. 
      ! This is why we need to find maxNSHL_TS in advanced for 
      ! allocating arrays.
      read(mf,*) tname, mTSP%NSHL(ip, iel), mBEZ%Pb(ip, iel), mBEZ%Qb(ip, iel)

      mBEZ%NSHL(ip, iel) = (mBEZ%Pb(ip, iel)+1)*(mBEZ%Qb(ip, iel)+1)

      ! For each global T-spline basis function which is non-zero
      ! over this element the global index of the basis function 
      ! is listed.
      read(mf,*) (mTSP%IEN(ip,iel,j), j = 1, mTSP%NSHL(ip,iel))
      mTSP%IEN(ip,iel,1:mTSP%NSHL(ip, iel)) = mTSP%IEN(ip,iel,1:mTSP%NSHL(ip,iel))+1
      
	  !write(*,*) tname, iel, mTSP%NSHL(ip,iel)
	  
      do j = 1, mTSP%NSHL(ip,iel)
        read(mf,*) (mBEZ%Ext(ip,iel,j,k), k = 1, mBEZ%NSHL(ip, iel))
      end do
      
    end do

	
	nparts = Npats(ip)
	
	total_nel = 0
	
	if (nparts > 0) then
		
		do i = 1, nparts
	        ! read element type flag and give settings.
	        ! TPTYPE is for part
	        read(mf,*) tname, mp_nel, tname2, tname3, (tempArray(j), j = 1, mp_nel)
		
		    ! find the position of character "_"
		    pos  = scan(tname3,'_')

		    ! string before "_" will be checked with input
			
			! if "_" was not found
			if (pos == 0) then
				tname = tname3 
			else
		    	tname = tname3(1:pos-1)
			end if
		
			if (tname == 'fixed') then
				mTSP%nIBC(ip) = mp_nel
				!write(*,*) tname3, tname, mTSP%nIBC(ip)
			    !read(mf,*) (mTSP%boundary(ip,j), j = 1,mTSP%nIBC(ip))
				mTSP%boundary(ip,1:mTSP%nIBC(ip)) = tempArray(1:mTSP%nIBC(ip))
			    mTSP%boundary(ip,:)=mTSP%boundary(ip,:)+1
			else
				read(tname,*) ptype
		        !read(mf,*) (mTSP%bound_elm(ip,j+total_nel), j = 1, mp_nel)
				!mTSP%bound_elm(ip,(1:mp_nel)+total_nel) = tempArray(1:mp_nel)
		        do j = 1, mp_nel
				  mTSP%bound_elm(ip, j+total_nel) = tempArray(j)
		          mTSP%bound_elm(ip,j+total_nel) = mTSP%bound_elm(ip,j+total_nel) + 1
		          mTSP%TPTYPE(ip, mTSP%bound_elm(ip,j+total_nel)) = ptype
		        end do
		        total_nel = total_nel + mp_nel
			end if
		end do
		
        !tnel is a flag to see how many elements are not defined.
        tnel = 0
        if (total_nel == mTSP%NEL(ip)) then
          do i = 1, mTSP%NEL(ip)
            if (mTSP%TPTYPE(ip, i) == 0) then
              tnel = tnel + 1
            end if
          end do
          if (tnel == 0) then
            write(*,*) "All Elements ptype are defined in ip:", ip
          else
            write(*,*) "Number of Elements ptype not defined:", tnel
            stop
          end if
        else
          write(*,*) "Not All elements ptype are defined in ip:", ip
          write(*,*) "Only defined", total_nel, "from", mTSP%NEL(ip)
          !stop
        end if
			
	end if

	!if (ip == 2) then
		do i = 1, mTSP%nIBC(ip)
	   	 	mTSP%IBC(ip,mTSP%boundary(ip,i),:) = 1
		end do
	!end if

    close(mf)
  end do ! end loop patches
  
  ! remove duplcate nodes
  ! the reduced node count that counts only master nodes 
  ! in file "input_mp"
  allocate(mTSP%MAP(NP,maxNNODE), stat=ier)
  if (ier /= 0) stop 'Allocation Error: mNRB%MAP'
  mTSP%MAP = -1
  
  print *, "============================================="
  
  deallocate(Npats)
  deallocate(tempArray)
  
end subroutine shell_input_tsp_mp
