      subroutine IntElmAss_shell_str(NRB, nsd, NVAR, RHSG_SH, mg_SH)

      use types_shell
      implicit none

      type(mesh), intent(in) :: NRB

      integer, intent(in) :: nsd, NVAR

      real(8), intent(out) :: RHSG_SH(NRB%NNODE,NVAR),
     &                        mg_SH(NRB%NNODE)

      !  Local variables
      integer :: p, q, nshl, nuk, nvk, ptype, kply, ct
      integer :: iel, igauss, jgauss, i, j, ii, jj, kk
      integer :: ni, nj

      real(8) :: gp(NRB%NGAUSS), gw(NRB%NGAUSS), gwt, da, DetJb_SH, 
     &           zeta, nor(NSD), thi, str(NVAR), xu(nsd), xd(NSD), 
     &           dxdxi(NSD,2), ddxddxi(nsd,3)

      integer, allocatable :: lIEN(:)
      real(8), allocatable :: shl(:), shgradl(:,:), shhessl(:,:),
     &                        Rhs(:,:), lmass(:)

      gp = 0.0d0
      gw = 0.0d0

      ! get Gaussian points and weights     
      call genGPandGW_shell(gp, gw, NRB%NGAUSS) 

      ! loop over elements
      do iel = 1, NRB%NEL
        ! get NURB coordinates
        ni = NRB%INN(iel,1); nj = NRB%INN(iel,2)        

        ! Check to see if current element has nonzero area, skip if it doesn't
        ! also check if this element is bending strip element, skip it.
        if ( (NRB%U_KNOT(iel,ni) /= NRB%U_KNOT(iel,ni+1)) .and.
     &       (NRB%V_KNOT(iel,nj) /= NRB%V_KNOT(iel,nj+1)) .and. 
     &       NRB%PTYPE(iel) > 0 ) then
                     
          ! used in calculating quadrature points. The factor of 4.0d0
          ! comes from mapping from the [-1,1] line onto a real segment...
          da = (NRB%U_KNOT(iel,ni+1)-NRB%U_KNOT(iel,ni))*
     &         (NRB%V_KNOT(iel,nj+1)-NRB%V_KNOT(iel,nj))/4.0d0

          p = NRB%P(iel); nuk = NRB%NUK(iel); nshl = NRB%NSHL(iel)
          q = NRB%Q(iel); nvk = NRB%NVK(iel); ptype = NRB%PTYPE(iel)

          allocate(shl(nshl), shgradl(nshl,2), shhessl(nshl,3),
     &             Rhs(NVAR,nshl), lIEN(nshl), lmass(nshl))

          lIEN = -1
          do i = 1, nshl
            lIEN(i) = NRB%IEN(iel,i)
          end do


            ! initialize local load vector 
            Rhs = 0.0d0          
            ct  = 0
            lmass    = 0.0d0
			
            ! Loop over integration points (NGAUSS in each direction) 
            do igauss = 1, NRB%NGAUSS
              do jgauss = 1, NRB%NGAUSS
                
                ct = ct + 1

                ! Get Element Shape functions and their gradients
                shl = 0.0d0; shgradl = 0.0d0; shhessl = 0.0d0
                xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0; ddxddxi = 0.0d0
                nor = 0.0d0; str = 0.0d0

                call eval_SHAPE_shell(gp(igauss), gp(jgauss), 
     &                                shl, shgradl, shhessl, nor, 
     &                                xu, xd, dxdxi, ddxddxi, 
     &                                p, q, nsd, nshl,
     &                                NRB%IEN(iel,1:nshl), NRB%NNODE,
     &                                NRB%B_NET_U, NRB%B_NET_D, 
     &                                DetJb_SH, ni, nj, nuk, nvk,
     &                                NRB%U_KNOT(iel,1:nuk),
     &                                NRB%V_KNOT(iel,1:nvk))

                gwt = gw(igauss)*gw(jgauss)*da

                thi = NRB%Thickness
                         
                !======================================================
                ! evaluate the stress and resultant force
                !======================================================
                ! stress (bottom)
                zeta = -0.5d0*thi
                call stress_KL(shgradl, shhessl, nshl, q, nsd, nor, 
     &                         NRB%B_NET, NRB%B_NET_D, lIEN, 
     &                         NRB%NNODE, zeta, str(1:3))

                ! stress (top)
                zeta = 0.5d0*thi
                call stress_KL(shgradl, shhessl, nshl, q, nsd, nor,
     &                         NRB%B_NET, NRB%B_NET_D, lIEN, 
     &                         NRB%NNODE, zeta, str(4:6))

                !=====================================================

                ! L2-projection
                do ii = 1, nshl
                  Rhs(:,ii) = Rhs(:,ii) + str(:)*shl(ii)*DetJb_SH*gwt
                  lmass(ii) = lmass(ii) + shl(ii)*DetJb_SH*gwt
				  
                end do

              end do
            end do  ! end loop gauss points



            do ii = 1, NSHL
              ! stress
              RHSG_SH(lIEN(ii),:) = RHSG_SH(lIEN(ii),:) + Rhs(:,ii)
			  
              mg_SH(lIEN(ii)) = mg_SH(lIEN(ii)) + lmass(ii)
			  
            end do



          deallocate(shl, shgradl, shhessl, Rhs, lIEN, lmass)

        end if  ! end if nonzero areas elements

      end do    ! end loop elements
      
      end subroutine IntElmAss_shell_str


