      subroutine stress_res_KL(shgradl, shhessl, Dm, Dc, Db,
     &                         nshl, q, NSD, nor, B_NET_SH, B_NET_SH_D,
     &                         lIEN, nnode, thi, nm_out)

      implicit none      

      integer, intent(in) :: nshl, q, NSD,
     &                       lIEN(nshl), nnode
      real(8), intent(in) :: thi, shgradl(NSHL,2), shhessl(NSHL,3),
     &                       Dm(3,3), Dc(3,3), Db(3,3), nor(3),
     &                       B_NET_SH(nnode,nsd+1),
     &                       B_NET_SH_D(nnode,nsd+1)
      real(8), intent(out):: nm_out(6)

      real(8) :: dR(NSHL,2), ddR(NSHL,3)
      real(8) :: Gab_r(2,2), Bv_r(3), T_Gcon_E(3,3), T_E_G(3,3), dA_r
      real(8) :: gab(2,2), gab_con(2,2), bv(3), T_g_e(3,3), da
      real(8) :: E_cu(3), E_ca(3), K_cu(3), K_ca(3)
      real(8) :: detJk
      real(8) :: n_pk2_ca(3), n_pk2(3), n_cau(3), n_g(5), n_e(5)
      real(8) :: m_pk2_ca(3), m_pk2(3), m_cau(3), m_g(5), m_e(5)
      real(8) :: tmp33(3,3), tmp32(3,2), tmp3(3), tmp22(2,2)

      integer :: i

      do i = 1, NSHL
        dR(i,:)  = shgradl(i,:)
        ddR(i,1) = shhessl(i,1)
        ddR(i,2) = shhessl(i,3)
        ddR(i,3) = shhessl(i,2)
      end do
      
      ! reference configuration
      call shell_geo(nshl, nsd, nnode, q, dR, ddR, lIEN, B_NET_SH, nor,
     &               tmp32, tmp3, dA_r, tmp3, Gab_r, tmp22, tmp33, 
     &               Bv_r, T_Gcon_E, T_E_G, tmp33)

      ! actual configuration
      call shell_geo(nshl, nsd, nnode, q, dR, ddR, lIEN, B_NET_SH_D, 
     &               nor, tmp32, tmp3, da, tmp3, gab, gab_con, tmp33, 
     &               bv, tmp33, tmp33, T_g_e)

      detJk = da/dA_r

      ! strain vector [E11,E22,E12] referred to curvilinear coor sys
      E_cu(1) = 0.5d0*(gab(1,1)-Gab_r(1,1))
      E_cu(2) = 0.5d0*(gab(2,2)-Gab_r(2,2))
      E_cu(3) = 0.5d0*(gab(1,2)-Gab_r(1,2))

      ! curvature vector [K11,K22,K12] referred to curvilinear coor sys
      K_cu = -(bv-Bv_r)

      ! strain coeff. in local cartesian coord. E_ca=[E11 E22 2E12]
      E_ca = matmul(T_Gcon_E,E_cu)

      ! curvature coeff. in local cartesian coord. K_ca=[K11 K22 2K12]
      K_ca = matmul(T_Gcon_E,K_cu)

      ! PK2 normal force in local cartesian coord. E1,E2
      n_pk2_ca = matmul(Dm,E_ca) + matmul(Dc,K_ca)

      ! PK2 moment in local cartesian E1,E2
      m_pk2_ca = matmul(Dc,E_ca) + matmul(Db,K_ca)

      ! PK2 normal force in G1,G2
      n_pk2 = matmul(T_E_G,n_pk2_ca)

      ! PK2 moment in G1,G2
      m_pk2 = matmul(T_E_G,m_pk2_ca)

      ! Cauchy normal force in g1,g2
      n_cau(:) = 1.0d0/detJk*n_pk2(:)

      ! Cauchy moment in g1,g2
      m_cau(:) = 1.0d0/detJk*m_pk2(:)

      !---------------------------------------------------
      ! resultant force
      !---------------------------------------------------
      n_g = 0.0d0
      ! Cauchy normal force in normalized g1,g2
      n_g(1) = sqrt(gab(1,1)/gab_con(1,1))*n_cau(1)
      n_g(2) = sqrt(gab(2,2)/gab_con(2,2))*n_cau(2)
      n_g(3) = sqrt(gab(1,1)/gab_con(2,2))*n_cau(3)
      ! principal normal forces in normalized g1,g2
      n_g(4)=0.5d0*(n_g(1)+n_g(2)
     &              +sqrt((n_g(1)-n_g(2))**2+4.0d0*n_g(3)**2))
      n_g(5)=0.5d0*(n_g(1)+n_g(2)
     &              -sqrt((n_g(1)-n_g(2))**2+4.0d0*n_g(3)**2))

      n_e = 0.0d0
      ! Cauchy normal force in local cartesian e1,e2
      n_e(1:3) = matmul(T_g_e,n_cau)
      ! principal normal forces in local cartesian e1,e2
      n_e(4)=0.5d0*(n_e(1)+n_e(2)
     &              +sqrt((n_e(1)-n_e(2))**2+4.0d0*n_e(3)**2))
      n_e(5)=0.5d0*(n_e(1)+n_e(2)
     &              -sqrt((n_e(1)-n_e(2))**2+4.0d0*n_e(3)**2))


      !---------------------------------------------------
      ! resultant moment
      !---------------------------------------------------
      m_g = 0.0d0
      ! Cauchy moment in normalized g1,g2
      m_g(1) = sqrt(gab(1,1)/gab_con(1,1))*m_cau(1)
      m_g(2) = sqrt(gab(2,2)/gab_con(2,2))*m_cau(2)
      m_g(3) = sqrt(gab(1,1)/gab_con(2,2))*m_cau(3)
      ! principal moment in normalized g1,g2
      m_g(4)=0.5d0*(m_g(1)+m_g(2)
     &              +sqrt((m_g(1)-m_g(2))**2+4.0d0*m_g(3)**2))
      m_g(5)=0.5d0*(m_g(1)+m_g(2)
     &              -sqrt((m_g(1)-m_g(2))**2+4.0d0*m_g(3)**2))

      m_e = 0.0d0
      ! Cauchy moment in local cartesian e1,e2
      m_e(1:3) = matmul(T_g_e,m_cau)
      ! principal moment
      m_e(4)=0.5d0*(m_e(1)+m_e(2)
     &              +sqrt((m_e(1)-m_e(2))**2+4.0d0*m_e(3)**2))
      m_e(5)=0.5d0*(m_e(1)+m_e(2)
     &              -sqrt((m_e(1)-m_e(2))**2+4.0d0*m_e(3)**2))

      nm_out(1:3) = n_e(1:3)
      nm_out(4:6) = m_e(1:3)
          
      end subroutine stress_res_KL
