% Params = {{1.2, 0.8, -0.5, 0.0, 0.9211, 0.8148, 0.95, 1.35} 
%     {1.2, 0.8, -0.5, 0.0, 0.8809, 0.8, 1.05, 1.5}
%     {1.2, 0.8, -0.5, 0.0, 0.8913, 0.8438, 1.15, 1.6}
%     {1.2, 0.8, -0.5, 0.0, 0.88, 0.8823, 1.25, 1.75}
%     {1.2, 0.8, -0.5, 0.0, 0.8889, 0.8857, 1.35, 1.85}
%     {1.2, 0.8, -0.5, 0.0, 0.8966, 0.8, 1.45, 2.0}};
% Params = {{0.2, 0.2, -0.5 , 0.0, 0.9211, 0.8148, 0.95, 1.35}};

cfdhs = [-0.1, 0.5];
cfdrs = [0.2, 1.0];
cfdzs = [-0.5, -0.1];
cfrfrs = [0.9211, 0.8809, 0.8913, 0.88, 0.8889, 0.8966];
cfhtrs = [0.8148, 0.8, 0.8438, 0.8823, 0.8857, 0.8];
rs = [0.95, 1.05, 1.15, 1.25, 1.35, 1.45];
hts = [1.1, 1.2, 1.35, 1.5, 1.55, 1.6];
pts = 6;
numdesigns = 11;

i = 1;

for p = 1:pts

%     design = rowexch(3,numdesigns,'quadratic','tries',10, 'levels', [4, 4, 3]);
    design = lhsdesign(numdesigns, 3, 'criterion','correlation', 'iterations',10);
    bounds = [cfdhs; cfdrs; cfdzs];
    designparams = zeros(size(design));

    for j = 1:size(design,2) % Convert coded values to real-world units
        zmax = max(design(:,j));
        zmin = min(design(:,j));
        designparams(:,j) = interp1([zmin zmax],bounds(j,:),design(:,j));
    end

    for d = 1:numdesigns
        HV_design(designparams(d,1), designparams(d,2), designparams(d,3), 0.0, cfrfrs(p), cfhtrs(p), rs(p), hts(p), "Designs/"+int2str(i));
        i = i + 1;
    end
end

% for i=1:numparams
%     HV_design(Params{i}{:}, "Design"+int2str(i))
% end

