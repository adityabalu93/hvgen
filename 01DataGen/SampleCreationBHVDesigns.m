numdesigns = 100000;
load(['../designs_',num2str(numdesigns),'.mat'],'designparams');

mkdir("../designs_" + num2str(numdesigns));

module = py.importlib.import_module('setup_params');

% designparams = zeros(size(design));
% for j = 1:size(design,2) % Convert coded values to real-world units
%     zmax = max(design(:,j));
%     zmin = min(design(:,j));
%     designparams(:,j) = interp1([zmin zmax],bounds(j,:),design(:,j));
% end
% 
% designparams(:,8) = 10.^designparams(:,8);
% designparams(:,9) = 1e5.*designparams(:,9);
% designparams(:,10) = 1e4*10.^designparams(:,10);
% designparams(:,11) = 1e2.*designparams(:,11);


for d = 56729:numdesigns
    HV_design(designparams(d,7), designparams(d,6), designparams(d,5),...
        0.0, designparams(d,4), designparams(d,3), designparams(d,1),...
        designparams(d,2), "../designs_" + num2str(numdesigns) + "/" + int2str(d));
    module.setup_param_mat("../designs_" + num2str(numdesigns) + "/" + int2str(d), ...
        designparams(d,8), designparams(d,9), designparams(d,10),...
        designparams(d,11), designparams(d,12));
    close all;
    fclose all;
end
