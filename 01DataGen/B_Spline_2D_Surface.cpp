#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include "mex.h"

#define dim  3 // dimension
#define ndg  2 // 2 dog, U and V
#define pmax 10
#define eps 1.0e-8



// Algorithm A2.1 (page 68)
int FindSpan(int n, int p, double u, double *U)
{
      //if (u == U[n+1])
      //      return n; // Special case
      if (fabs(u - U[n+1]) < eps)
            return n; // Special case
         
      int low  = p;
      int high = n+1; 
      // Do binary search
      int mid = (low + high)/2;
      while (u < U[mid]-eps || u >= U[mid+1]+eps){
            if (u < U[mid]-eps)
                  high = mid;
            else
                  low = mid;
            mid = (low + high)/2;
      }
      return mid;
}

// Algorithm A2.2 (page 70)
void BasisFuns(int i, double u, int p, double* U, double* N)
{
      double *left  = new double [p+1];
      double *right = new double [p+1];
      double saved, temp;

      N[0] = 1.0;
      for (int j = 1; j <= p; j += 1){
            left[j] = u-U[i+1-j];
            right[j] = U[i+j]-u;
            saved = 0.0;
            for (int r = 0; r < j; r += 1){
                  temp = N[r]/(right[r+1] + left[j-r]);
                  N[r] = saved + right[r+1]*temp;
                  saved = left[j-r]*temp;
            }
            N[j] = saved;
      }
      
      delete[] left;
      delete[] right;
}

// Algorithm A2.3 (page 72)
void DersBasisFuns(int i, double u, int p, int n,  double* U, double ders[][pmax+1])
{
//       double *left  = new double [p+1];
//       double *right = new double [p+1];
      double left[pmax];
      double right[pmax];
      double saved, temp;
      double ndu[pmax+1][pmax+1];
      double a[2][pmax+1];
      double d;
      int s1, s2, r, j1, j2, rk, pk, jtmp;
      
      ndu[0][0] = 1.0;
      for (int j = 1; j <= p; j += 1){
            left[j] = u-U[i+1-j];
            right[j] = U[i+j]-u;
            saved = 0.0;
            for (int r = 0; r < j; r += 1){
                  
                  // Lower triangle
                  ndu[j][r] = right[r+1] + left[j-r];
                  temp = ndu[r][j-1]/ndu[j][r];
                  
                  // Upper triangle
                  ndu[r][j] = saved+right[r+1]*temp;
                  saved = left[j-r]*temp;
            }
            ndu[j][j] = saved;
      }
      
      // Load the basis functions
      for (int j = 0; j <= p; j+=1){
            ders[0][j] = ndu[j][p];
      }
      
      // ----- This section computes the derivatives (Eq. [2.9]) -----
      // Loop over function index
      for (int r = 0; r <= p; r += 1){
            // Alternate rows in array "a"
            s1 = 0;
            s2 = 1;
            a[0][0] = 1.0;
            // Loop to compute kth derivative
            for (int k = 1; k <= n; k += 1){
                  d = 0.0;
                  rk = r-k;
                  pk = p-k;
                  if (r >= k){
                        a[s2][0] = a[s1][0]/ndu[pk+1][rk];
                        d = a[s2][0]*ndu[rk][pk];
                  }
                  
                  if (rk >= -1)
                        j1 = 1;
                  else
                        j1 = -rk;
                  if (r-1 <= pk)
                        j2 = k-1;
                  else
                        j2 = p-r;
                  
                  for (int j = j1; j <= j2; j += 1){
                        a[s2][j] = (a[s1][j]-a[s1][j-1])/ndu[pk+1][rk+j];
                        d += a[s2][j]*ndu[rk+j][pk];
                  }
                  if (r <= pk){
                        a[s2][k] = -a[s1][k-1]/ndu[pk+1][r];
                        d += a[s2][k]*ndu[r][pk];
                  }
                  ders[k][r] = d;
                  jtmp = s1;
                  s1 = s2;
                  s2 = jtmp; // Switch rows
            }
      }
      
      // Multiply through by the correct factors (Eq. [2.9])
      r = p;
      for (int k = 1; k <= n; k += 1){
            for (int j = 0; j <= p; j += 1)
                  ders[k][j] *= r;
            r *= (p-k);
      }
      
      
//       delete[] left;
//       delete[] right;
}

// Algorithm A3.6 (page 111)
void SurfaceDerivsAlg1(double *n_in, double *p_in, double* U_in, double *P, double *u_in, int d, double SKL[][pmax][dim])
{
      int n = n_in[0];
      int m = n_in[1];
      int p = p_in[0];
      int q = p_in[1];
      double* U = U_in;
      double* V = U_in + p + n + 2;
      double u = u_in[0];
      double v = u_in[1];
      
      double Nu[pmax+1][pmax+1], Nv[pmax+1][pmax+1];
      double temp[pmax+1];
      int dd;
      
      int du = std::min(d, p);
      for (int k = p+1; k <= d; k += 1)
            for (int l = 0; l <= d-k; l += 1)
                  for (int di = 0; di < dim; di += 1)
                        SKL[k][l][di] = 0.0;
      
      int dv = std::min(d, q);
      for (int l = q+1; l <= d; l += 1)
            for (int k = 0; k <= d-l; k += 1)
                  for (int di = 0; di < dim; di += 1)
                        SKL[k][l][di] = 0.0;
      
      int uspan = FindSpan(n, p, u, U);
      DersBasisFuns(uspan, u, p, du, U, Nu);
      int vspan = FindSpan(m, q, v, V);
      DersBasisFuns(vspan, v, q, dv, V, Nv);
      
      for (int di = 0; di < dim; di += 1){
            for (int k = 0; k <= du; k += 1){
                  for (int s = 0; s <= q; s += 1){
                        temp[s] = 0.0;
                        for (int r = 0; r <= p; r += 1){
                              temp[s] += Nu[k][r]*P[((uspan-p+r)*(m+1)+(vspan-q+s))*dim + di];
                        }
                        dd = std::min(d-k,dv);
                        for (int l = 0; l <= dd; l += 1){
                              SKL[k][l][di] = 0.0;
                              for (int s = 0; s <= q; s += 1){
                                    SKL[k][l][di] += Nv[l][s]*temp[s];
                              }
                        }
                  }
            }
      }
      
}

// Algorithm A3.1 (page 82)
void CurvePoint(double *n, double *p, double* U, double *P, double *u, double *C)
{

      
      int m = (int)p[0] + (int)n[0] + 1;
      
      double *Nu = new double [(int)p[0]+1];
      int uspan = FindSpan((int)n[0], (int)p[0], u[0], U);
      BasisFuns(uspan, u[0], (int)p[0], U, Nu);
      
      
      double *Nv = new double [(int)p[1]+1];
      int vspan = FindSpan((int)n[1], (int)p[1], u[1], U+m+1);
      BasisFuns(vspan, u[1], (int)p[1], U+m+1, Nv);
      
      
      int uind = uspan - (int)p[0];
      int vind;
      double temp;
      for (int d = 0; d < dim; d += 1){
            C[d] = 0;
            
            for (int l = 0; l <= (int)p[1]; l += 1){
                  temp = 0.0;
                  vind = vspan-(int)p[1]+l;
                  for (int k = 0; k <= (int)p[0]; k += 1){
                        temp += Nu[k]*P[ ((uind+k)*((int)n[1]+1) + vind)*dim + d ];
                  }
                  C[d] += Nv[l]*temp;
            }
            
      }
      
      delete[] Nu;
      delete[] Nv;
      
}


// Main funciton to call PointOnBezierCurve
void mexFunction(
		 int          nlhs,
		 mxArray      *plhs[],
		 int          nrhs,
		 const mxArray *prhs[]
		 )
{

  if (nrhs != 5) {
      mexErrMsgIdAndTxt("MATLAB:mexcpp:nargin", 
      "MEXCPP requires 5 input arguments.");
  }
  
  double* nP = (double *) mxGetPr(prhs[0]);
  double* P  = (double *) mxGetPr(prhs[1]);
  double* u  = (double *) mxGetPr(prhs[2]);
  double* U  = (double *) mxGetPr(prhs[3]);
  double* p  = (double *) mxGetPr(prhs[4]);
 
  
  plhs[0]   = mxCreateDoubleMatrix(1,dim,mxREAL);
  
  double n[5];
  for (int i = 0; i < ndg; i += 1){
        n[i] = nP[i] - 1;
  }
  
  
//   printf("%lf %lf\n", nP[0], nP[1]);
//   for (int i = 0; i < 9; i += 1){
//       for (int j = 0; j < 3; j += 1){
//             printf("%lf, ", P[i*3+j]);
//       }
//       printf("\n");
//   }
//   printf("%lf %lf\n", u[0], u[1]);
//   for (int i = 0; i < 12; i += 1)
//         printf("%lf, ", U[i]);
//   printf("\n");
//   printf("%lf %lf\n", p[0], p[1]);
  
  
  
  CurvePoint(n, p, U, P, u, mxGetPr(plhs[0]) );
  
  double SKL[pmax][pmax][dim];
  SurfaceDerivsAlg1(n, p, U, P, u, 2, SKL);
  
  plhs[1]   = mxCreateDoubleMatrix(1,dim,mxREAL);
  int dd = 1;
  double kappaU, kappaV;
  double cross_12[3], tp1, tp2;
  
//   *(mxGetPr(plhs[1]) + 0) = SKL[dd][0][0] + SKL[0][dd][0];
//   *(mxGetPr(plhs[1]) + 1) = SKL[dd][0][1] + SKL[0][dd][1];
//   *(mxGetPr(plhs[1]) + 2) = SKL[dd][0][2] + SKL[0][dd][2];
  
  *(mxGetPr(plhs[1]) + 0) = SKL[dd][0][1]*SKL[0][dd][2] - SKL[dd][0][2]*SKL[0][dd][1];
  *(mxGetPr(plhs[1]) + 1) = SKL[dd][0][2]*SKL[0][dd][0] - SKL[dd][0][0]*SKL[0][dd][2];
  *(mxGetPr(plhs[1]) + 2) = SKL[dd][0][0]*SKL[0][dd][1] - SKL[dd][0][1]*SKL[0][dd][0];
  
  dd = 1;
  plhs[2]   = mxCreateDoubleMatrix(1,dim,mxREAL);
//   *(mxGetPr(plhs[2]) + 0) = SKL[dd][0][0] + SKL[0][dd][0];
//   *(mxGetPr(plhs[2]) + 1) = SKL[dd][0][1] + SKL[0][dd][1];
//   *(mxGetPr(plhs[2]) + 2) = SKL[dd][0][2] + SKL[0][dd][2];
  *(mxGetPr(plhs[2]) + 0) = SKL[dd][dd][0];
  *(mxGetPr(plhs[2]) + 1) = SKL[dd][dd][1];
  *(mxGetPr(plhs[2]) + 2) = SKL[dd][dd][2];
  
//   dd = 2;
//   *(mxGetPr(plhs[2]) + 0) = SKL[dd][0][1]*SKL[0][dd][2] - SKL[dd][0][2]*SKL[0][dd][1];
//   *(mxGetPr(plhs[2]) + 1) = SKL[dd][0][2]*SKL[0][dd][0] - SKL[dd][0][0]*SKL[0][dd][2];
//   *(mxGetPr(plhs[2]) + 2) = SKL[dd][0][0]*SKL[0][dd][1] - SKL[dd][0][1]*SKL[0][dd][0];
  
//   cross_12[0] = SKL[1][0][1]*SKL[2][0][2] - SKL[1][0][2]*SKL[2][0][1];
//   cross_12[1] = SKL[1][0][2]*SKL[2][0][0] - SKL[1][0][0]*SKL[2][0][2];
//   cross_12[2] = SKL[1][0][0]*SKL[2][0][1] - SKL[1][0][1]*SKL[2][0][0];
//   tp1 = sqrt(cross_12[0]*cross_12[0] + cross_12[1]*cross_12[1] + cross_12[2]*cross_12[2]);
//   tp2 = (SKL[1][0][0]*SKL[1][0][0] + SKL[1][0][1]*SKL[1][0][1] + SKL[1][0][2]*SKL[1][0][2]);
//   tp2 = tp2*tp2*tp2;
//   kappaU = tp1/tp2;
//   
//   cross_12[0] = SKL[0][1][1]*SKL[0][2][2] - SKL[0][1][2]*SKL[0][2][1];
//   cross_12[1] = SKL[0][1][2]*SKL[0][2][0] - SKL[0][1][0]*SKL[0][2][2];
//   cross_12[2] = SKL[0][1][0]*SKL[0][2][1] - SKL[0][1][1]*SKL[0][2][0];
//   tp1 = sqrt(cross_12[0]*cross_12[0] + cross_12[1]*cross_12[1] + cross_12[2]*cross_12[2]);
//   tp2 = (SKL[0][1][0]*SKL[0][1][0] + SKL[0][1][1]*SKL[0][1][1] + SKL[0][1][2]*SKL[0][1][2]);
//   tp2 = tp2*tp2*tp2;
//   kappaV = tp1/tp2;
  
//   *(mxGetPr(plhs[1]) + 0) = kappaU;
//   *(mxGetPr(plhs[1]) + 1) = kappaV;
//   *(mxGetPr(plhs[1]) + 2) = 0;
 
  
//   *(mxGetPr(plhs[1]) + 0) = SKL[dd][0][0] * SKL[0][dd][0];
//   *(mxGetPr(plhs[1]) + 1) = SKL[dd][0][1] * SKL[0][dd][1];
//   *(mxGetPr(plhs[1]) + 2) = SKL[dd][0][2] * SKL[0][dd][2];

//   *(mxGetPr(plhs[1]) + 0) = SKL[dd][0][0];
//   *(mxGetPr(plhs[1]) + 1) = SKL[dd][0][1];
//   *(mxGetPr(plhs[1]) + 2) = SKL[dd][0][2];
  
//   *(mxGetPr(plhs[1]) + 0) = SKL[dd][0][1]*SKL[0][dd][2] - SKL[dd][0][2]*SKL[0][dd][1];
//   *(mxGetPr(plhs[1]) + 1) = SKL[dd][0][2]*SKL[0][dd][0] - SKL[dd][0][0]*SKL[0][dd][2];
//   *(mxGetPr(plhs[1]) + 2) = SKL[dd][0][0]*SKL[0][dd][1] - SKL[dd][0][1]*SKL[0][dd][0];
  
  return;
}
