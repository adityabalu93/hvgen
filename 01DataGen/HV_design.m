function HV_design(cf_dh, cf_dr, cf_dZ, loZ, cf_Rfr, cf_HtR, R, Ht, foldername)
global dim
global folderpath
dim = 3;
folderpath = foldername;
% figure; hold on
% axis equal vis3d 
% ax = gca;               % get the current axis
% ax.Clipping = 'off';    % turn clipping off
% view(78, 48);
% xlabel('x');
% ylabel('y');


% ================= Coefs ==================
% cf_dh = 1.2;
% cf_dr = 0.8;
% cf_dZ  = -0.5;

nU = 17;
nV = 12;
% ==========================================
cfs = [cf_dh, cf_dr, cf_dZ, cf_Rfr, cf_HtR, R, Ht, loZ];

th1 = (120)*pi/180;
phi = (1.5)*pi/180;
center = [0 0];

angLf(1,:) = [0, th1] + [phi/2, -phi/2];
angLf(2,:) = [0, th1] + [phi/2, -phi/2] + th1;
angLf(3,:) = [0, th1] + [phi/2, -phi/2] + th1*2;

lf1 = GEN_leaflet2(angLf(1,1), angLf(1,2), center, cfs, nU, nV);
lf2 = GEN_leaflet2(angLf(2,1), angLf(2,2), center, cfs, nU, nV);
lf3 = GEN_leaflet2(angLf(3,1), angLf(3,2), center, cfs, nU, nV);


lf1.id = 1;
lf1.name = 'leaflet';

lf2.id = 2;
lf2.name = 'leaflet';

lf3.id = 3;
lf3.name = 'leaflet';

% write_smesh_pq(lf1, 'IGA_Files/leaflet1.dat', 2);
% write_smesh_pq(lf2, 'IGA_Files/leaflet2.dat', 2);
% write_smesh_pq(lf3, 'IGA_Files/leaflet3.dat', 2);
system(char(compose('bash -c "mkdir %s"',folderpath)));
write_smesh_pq(lf1, folderpath+"/smesh.1.dat", 2);
write_smesh_pq(lf2, folderpath+"/smesh.2.dat", 2);
write_smesh_pq(lf3, folderpath+"/smesh.3.dat", 2);

Covert_to_bezier_ext;


function Covert_to_bezier_ext
global folderpath
% system('cp IGA_Files/leaflet1.dat mesh.dat');
system(char(compose('bash -c "cp %s/smesh.1.dat mesh.dat"',folderpath)));
system('bash -c "p_ref/a.out"');
system('bash -c "cp refined_mesh.dat mesh.dat"');
system(char(compose('bash -c "cp refined_mesh.dat %s/smesh.1.dat"', folderpath)));
system('bash -c "bezier_ext/bin/bezier_shell.exe"');
system(char(compose('bash -c "mv tmesh.1.iga %s/tmesh.leaflet1.iga"',folderpath)));

[~, nCP, ~, ~, ~] = Readsmesh('refined_mesh.dat');
nPU_cu = nCP(1);
nPV_cu = nCP(2);

% system('cp IGA_Files/leaflet2.dat mesh.dat');
system(char(compose('bash -c "cp %s/smesh.2.dat mesh.dat"', folderpath)));
system('bash -c "p_ref/a.out"');
system('bash -c "cp refined_mesh.dat mesh.dat"');
system(char(compose('bash -c "cp refined_mesh.dat %s/smesh.2.dat"', folderpath)));
system('bash -c "bezier_ext/bin/bezier_shell.exe"');
system(char(compose('bash -c "mv tmesh.1.iga %s/tmesh.leaflet2.iga"', folderpath)));

% system('cp IGA_Files/leaflet3.dat mesh.dat');
system(char(compose('bash -c "cp %s/smesh.3.dat mesh.dat"', folderpath)));
system('bash -c "p_ref/a.out"');
system('bash -c "cp refined_mesh.dat mesh.dat"');
system(char(compose('bash -c "cp refined_mesh.dat %s/smesh.3.dat"', folderpath)));
system('bash -c "bezier_ext/bin/bezier_shell.exe"');
system(char(compose('bash -c "mv tmesh.1.iga %s/tmesh.leaflet3.iga"', folderpath)));

[mp(1)] = read_IGA_shell_file(folderpath+"/tmesh.leaflet1.iga");
[mp(2)] = read_IGA_shell_file(folderpath+"/tmesh.leaflet2.iga");
[mp(3)] = read_IGA_shell_file(folderpath+"/tmesh.leaflet3.iga");

mp(1) = add_BC2(mp(1), nPU_cu, nPV_cu);
mp(2) = add_BC2(mp(2), nPU_cu, nPV_cu);
mp(3) = add_BC2(mp(3), nPU_cu, nPV_cu);
% mp(1) = add_BC(mp(1), nPU_cu, nPV_cu);
% mp(2) = add_BC(mp(2), nPU_cu, nPV_cu);
% mp(3) = add_BC(mp(3), nPU_cu, nPV_cu);


p = Combine_mp(mp);

write_shell_IGA(folderpath + "/tmesh.1.iga", p);

% 
% function p = add_BC(p, nPU, nPV)
% 
% ix = 1:nPU:(nPV*nPU);
% ix = [ix, 2:nPU:(nPV*nPU)];
% ix = [ix, 1:nPU*2];
% ix = [ix, (nPV*nPU-nPU*2+1):(nPV*nPU)];
% 
% ix = sort(unique(ix));
% 
% p.sets{2}{1} = length(ix);
% p.sets{2}{2} = 4;
% p.sets{2}{3} = 'fixed';
% p.sets{2}{4} = ix;
% 
% plot3(p.Nodes(ix, 1), p.Nodes(ix, 2), p.Nodes(ix, 3), 'o');

function p = add_BC2(p, nPU, nPV)

ix = 1:nPU:(nPV*nPU);
ix = [ix, 2:nPU:(nPV*nPU)];
ix = [ix, 1:nPU*2];
ix = [ix, (nPV*nPU-nPU*2+1):(nPV*nPU)];

% ix = nPU:nPU:(nPV*nPU);
% ix = [ix, (nPU-1):nPU:(nPV*nPU)];
% ix = [ix, 1:nPU*2];
% ix = [ix, (nPV*nPU-nPU*2+1):(nPV*nPU)];

% ix = (nPV*nPU-nPU*2+1):(nPV*nPU);
% ix = [ix, (nPU-1):nPU:(nPV*nPU)];
% ix = [ix,  nPU:nPU:(nPV*nPU)];
% ix = [ix,  1:nPU:(nPV*nPU)];
% ix = [ix,  2:nPU:(nPV*nPU)];

ix = sort(unique(ix));

p.sets{2}{1} = length(ix);
p.sets{2}{2} = 4;
p.sets{2}{3} = 'fixed';
p.sets{2}{4} = ix;

% plot3(p.Nodes(ix, 1), p.Nodes(ix, 2), p.Nodes(ix, 3), 'o');


function Sur = GEN_leaflet2(ang1, ang2, center, cfs, nPv, nPcn)

pw = 2;

% nPv = 13;
% nPcn = 15;

% nPv = 13 + 5;
% nPcn = 15 + 5;

% cf_dh1 = cfs(1);
% cf_dh2 = cfs(2);
% cf_dr1 = cfs(3);
% cf_dr2 = cfs(4);
% dZ     = cfs(5);

cf_dh = cfs(1);
cf_dr = cfs(2);
dZ    = cfs(3);
cf_Rfr = cfs(4);
cf_HtR = cfs(5);
R = cfs(6);
Ht = cfs(7);
% R  = 1.10634;
% Ht = 1.1108;

HtR = Ht * cf_HtR;
Rfr = R  * cf_Rfr;

% =========== free edge ============
coms(1,:) = [Rfr*cos(ang1), Rfr*sin(ang1), Ht];
coms(2,:) = [Rfr*cos(ang2), Rfr*sin(ang2), Ht];

psc = zeros(3,3);
psc(1,:) = coms(1,:);
psc(end,:) = coms(end,:);

psc(2,:) = [center, coms(1,3)+dZ];

nps = 100;
L3_ps = zeros(nps, 3);
% angs  = linspace(ang1, ang2, nps);
dt = linspace(0, 1, nps-1);
dt = 4*dt.*(1-dt);
dt = dt*0 + 1;

t = zeros(1, nps);
for i = 1:(nps-1)
    t(i+1) = t(i) + dt(i);
end
t = t/max(t);
angs = t * (ang2 - ang1) + ang1;

t = ord4p([0 1 0.5], [1 1 -5 5 0], nps);

%t = sin(pi*(linspace(0, 1, nps))+ pi);
% t = linspace(-1, 1, nps);
% t = (acos(t)-pi)/pi;

for i = 1:nps
    
    L3_ps(i,1:2) = [R*cos(angs(i)), R*sin(angs(i))];
    L3_ps(i,3) = t(i)*HtR;
    %L3_ps(i,3) = (1-sin(pi*(t(i))+ pi))*Ht;
    
end

loZ = cfs(8); % lowest point (cm)
nIns = 1;
nPLc = nIns*2 + 3;
disp(cfs)
insr1 = zeros(nIns, 3);
insr2 = zeros(nIns, 3);
for i = 1:nIns
    t = (i)/(nIns+1);
    
    insr1(i,:) = (1-t)*psc(1,:) + t*psc(2,:);
    insr2(i,:) = (1-t)*psc(2,:) + t*psc(3,:);
end

psc = [psc(1,:);insr1;psc(2,:);insr2;psc(3,:)];

nPsc = size(psc,1);

for i = 1:nPsc
    t = -abs(2/(nPsc-1)*(i-(nPsc+1)/2))+1;
    
    psc(i,3) = psc(i,3) - loZ*t;
end

Usc = culc_U(nPsc, pw);

% nPv = 21;

[CPat, Uat] = Least_Sqare_Fitting(L3_ps, nPLc, pw);

% plot3(L3_ps(:,1), L3_ps(:,2), L3_ps(:,3),'o-', 'markersize', 10);


% plot3(CPat(:,1), CPat(:,2),CPat(:,3),'o-', 'markersize', 10);

psLw = zeros(nPv, 3);
psHi = zeros(nPv, 3);
for i = 1: nPv
      t = (i-1)/(nPv-1);
      psLw(i,:) = NURBS_eval_3D(nPLc,[CPat, ones(nPLc,1)]', t, Uat, pw);
      psHi(i,:) = NURBS_eval_3D(nPsc,[psc, ones(nPsc,1)]', t, Usc, pw);
end 

% plot3(psLw(:,1), psLw(:,2),psLw(:,3),'o-', 'markersize', 10);
% plot3(psHi(:,1), psHi(:,2),psHi(:,3),'o-', 'markersize', 10);


NcsP = pw+1; % fix for cubic curve (p_v = 2 or 3)
cixs = 2:(nPv-1);

% nPcn = 23;
CPcn = zeros(nPcn, 3, nPv);

t = linspace(0, 1, nPcn);

for i = 1: nPcn
    
    ix = 1;
    CPcn(i,:,ix) = t(i)*psLw(ix,:) + (1-t(i))*psHi(ix,:);
    
    ix = nPv;
    CPcn(i,:,ix) = t(i)*psLw(ix,:) + (1-t(i))*psHi(ix,:);
    
end

% ix = 1;
% plot3(CPcn(:,1,ix), CPcn(:,2,ix),CPcn(:,3,ix),'o-', 'markersize', 10);
% 
% ix = nPv;
% plot3(CPcn(:,1,ix), CPcn(:,2,ix),CPcn(:,3,ix),'o-', 'markersize', 10);


% CPcn(:,:,1)   = repmat(psLw(1,:)  , nPcn, 1);
% CPcn(:,:,nPv) = repmat(psLw(nPv,:), nPcn, 1);

Ucn = culc_U(nPcn, pw);
nUcn = length(Ucn);
in_Ucn  = Ucn((pw+2):(nUcn-pw-1)); % for knot insertion
Nin_Ucn = ones(1,length(in_Ucn));

for i = 1:(nPv-2)
    cix = cixs(i);
    
    t = (cix-1)/(nPv-1);
    
    t = (t-0.5)*1.1 + 0.5;
    
    wt = 4*t*(1-t);
    
    %wt = -abs(2/(nPv-2-1)*(i-(nPv-2+1)/2))+1;
    
    wt1 = (1-wt);
    if t > 0.5
        wt1 = 0;
    end
    
    wt2 = (1-wt);
    if t < 0.5
        wt2 = 0;
    end
    
    dh = [0, 0, psHi(cix,3) - psLw(cix,3)];
    dr = [psHi(cix,1:2) - psLw(cix,1:2), 0];
    
    
    csCPcn(1,:) = psHi(cix,:);
    csCPcn(3,:) = psLw(cix,:);
    
    
    csCPcn(2,:) = psLw(cix,:) + dh*(cf_dh) + dr*(cf_dr);
    
%     csCPcn(1,:) = psHi(cix,:);
%     csCPcn(4,:) = psLw(cix,:);
%     
%     
%     csCPcn(2,:) = psLw(cix,:) + dh*(cf_dh1) + dr*(cf_dr1);
%     csCPcn(3,:) = psLw(cix,:) + dh*(cf_dh2) + dr*(cf_dr2);
    
%     plot3( csCPcn(:,1),  csCPcn(:,2), csCPcn(:,3),'o-', 'markersize', 10);

    
    Ucs = culc_U(NcsP, pw);
    
    [nPout ,Uout, CPout] = NURBS_Knot_Insertion(NcsP, pw, Ucs, [csCPcn, ones(size(csCPcn,1),1)]', 1e-10, in_Ucn, uint32(Nin_Ucn));
    
    
    CPmx = CPout(:,1:3)*wt + CPcn(:,1:3,1)*wt1 + CPcn(:,1:3, nPv)*wt2;
    
%     plot3(CPmx(:,1),  CPmx(:,2), CPmx(:,3),'o-', 'markersize', 10);
    
    nC = size(CPmx, 1);
    
    d1 = CPout(1,1:3)  - CPmx(1  ,:);
    d2 = CPout(nC,1:3) - CPmx(nC,:);
    
    td = linspace(1, 0, nC);
    
    CPmx2 = zeros(nC,3);
    for d = 1:nC
        CPmx2(d,:) = CPmx(d,:) + td(d) * d1 + (1-td(d)) * d2;
    end
    
    CPcn(:,:,cix) = CPmx2;
    
end

[nPU, nPV, U, V, P_Sur] = Create_Surface(CPcn, Ucn, pw);

Sur.nPU   = nPU;
Sur.nPV   = nPV;
Sur.U     = U;
Sur.V     = V;
Sur.P_Sur = P_Sur;

Sur.P_Sur = Sur_change_dir(Sur.P_Sur, Sur.nPU, Sur.nPV);


function Sur= GEN_leaflet(ang1, ang2, center, cfs)

cf_dh1 = cfs(1);
cf_dh2 = cfs(2);
cf_dr1 = cfs(3);
cf_dr2 = cfs(4);
dZ     = cfs(5);

R  = 1.10634;
Ht = 1.1108;

pw = 3;

% =========== free edge ============
coms(1,:) = [R*cos(ang1), R*sin(ang1), Ht];
coms(2,:) = [R*cos(ang2), R*sin(ang2), Ht];

% plot3(coms(:,1), coms(:,2), coms(:,3), 'o-');

psc = zeros(3,3);
psc(1,:) = coms(1,:);
psc(end,:) = coms(end,:);

% psc(2,:) = [coms(1,2)*3, coms(1,2)*3*2, coms(1,3)];
% psc(2,:) = [coms(1,2)*5, coms(1,2)*5*2, coms(1,3)];
% psc(2,:) = [0, 0, coms(1,3)];
psc(2,:) = [center, coms(1,3)+dZ];


% plot3(psc(:,1), psc(:,2),psc(:,3),'o-');

nps = 100;
L3_ps = zeros(nps, 3);
% angs  = linspace(ang1, ang2, nps);
dt = linspace(0, 1, nps-1);
dt = 4*dt.*(1-dt);
t = zeros(1, nps);
for i = 1:(nps-1)
    t(i+1) = t(i) + dt(i);
end
t = t/max(t);
angs = t * (ang2 - ang1) + ang1;


t = ord4p([0 1 0.5], [1 1 -6 6 0], nps);

%t = sin(pi*(linspace(0, 1, nps))+ pi);
% t = linspace(-1, 1, nps);
% t = (acos(t)-pi)/pi;

for i = 1:nps
    
    L3_ps(i,1:2) = [R*cos(angs(i)), R*sin(angs(i))];
    L3_ps(i,3) = t(i)*Ht;
    %L3_ps(i,3) = (1-sin(pi*(t(i))+ pi))*Ht;
    
end




loZ = 0.0; % lowest point (cm)
nIns = 2;
nPLc = nIns*2 + 3;

insr1 = zeros(nIns, 3);
insr2 = zeros(nIns, 3);
for i = 1:nIns
    t = (i)/(nIns+1);
    
    insr1(i,:) = (1-t)*psc(1,:) + t*psc(2,:);
    insr2(i,:) = (1-t)*psc(2,:) + t*psc(3,:);
end

psc = [psc(1,:);insr1;psc(2,:);insr2;psc(3,:)];

nPsc = size(psc,1);

for i = 1:nPsc
    t = -abs(2/(nPsc-1)*(i-(nPsc+1)/2))+1;
    
    psc(i,3) = psc(i,3) - loZ*t;
end


% plot3(psc(:,1), psc(:,2),psc(:,3),'o-');
Usc = culc_U(nPsc, pw);

nPv = 21;

[CPat, Uat] = Least_Sqare_Fitting(L3_ps, nPLc, pw);

psLw = zeros(nPv, 3);
psHi = zeros(nPv, 3);
for i = 1: nPv
      t = (i-1)/(nPv-1);
      psLw(i,:) = NURBS_eval_3D(nPLc,[CPat, ones(nPLc,1)]', t, Uat, pw);
      psHi(i,:) = NURBS_eval_3D(nPsc,[psc, ones(nPsc,1)]', t, Usc, pw);
end 

% plot3(psLw(:,1), psLw(:,2),psLw(:,3),'o-', 'markersize', 10);
% plot3(psHi(:,1), psHi(:,2),psHi(:,3),'o-', 'markersize', 10);


NcsP = 4; % fix for cubic curve (p_v = 3)
cixs = 2:(nPv-1);

nPcn = 23;
CPcn = zeros(nPcn, 3, nPv);

CPcn(:,:,1)   = repmat(psLw(1,:)  , nPcn, 1);
CPcn(:,:,nPv) = repmat(psLw(nPv,:), nPcn, 1);

Ucn = culc_U(nPcn, pw);
nUcn = length(Ucn);
in_Ucn  = Ucn((pw+2):(nUcn-pw-1)); % for knot insertion
Nin_Ucn = ones(1,length(in_Ucn));


for i = 1:(nPv-2)
    cix = cixs(i);
    
    t = (cix-1)/(nPv-1);
    
    t = (t-0.5)*1.1 + 0.5;
    
    wt = 4*t*(1-t);
    
    %wt = -abs(2/(nPv-2-1)*(i-(nPv-2+1)/2))+1;
    
    wt1 = (1-wt);
    if t > 0.5
        wt1 = 0;
    end
    
    wt2 = (1-wt);
    if t < 0.5
        wt2 = 0;
    end
    
    dh = [0, 0, psHi(cix,3) - psLw(cix,3)];
    dr = [psHi(cix,1:2) - psLw(cix,1:2), 0];
    
    csCPcn(1,:) = psHi(cix,:);
    csCPcn(4,:) = psLw(cix,:);
    
    %csCPcn(2,:) = psLw(cix,:) + dh*(2/3 - 0.1) + dr*(2/3 + 0.1);
    %csCPcn(3,:) = psLw(cix,:) + dh*(1/3 - 0.2) + dr*(1/3 + 0.2);
    
    csCPcn(2,:) = psLw(cix,:) + dh*(cf_dh1) + dr*(cf_dr1);
    csCPcn(3,:) = psLw(cix,:) + dh*(cf_dh2) + dr*(cf_dr2);
    
    %plot3( csCPcn(:,1),  csCPcn(:,2), csCPcn(:,3),'o-', 'markersize', 10);

    
    Ucs = culc_U(NcsP, pw);
    
    [nPout ,Uout, CPout] = NURBS_Knot_Insertion(NcsP, pw, Ucs, [csCPcn, ones(size(csCPcn,1),1)]', 1e-10, in_Ucn, uint32(Nin_Ucn));
    
    
    CPmx = CPout(:,1:3)*wt + CPcn(:,1:3,1)*wt1 + CPcn(:,1:3, nPv)*wt2;
    
    nC = size(CPmx, 1);
    
    d1 = CPout(1,1:3)  - CPmx(1  ,:);
    d2 = CPout(nC,1:3) - CPmx(nC,:);
    
    td = linspace(1, 0, nC);
    
    CPmx2 = zeros(nC,3);
    for d = 1:nC
        CPmx2(d,:) = CPmx(d,:) + td(d) * d1 + (1-td(d)) * d2;
    end
    
    CPcn(:,:,cix) = CPmx2;
    
end

[nPU, nPV, U, V, P_Sur, H_pa] = Create_Surface(CPcn, Ucn, pw);

Sur.nPU   = nPU;
Sur.nPV   = nPV;
Sur.U     = U;
Sur.V     = V;
Sur.P_Sur = P_Sur;

Sur.P_Sur = Sur_change_dir(Sur.P_Sur, Sur.nPU, Sur.nPV);

% ix = 1:Sur.nPV*2;
% ix = [ix, 2:Sur.nPV:(Sur.nPV*Sur.nPU)];
% ix = [ix, 1:Sur.nPV:(Sur.nPV*Sur.nPU)];
% ix = [ix, Sur.nPV:Sur.nPV:(Sur.nPV*Sur.nPU)];
% ix = [ix, (Sur.nPV-1):Sur.nPV:(Sur.nPV*Sur.nPU)];
% 
% 
% ix = sort(unique(ix));
% 
% Sur.fix_ix = ix;

% plot3(Sur.P_Sur(ix, 1), Sur.P_Sur(ix, 2), Sur.P_Sur(ix, 3), 'o');


function [nPU, nPV, U, V, P_Sur] = Create_Surface(PV, U, p)
global dim

NPP = 150;

nPV  = size(PV, 3);
nPU  = size(PV,1);
P = zeros(nPV*nPU, 3);

for i = 1:nPV
    for j = 1:nPU
        %P((i-1)*nPU+j,:) = PV(j,:,i);
        P((j-1)*nPV + i,:) = PV(j,:,i);
    end 
end

n = nPV - 1;

% ======== Uniformly distributed knot vector ========
dx = 1/(n + 1 - p);
m = p + n + 1; % m = p + n + 1

V = zeros(1,m+1);
for i = 1: m
      if i <= p || i >= m-p+1
            d = 0;
      else
            d = dx;
      end
      V(i+1) = V(i) + d;
end
% V = V.^(1/6);

% nPU = size(P1, 1);
P_Sur = zeros(nPU*nPV, dim);
for i = 1: nPU
        ix = ((i-1)*nPV+1):i*nPV;
        
      [P_Sur( ((i-1)*nPV+1):i*nPV,:), ~] = Least_Sqare_Fitting(P(ix,:), nPV, p);
end



nx = NPP;
ny = NPP;
c = zeros(nx, ny, 3);
% dcV = zeros(nx*ny, 1);
for i = 0: (nx-1)
      for j = 0:(ny-1)
            u = [i/(nx-1), j/(ny-1)];
            [c(i+1,j+1,:), dc, ddc] = B_Spline_2D_Surface([nPU, nPV],P_Sur', u, [U, V]', [p,p]);
      end
end

% create IEN and NOTES
IEN = zeros((nx-1)*(ny-1), 4);
NOTES = zeros(nx*ny,3);
for i = 1: nx
      for j = 1: ny
            NOTES((i-1)*ny + j, :) = c(i,j,:);
      end
end

for i = 1: (nx-1)
      for j = 1: (ny-1)
            IEN((i-1)*(ny-1) + j, 1:2) = [j, j+1] + (i-1)*ny; 
            IEN((i-1)*(ny-1) + j, 3:4) = [j+1, j] + i*ny; 
      end
end

% H_pa1 = patch('Faces',IEN,'Vertices',NOTES,'FaceColor','b', 'edgecolor','none', 'facealpha', 0.8);
% material([0.5 0.5 0.5]);

% H_pa1 = patch('Faces',IEN,'Vertices',NOTES,'FaceColor',[1 1 1]*0.5, 'edgecolor','none', 'facealpha', 0.9);
% material([0.5 0.5 0.5]);

% plot3(P_Sur(:,1), P_Sur(:,2), P_Sur(:,3), 'r.', 'markersize', 15);


% ====================== plot elements boundareis ========================
% nx = length(U) - p;
% ny = length(V) - p;
% c = zeros(nx, ny, 3);
% % dcV = zeros(nx*ny, 1);
% for i = 0: (nx-1)
%       for j = 0:(ny-1)
%             u = [U(p+i+1), V(p+j+1)];
%             [c(i+1,j+1,:), dc, ddc] = B_Spline_2D_Surface([nPU, nPV],P_Sur', u, [U, V]', [p,p]);
%       end
% end

% % create IEN and NOTES
% IEN = zeros((nx-1)*(ny-1), 4);
% NOTES = zeros(nx*ny,3);
% for i = 1: nx
%       for j = 1: ny
%             NOTES((i-1)*ny + j, :) = c(i,j,:);
%       end
% end
% 
% for i = 1: (nx-1)
%       for j = 1: (ny-1)
%             IEN((i-1)*(ny-1) + j, 1:2) = [j, j+1] + (i-1)*ny; 
%             IEN((i-1)*(ny-1) + j, 3:4) = [j+1, j] + i*ny; 
%       end
% end
% 
% patch('Faces',IEN,'Vertices',NOTES,'FaceColor','none', 'edgecolor','k', 'linewidth', 2);


nx = length(U) - p;
ny = NPP;
c = zeros(nx, ny, 3);
% dcV = zeros(nx*ny, 1);
for i = 0: (nx-1)
      for j = 0:(ny-1)
            u = [U(p+i+1), j/(ny-1)];
            [c(i+1,j+1,:), dc, ddc] = B_Spline_2D_Surface([nPU, nPV],P_Sur', u, [U, V]', [p,p]);
      end
end
IEN = zeros((nx-1)*(ny-1), 2);
NOTES = zeros(nx*ny,3);
for i = 1: nx
      for j = 1: ny
            NOTES((i-1)*ny + j, :) = c(i,j,:);
      end
      for j = 1: (ny-1)
          IEN((i-1)*(ny-1) + j, 1) = (i-1)*ny + j;
          IEN((i-1)*(ny-1) + j, 2) = (i-1)*ny + j + 1;
      end
end

% H_pa2 = patch('Faces',IEN,'Vertices',NOTES,'FaceColor','none', 'edgecolor','k', 'linewidth', 1);

nx = NPP;
ny = length(V) - p;
c = zeros(nx, ny, 3);
% dcV = zeros(nx*ny, 1);
for i = 0: (nx-1)
      for j = 0:(ny-1)
            u = [i/(nx-1), V(p+j+1)];
            [c(i+1,j+1,:), dc, ddc] = B_Spline_2D_Surface([nPU, nPV],P_Sur', u, [U, V]', [p,p]);
      end
end
IEN = zeros((nx-1)*(ny-1), 2);
NOTES = zeros(nx*ny,3);
for j = 1: ny
      for i = 1: nx
            %NOTES((i-1)*ny + j, :) = c(i,j,:);
            NOTES((j-1)*nx + i, :) = c(i,j,:);
      end
      for i = 1: (nx-1)
          IEN((j-1)*(nx-1) + i, 1) = (j-1)*nx + i;
          IEN((j-1)*(nx-1) + i, 2) = (j-1)*nx + i + 1;
      end
end




% H_pa3 = patch('Faces',IEN,'Vertices',NOTES,'FaceColor','none', 'edgecolor','k', 'linewidth', 1);

% H_pa = [H_pa1, H_pa2, H_pa3];



function A = ord4p(X, F, n)


K = [1  X(1) X(1)^2,  X(1)^3     X(1)^4;
     1  X(2) X(2)^2,  X(2)^3     X(2)^4;
     0  1    2*X(1),  3*X(1)^2, 4*X(1)^3;
     0  1    2*X(2),  3*X(2)^2, 4*X(2)^3;
     1  X(3) X(3)^2,  X(3)^3     X(3)^4];

C = inv(K)*F';

t = linspace(X(1), X(2), n);

A = C(1) + C(2)*t + C(3)*t.^2 + C(4)*t.^3 + C(5)*t.^4; 
 

function [P, U] = Least_Sqare_Fitting(Q, nP, p)
global dim

nQ = size(Q,1);
n = nP - 1;

% ======== Uniformly distributed knot vector ========
dx = 1/(n + 1 - p);
m = p + n + 1; % m = p + n + 1

U = zeros(1,m+1);
for i = 1: m
      if i <= p || i >= m-p+1
            d = 0;
      else
            d = dx;
      end
      U(i+1) = U(i) + d;
end

% ========= u_bar ========
u_bar = linspace(0, 1, nQ);
m_bar = nQ - 1;

% ============ Fitting ============

N = zeros(m_bar-1,n-1);
for i = 1: (m_bar-1)
      bf = zeros(1, n+1);
      span = findspan(n,p,u_bar(i+1),U);
      bfnv = basisfun(span,u_bar(i+1),p,U);
      bf((span-p+1):(span+1)) = bfnv;
      N(i,:) = bf(2:n);
end

Rk = zeros(m_bar-1,dim);
for i = 1:(m_bar-1)
      bf = zeros(1, n+1);
      span = findspan(n,p,u_bar(i+1),U);
      bfnv = basisfun(span,u_bar(i+1),p,U);
      bf((span-p+1):(span+1)) = bfnv;
      Rk(i,:) = Q(i+1,:) - bf(1)* Q(1,:) - bf(n+1)* Q(nQ,:);
end
R = zeros(n-1, dim);
for i = 1: (m_bar-1)
      bf = zeros(1, n+1);
      span = findspan(n,p,u_bar(i+1),U);
      bfnv = basisfun(span,u_bar(i+1),p,U);
      bf((span-p+1):(span+1)) = bfnv;
      R = R + repmat(bf(2:n)', 1, dim).*repmat(Rk(i,:), n-1, 1);
end

matN = N'*N;
P = inv(matN)*R;
P = [Q(1,:); P; Q(nQ,:)];

function U = culc_U(nP, p)
 
n = nP - 1;
 
% ======== Uniformly distributed knot vector ========
dx = 1/(n + 1 - p);
m = p + n + 1; % m = p + n + 1
 
U = zeros(1,m+1);
for i = 1: m
      if i <= p || i >= m-p+1
            d = 0;
      else
            d = dx;
      end
      U(i+1) = U(i) + d;
end

function plot_Surface(P_Sur, U, V, nPU, nPV, pU, pV)


P_Sur2 = P_Sur;

for j = 1: nPV
    for i = 1: nPU
        ix  = (j-1)*nPU + i;
        ix2 = (i-1)*nPV + j;
        
        P_Sur(ix2,:) = P_Sur2(ix,:);
    end
end



NPP = 150;

nx = NPP;
ny = NPP;
c = zeros(nx, ny, 3);
% dcV = zeros(nx*ny, 1);
for i = 0: (nx-1)
      for j = 0:(ny-1)
            u = [i/(nx-1), j/(ny-1)];
            [c(i+1,j+1,:), dc, ddc] = B_Spline_2D_Surface([nPU, nPV],P_Sur', u, [U, V]', [pU,pV]);
      end
end

% create IEN and NOTES
IEN = zeros((nx-1)*(ny-1), 4);
NOTES = zeros(nx*ny,3);
for i = 1: nx
      for j = 1: ny
            NOTES((i-1)*ny + j, :) = c(i,j,:);
      end
end

for i = 1: (nx-1)
      for j = 1: (ny-1)
            IEN((i-1)*(ny-1) + j, 1:2) = [j, j+1] + (i-1)*ny; 
            IEN((i-1)*(ny-1) + j, 3:4) = [j+1, j] + i*ny; 
      end
end

H_pa1 = patch('Faces',IEN,'Vertices',NOTES,'FaceColor','b', 'edgecolor','none', 'facealpha', 0.8);

nx = length(U) - pU;
ny = NPP;

c = zeros(nx, ny, 3);
% dcV = zeros(nx*ny, 1);
for i = 0: (nx-1)
      for j = 0:(ny-1)
            u = [U(pU+i+1), j/(ny-1)];
            [c(i+1,j+1,:), dc, ddc] = B_Spline_2D_Surface([nPU, nPV],P_Sur', u, [U, V]', [pU,pV]);
      end
end
IEN = zeros((nx-1)*(ny-1), 2);
NOTES = zeros(nx*ny,3);
for i = 1: nx
      for j = 1: ny
            NOTES((i-1)*ny + j, :) = c(i,j,:);
      end
      for j = 1: (ny-1)
          IEN((i-1)*(ny-1) + j, 1) = (i-1)*ny + j;
          IEN((i-1)*(ny-1) + j, 2) = (i-1)*ny + j + 1;
      end
end

H_pa2 = patch('Faces',IEN,'Vertices',NOTES,'FaceColor','none', 'edgecolor','k', 'linewidth', 1);

nx = NPP;
ny = length(V) - pV;
c = zeros(nx, ny, 3);
% dcV = zeros(nx*ny, 1);
for i = 0: (nx-1)
      for j = 0:(ny-1)
            u = [i/(nx-1), V(pV+j+1)];
            [c(i+1,j+1,:), dc, ddc] = B_Spline_2D_Surface([nPU, nPV],P_Sur', u, [U, V]', [pU,pV]);
      end
end
IEN = zeros((nx-1)*(ny-1), 2);
NOTES = zeros(nx*ny,3);
for j = 1: ny
      for i = 1: nx
            %NOTES((i-1)*ny + j, :) = c(i,j,:);
            NOTES((j-1)*nx + i, :) = c(i,j,:);
      end
      for i = 1: (nx-1)
          IEN((j-1)*(nx-1) + i, 1) = (j-1)*nx + i;
          IEN((j-1)*(nx-1) + i, 2) = (j-1)*nx + i + 1;
      end
end




H_pa3 = patch('Faces',IEN,'Vertices',NOTES,'FaceColor','none', 'edgecolor','k', 'linewidth', 1);

H_pa = [H_pa1, H_pa2, H_pa3];

function P_Sur = Sur_change_dir(P_Sur, nPU, nPV)

% ======= flip U ========
P_Sur2 = P_Sur;
for i = 1: nPU
    ix = ((i-1)*nPV+1):i*nPV;
    
    i2 = nPU - i + 1;
    ix2 = ((i2-1)*nPV+1):i2*nPV;
    P_Sur2(ix2, :) = P_Sur(ix, :);
end
P_Sur = P_Sur2;

function write_smesh(suf, fname)

fileID = fopen(fname,'w');

fprintf(fileID, '3\n');
fprintf(fileID, '3 3\n');
fprintf(fileID, '%d %d\n', suf.nPU, suf.nPV);
fprintf(fileID, '%.5e ', suf.U);
fprintf(fileID, '\n');
fprintf(fileID, '%.5e ', suf.V);
fprintf(fileID, '\n');

% for j = 1:suf.nPV
%     for i = 1:suf.nPU
%         ix = (i-1)*suf.nPV+j;
%         fprintf(fileID, '%.8e %.8e %.8e %.8e\n', suf.P_Sur(ix,:), 1 );
%     end
% end

P_Sur2 = suf.P_Sur;

for j = 1: suf.nPV
    for i = 1: suf.nPU
        ix  = (j-1)*suf.nPU + i;
        ix2 = (i-1)*suf.nPV + j;
        
        suf.P_Sur(ix,:) = P_Sur2(ix2,:);
    end
end


ix = 0;
for j = 1:suf.nPV
    for i = 1:suf.nPU
        ix = ix + 1;
        fprintf(fileID, '%.8e %.8e %.8e %.8e\n', suf.P_Sur(ix,:), 1 );
    end
end

fprintf(fileID, '%d %s\n', suf.id, suf.name);

fclose(fileID);

function write_smesh_pq(suf, fname, p)

fileID = fopen(fname,'w');

fprintf(fileID, '3\n');
fprintf(fileID, '%d %d\n', p, p);
fprintf(fileID, '%d %d\n', suf.nPU, suf.nPV);
fprintf(fileID, '%.5e ', suf.U);
fprintf(fileID, '\n');
fprintf(fileID, '%.5e ', suf.V);
fprintf(fileID, '\n');

% for j = 1:suf.nPV
%     for i = 1:suf.nPU
%         ix = (i-1)*suf.nPV+j;
%         fprintf(fileID, '%.8e %.8e %.8e %.8e\n', suf.P_Sur(ix,:), 1 );
%     end
% end

P_Sur2 = suf.P_Sur;

for j = 1: suf.nPV
    for i = 1: suf.nPU
        ix  = (j-1)*suf.nPU + i;
        ix2 = (i-1)*suf.nPV + j;
        
        suf.P_Sur(ix,:) = P_Sur2(ix2,:);
    end
end

ix = 0;
for j = 1:suf.nPV
    for i = 1:suf.nPU
        ix = ix + 1;
        fprintf(fileID, '%.8e %.8e %.8e %.8e\n', suf.P_Sur(ix,:), 1 );
    end
end


fprintf(fileID, '%d %s\n', suf.id, suf.name);

fclose(fileID);

function p = Combine_mp(mp)

n_mp = length(mp);

dien = zeros(3,1);
dID  = zeros(3,1);
for i = 2:n_mp
    dien(i) = dien(i-1) + mp(i-1).NNODE;
    dID(i)  = dID(i-1)  + mp(i-1).Nel;
end

p.Nodes = [];
ix = 0;
ixset = 0;
for i = 1:n_mp
    p.Nodes = [p.Nodes; mp(i).Nodes];
    
    for j = 1:mp(i).Nel
        ix = ix + 1;
        p.IEN{ix} = mp(i).IEN{j} + dien(i);
        p.C{ix}   = mp(i).C{j};
    end
    
    p.pID{i} = (1:mp(i).Nel)-1 + dID(i);
    
    
    nsets = length(mp(i).sets);
    
    for j = 1:nsets
        
        if strcmp(mp(i).sets{j}{3}, 'fixed') 
            continue
        end
        
        ixset = ixset + 1;
        p.sets{ixset}{1} = mp(i).sets{j}{1};
        p.sets{ixset}{2} = mp(i).sets{j}{2};
        p.sets{ixset}{3} = mp(i).sets{j}{3};
        
        p.sets{ixset}{4} = mp(i).sets{j}{4} + dID(i);
    end
    
end

p.Nel   = ix;
p.NNODE = size(p.Nodes,1);

% for BC
ixset = ixset + 1;
p.sets{ixset}{4} = [];
p.sets{ixset}{1} = 0;
for i = 1:n_mp
    p.sets{ixset}{1} = p.sets{ixset}{1} + mp(i).sets{2}{1};
    p.sets{ixset}{2} = mp(i).sets{2}{2};
    p.sets{ixset}{3} = mp(i).sets{2}{3};
    
    p.sets{ixset}{4} = [p.sets{ixset}{4}, mp(i).sets{2}{4} + dien(i)];
end

function write_shell_IGA(fileName, st)
p  = 3;
pb = 3;

igaFL = fopen(fileName,'w');

CP = st.Nodes;
% IEN = st.IEN;
% lc = st.C;

nodeN = size(CP,1);
elemN = length(st.IEN);

fprintf(igaFL,'type surface\n');
fprintf(igaFL,'nodeN %d\n', nodeN);
fprintf(igaFL,'elemN %d\n', elemN);

for i = 1: nodeN
    fprintf(igaFL,'node %.16e %.16e %.16e %.16e\n', ...
        CP(i,1), CP(i,2), CP(i,3), CP(i,4));
end

for i = 1: elemN
    
    IEN = st.IEN{i};
    nshl = length(IEN);
    
    fprintf(igaFL,'belem %d %d %d\n', nshl, pb, pb);
    for a = 1: nshl
        fprintf(igaFL,'%d ', IEN(a)-1);
    end
    fprintf(igaFL,'\n');
    
    lc = st.C{i};
    %size(lc)
    for a = 1: nshl
        for b = 1: (pb+1)^2
            %fprintf(igaFL,'%.16e ', lc(a, b, mod(i-1, LelemN)+1));
            fprintf(igaFL,'%.16e ', lc(a, b));
        end
        fprintf(igaFL,'\n');
    end
end

nid = size(st.sets, 2);

for i = 1:nid
    ne    = st.sets{i}{1};
    sid   = st.sets{i}{2};
    sname = st.sets{i}{3};
    ixs   = st.sets{i}{4};
    
    if strcmp(sname, 'fixed') 
        fprintf(igaFL,'set %d node fixed ', ne);
        fprintf(igaFL,'%d ', ixs-1);
        fprintf(igaFL,'\n');
    else
        fprintf(igaFL,'set %d elem %d_%s ', ne, sid, sname);
        fprintf(igaFL,'%d ', ixs-1);
        fprintf(igaFL,'\n');
    end
end

% nid = size(st.pID, 2);
% 
% for i = 1:nid
%     fprintf(igaFL,'set %d elem %d_name\n', size(st.pID{i},2), i);
%     fprintf(igaFL,'%d ', st.pID{i});
%     fprintf(igaFL,'\n');
% end

% fprintf(igaFL,'set %d elem 5_skirt1\n', elemN);
% fprintf(igaFL,'%d ', (1:elemN)-1);
% fprintf(igaFL,'\n');

fclose(igaFL);

function st = read_IGA_shell_file(fname)

% p  = 3; % cubic
% pb = 3; % berstein 

igaF = fopen(fname,'r');

lin = fgets(igaF); % 1st line

lin = fgets(igaF); % second line

ixN = strfind(lin, 'nodeN');
nodeN = sscanf(lin(ixN+5:end), '%g', 1);

lin = fgets(igaF); % third line
ixN = strfind(lin, 'elemN');
elemN = sscanf(lin(ixN+5:end), '%g', 1);

Nodes = zeros(nodeN, 4);

for i = 1: nodeN
      lin = fgets(igaF);
      Nodes(i,1:4) = sscanf(lin(ixN+4:end), '%g', 4);
end

% C   = zeros((p+1)^2, (pb+1)^2, elemN);
% IEN = zeros(elemN, (p+1)^2);

for i = 1: elemN
% for i = 1: 1
    lin = fgets(igaF); % skip one
    tmp3 = sscanf(lin(ixN+6:end), '%d', 3);
    nshl = tmp3(1);
    pbi  = tmp3(2);
    pbj  = tmp3(3);
    st.IEN{i} = fscanf(igaF, '%d', [1 nshl]) + 1;
    tmC = fscanf(igaF, '%f', [(pbi+1)*(pbj+1), nshl]);
    st.C{i} = tmC';
    lin = fgets(igaF);
end

st.Nodes = Nodes;
%st.C     = C;
% st.IEN   = IEN;

st.NNODE    = nodeN;
st.Nel      = elemN;

% IEN = IEN + 1;

nst = 0;
while ~feof(igaF)
    nst = nst + 1;
    lin = fgets(igaF);
    ne = sscanf(lin(4:end), '%g', 1);
    k = strfind(lin,'elem ');
    if ~isempty(k)
        sid  = sscanf(lin(k+5:end), '%g', 1);
    else
        k = strfind(lin,'node ');
        sid  = sscanf(lin(k+4:end), '%g', 1);
    end

    k = strfind(lin,'_');
    sname = sscanf(lin(k+1:end), '%s', 1);
    k = strfind(lin,sname);
    ixs = sscanf(lin(k+length(sname):end), '%g', ne);
    
    st.sets{nst} = {ne, sid, sname, ixs+1};

end

st.ptypeELE = zeros(elemN, 1);
for ip = 1: nst
    if strcmp(st.sets{ip}{3}, 'fixed')
        continue;
    end
    
    eleids = st.sets{ip}{4};

    st.ptypeELE( eleids ) = st.sets{ip}{2};
    %st.ptypeELE( st.sets{ip}{4} ) = st.sets{ip}{2};
end

st.ptypeNode = zeros(nodeN, 1);

for iel = 1: elemN
    
    ien = st.IEN{iel};
    
    st.ptypeNode(ien) = st.ptypeELE(iel);
end


fclose(igaF);

function [p, nCP, P, U, V] = Readsmesh(FileName)
fileID = fopen(FileName,'r');

lin = fgets(fileID);
dim = sscanf(lin, '%d', 1);
lin = fgets(fileID);
p = sscanf(lin, '%d', 2);
lin = fgets(fileID);
nCP = sscanf(lin, '%d', 2);
nU = nCP + p + 1;
lin = fgets(fileID);
Up = sscanf(lin, '%f', nU(1));
lin = fgets(fileID);
Vp = sscanf(lin, '%f', nU(2));

Np = nCP(1)*nCP(2);
P = zeros(Np, dim+1);
for i = 1: Np
      lin = fgets(fileID);
      P(i, :) = sscanf(lin, '%f', dim+1);
end

p = p';
% nCP = nCP';
% nCP = nCP([2 1]);
% U = Vp';
% V = Up';
nCP = nCP';
U = Up';
V = Vp';

V = V/max(V);
U = U/max(U);
