      subroutine input_knots_3D(uflag,vflag,newu,newv)


      use aAdjKeep

      include "common.h"

      integer :: i, j, k,l, meshf,uflag,vflag,wflag,newu,newv,
     &  neww
      meshf = 14
      
c     
c...  Read in preliminary information
c     
      open (meshf, file='knot.dat', status='old')
      read (meshf,*)  uflag, vflag! which directions to insert into
      read (meshf,*)  newu, newv! number of new knots in each direction

      !write(*,*) uflag, vflag
      !write(*,*) newu, newv
c     
c...  Read knot vectors
c     
      if (uflag.eq.1) then
        allocate(x_knot_u(newu))
        read(meshf,*) (x_knot_u(i), i = 1, newu) 
      endif
      
      if (vflag.eq.1) then
        allocate(x_knot_v(newv))
        read(meshf,*) (x_knot_v(i), i = 1, newv) 
      endif
      
      
      return
      end
    
