

      use aAdjKeep
      include "common.h"


!
!     Please NOTE that this refinement routine does cannot handle
!       meshes with non-zero prescribed displacements,
!


c     -------------------------------------------------------
      integer i, icount, new_MCP, new_NCP, new_NNODZ, new_NEL,
     &     new_NEDJ, jcount, j, uflag, vflag
      integer, allocatable :: new_IEN(:,:), new_INN(:,:), new_IBC(:,:), 
     &     new_BdryIEN(:,:), new_EDJ_OR(:), new_IBC_EDJ(:,:),
     &     EDJ_CON(:), FACE_CON(:), new_IPER(:)
      real(8), allocatable :: new_U_KNOT(:), new_V_KNOT(:), 
     &     new_B_NET(:,:,:),
     &     new_BW(:,:,:), Pw_1(:,:), Pw_2(:,:),
     &     temp(:), temp2(:,:,:),new_LD_EDJ(:,:), new_LD_FACE(:,:),
     &     new_EY(:), new_NU(:),new_DIR_BC(:,:),
     &     a1(:), b1(:), a2(:), b2(:), c1(:,:), c2(:,:)
      real(8) u1, u2, v1, v2, c1_norm, c2_norm, user_weight
c     -------------------------------------------------------



c...  Get initial mesh data
      call input



c...  Form projective control net
      BW = B_NET
      do i = 1,NSD
         BW(:,:,i) = BW(:,:,i)*B_NET(:,:,NSD+1)
      enddo




c...  Figure out number of knots to be inserted in each direction
c     and new knot values

      call input_knots_3D(uflag,vflag,icount,jcount)
      
      new_MCP = MCP+icount      ! new number of control points in u
      allocate(new_U_KNOT(new_MCP+P+1))
      
      new_NCP = NCP+jcount      ! new number of control points in v
      allocate(new_V_KNOT(new_NCP+Q+1))



c...  Set number of global nodes
      new_NNODZ = new_MCP*new_NCP
c...  Set number of elements
      new_NEL = (new_MCP-P)*(new_NCP-Q)



c
c...  Form new Control Net in Projective coordinates
c
      allocate(temp2(new_MCP,NCP,NSD+1))
      allocate(new_BW(new_MCP,new_NCP,NSD+1))
      temp2 = 0d+0
      new_BW = 0d+0
      
c...  refine first in u
      
!      write(*,*) uflag, icount
 !     write(*,*) x_knot_u

  !    write(*,*) vflag, jcount
   !   write(*,*) x_knot_v
      
      if (uflag.eq.1) then
         
         call RefineKnotVectSurf(MCP,P,U_KNOT,NCP,Q,V_KNOT, 
     &        BW,x_knot_u,icount,1,temp2,new_U_KNOT,V_KNOT)
         
      else

         temp2 = BW
         new_U_KNOT = U_KNOT
         
      endif
      
c...  now refine in v
      if (vflag.eq.1) then
         
         call RefineKnotVectSurf(new_MCP,P,new_U_KNOT,NCP,Q,V_KNOT, 
     &        temp2,x_knot_v,jcount,0,new_BW,new_U_KNOT,new_V_KNOT)
         
      else
         
         new_BW = temp2
         new_V_KNOT = V_KNOT
         
      endif
      
      deallocate(temp2)




c...  Convert projective coordinates into physical coordinates
      allocate(new_B_NET(new_MCP,new_NCP,NSD+1))
      new_B_NET = new_BW
      do i = 1,NSD
         new_B_NET(:,:,i) = new_B_NET(:,:,i)/new_B_NET(:,:,NSD+1)
      enddo



c
c     At this point we have a new control net and new knot vectors in each
c       direction. We must now create connectivities such that we may
c       transfer load and boundary conditions, periodicities, etc. from the
c       original mesh to the new one.


!      new_NEDJ = 2*(new_MCP-P)*(1 - CLOSED_V_flag) + 
!      &     2*(new_NCP-Q)*(1-CLOSED_U_flag) ! new number of edges





c     We need the connectivities from our original mesh in order to 
c      assign old data to the correct locations in the new mesh.
!      call genIEN_INN
!      call gen_BdryIEN




c     Generate EDJ_CON matrix connecting old edges to new ones. This is 
c      structured oppositely from IJ_CON as now EDJ_CON(new_edj_num) equals
c      the old edge number rather than vice versa.

!      allocate(EDJ_CON(new_NEDJ))
!      EDJ_CON = 1
ccc      call gen_EDJ_CON(EDJ_CON, new_NEDJ)


      !write(*,*) EDJ_CON



c     Generate FACE_CON matrix connecting old elements to new ones. This is 
c      structured oppositely from IJ_CON as well. FACE_CON(new_el_num) equals
c      the old element number rather than vice versa.

!      allocate(FACE_CON(new_NEL))
!      FACE_CON = 1
ccc      call gen_FACE_CON(FACE_CON, new_NEL, new_MCP, new_NCP)





c...  Create new IEN and INN arrays
c        also return INN_inv which will consume i and j NURBS coordinates
c        and will return the global node number

!	allocate(new_INN(new_MCP*new_NCP,2))
!	allocate(new_IEN((new_MCP-P)*(new_NCP-Q),(P+1)*(Q+1)))
!        call gen_new_IEN_INN(new_MCP,new_NCP,new_IEN,
!     &       new_INN,new_NEL,new_NNODZ)




      
c...  Also create new boundary connectivity arrays
!	allocate(new_BdryIEN(new_NEDJ, max(P+1,Q+1)))
!	allocate(new_EDJ_OR(new_NEDJ))
!        call gen_new_BdryIEN(new_NEDJ, new_NNODZ,
!     &       new_BdryIEN, new_EDJ_OR,
!     &       new_MCP, new_NCP)


c
c...  Create new IBC array assign edge data
c
!      allocate(new_IBC(new_NNODZ,NSD))
!      allocate(new_IBC_EDJ(new_NEDJ,NSD))
!      allocate(new_IPER(new_NNODZ))
!      allocate(new_LD_EDJ(new_NEDJ,NSD))
!      allocate(new_DIR_BC(new_NNODZ,NSD))
c...  FIXME prescribed displacements all zero for now...
!      new_DIR_BC = 0d+0
!      call gen_EDJ_DATA(new_NNODZ,new_NEDJ,EDJ_CON,new_EDJ_OR, 
!     &     new_BdryIEN, new_IPER,  new_IBC, new_IBC_EDJ, new_LD_EDJ,
!     &     new_MCP,new_NCP, new_INN, new_B_NET,new_U_KNOT, new_V_KNOT)


c
c...  Assign element data
c
!      allocate(new_LD_FACE(new_NEL,NSD))
!      allocate(new_EY(new_NEL))
!      allocate(new_NU(new_NEL))
!      call gen_FACE_DATA(new_NEL, FACE_CON, new_LD_FACE, new_EY, new_NU)


c     ------------------------------------------------------------------


c
c...  Now we have assembled all of the new structures, we may replace
c       the old ones and write our results to refined_mesh.dat
c


c     Deallocate old structures and reallocate with new dimensions
      deallocate(U_KNOT)    
      deallocate(V_KNOT)
      deallocate(B_NET)
      deallocate(BW)
!      deallocate(IPER)
!      deallocate(IBC)
!      deallocate(DIR_BC)
!      deallocate(IBC_EDJ)
!      deallocate(LD_EDJ)
!      deallocate(LD_FACE)
!      deallocate(EY)
!      deallocate(NU)
c$$$     deallocate(IEN) ! for the time being, IEN and INN don't need 
c$$$     deallocate(INN) !  to be written as they are not part of mesh.dat

c...  assign common variables to new values
      NNODZ = new_NNODZ
      MCP = new_MCP
      NCP = new_NCP
!      NEDJ = new_NEDJ
      NEL = new_NEL



      allocate(U_KNOT(MCP+P+1))
      U_KNOT = new_U_KNOT
      allocate(V_KNOT(NCP+Q+1))
      V_KNOT = new_V_KNOT
      allocate(B_NET(MCP,NCP,NSD+1))
      B_NET = new_B_NET
      allocate(BW(MCP,NCP,NSD+1))
      BW = new_BW
!      allocate(IPER(NNODZ))
!      IPER = new_IPER
!      allocate(IBC(NNODZ,NSD))
!      IBC = new_IBC
!      allocate(DIR_BC(NNODZ,NSD))
!      DIR_BC = new_DIR_BC
!      allocate(IBC_EDJ(NEDJ,NSD))
!      IBC_EDJ = new_IBC_EDJ
!      allocate(LD_EDJ(NEDJ,NSD))
!      LD_EDJ = new_LD_EDJ
!      allocate(LD_FACE(NEL,NSD))
!      LD_FACE = new_LD_FACE
!      allocate(EY(NEL))
!      EY = new_EY
!      allocate(NU(NEL))
!      NU = new_NU
c$$$      allocate(IEN(NEL,(P+1)*(Q+1)))
c$$$      IEN = new_IEN
c$$$      allocate(INN(NNODZ,2))
c$$$      INN = new_INN
      



c...  Write to file refined_mesh.dat
      call output

      end
