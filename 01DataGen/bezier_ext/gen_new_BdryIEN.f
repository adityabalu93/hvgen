c
c       This subroutine generates the NEW BdryIEN matrix, which relates edge 
c       numbers and local node numbers to the appropriate global node numbers. 
c       It will also generate EDJ_OR which will contain the orientations
c       of a given edge.
c
c
c       July 10, 2003
c
c       J. Austin Cottrell
c       CAM Graduate Student
c       Institute for Computational Engineering Science
c       The University of Texas at Austin

 
	
	subroutine gen_new_BdryIEN(new_NEDJ, new_NNODZ,
     &     new_BdryIEN,new_EDJ_OR, new_MCP, new_NCP)
	
	
	use aAdjKeep	
c       
	include "common.h"	! common file defines all variables
c       
c...    Local variables
	integer i, j, edj, new_NEDJ, new_NNODZ, new_MCP, new_NCP
	integer new_BdryIEN(new_NEDJ, max((P+1),(Q+1))), 
     &       new_EDJ_OR(new_NEDJ)
	

c       
c...    Initialize matrices and variables
c       
	new_BdryIEN = 0
	new_EDJ_OR = 0
        edj = 0
	
c       
c...    Loop through edges assigning numbers and filling out matrices
c         Edge numbers will be assigned sequentially while traversing
c         the boundary in a counter clockwise direction
c       
	if (CLOSED_V_flag.ne.1) then
	   do i = 1,new_MCP-P
	      edj = edj + 1
	      new_EDJ_OR(edj) = 1	! 1 indicates edge on "bottom" of grid
	      do j = 0,P
		 new_BdryIEN(edj,j+1) = edj+P-j
	      enddo
	   enddo
	   if (CLOSED_U_flag.ne.1) then
	      edj = edj+(new_NCP-Q)
	   endif
	   do i = 1,new_MCP-P
	      edj = edj+1
	      new_EDJ_OR(edj) = 3	! 3 indicates edge on "top" of grid
	      do j = 0,P
		 new_BdryIEN(edj,j+1) = new_NNODZ - i - j + 1
	      enddo
	   enddo
	   edj = new_MCP - P
	endif

	if (CLOSED_U_flag.ne.1) then
	   do i = 1,new_NCP-Q
	      edj = edj+1
	      new_EDJ_OR(edj) = 2 ! 2 indicates edge on "right" side of grid
	      do j = 0,Q
		 new_BdryIEN(edj,j+1) = (i-j+Q)*new_MCP 
	      enddo 
	   enddo
	   if (CLOSED_V_flag.ne.1) then
	      edj = edj+(new_MCP-P)
	   endif
	   do i = 1,new_NCP-Q
	      edj = edj+1
	      new_EDJ_OR(edj) = 4 ! 4 indicates edge on "left" side of grid
	      do j = 0,Q
		 new_BdryIEN(edj,j+1) = new_NNODZ-new_MCP+1 -
     &                (i+j-1)*new_MCP
	      enddo
	   enddo
	endif
	
	
	return
	end
