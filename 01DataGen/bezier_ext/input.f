
      subroutine input

c------------------------------------------------------------------------c
c                                                                        c
c        Subroutine to open file and read vertex, edge                   c
c        and element numbers. Initialze parameters of interest.          c
c                                                                        c
c------------------------------------------------------------------------c

      use aAdjKeep
      implicit none
      include "common.h"

      integer :: i, j, k, l, meshf
      meshf = 11

c
c...  Read in preliminary information
c
      open (meshf, file='mesh.dat', status='old')
      read (meshf,*)  NSD       ! number of spatial dimensions
      read (meshf,*)  P, Q      ! degree of curves in u then v direction
      read (meshf,*)  MCP, NCP  ! number of control points in each direction

c
c...  Allocate arrays for knot vectors and for control net
c
      allocate(U_KNOT(MCP+P+1))    
      allocate(V_KNOT(NCP+Q+1))
      allocate(B_NET(MCP,NCP,NSD+1))
      allocate(BW(MCP,NCP,NSD+1))

c
c...  Read knot vectors and control net
c
      read(meshf,*) (U_KNOT(i), i = 1, MCP+P+1)
      read(meshf,*) (V_KNOT(i), i = 1, NCP+Q+1)


      do j = 1, NCP
        do i = 1, MCP
          read(meshf,*) (B_NET(i,j,l), l = 1, NSD+1)
        end do
      end do
      
      read(meshf,*) Ptype, Pname
      
      
            
c     
c...  Set number of global nodes
      NNODZ = MCP*NCP
c...  Set number of elements
      NEL = (MCP-P)*(NCP-Q)
      write(*,*) "NEL is ", NEL
c
c...  Read in Master/Slave relationships. These arise from periodic boundary
c       conditions, corners in the geometry, or any other case where two
c       control points must always remain equal.
c
!      allocate(IPER(NNODZ))
!      do i = 1, NNODZ
!         read (meshf,*)  IPER(i) 
!      enddo

c
c...  Read in Boundary/Edge information
c
!      read (meshf,*) CLOSED_U_flag, CLOSED_V_flag
                                ! flag determines if surface is closed
                                !  in either direction

c...  Set number of Boundary Edges 
!      NEDJ = 2*(MCP-P)*(1 - CLOSED_V_flag) + 2*(NCP-Q)*(1-CLOSED_U_flag)

c...  Read in boundary condition indicator at each node, in each direction
c     0 - nothing, 1 - Dir., 2 - Neu., 3 - Periodic
!      allocate(IBC(NNODZ,NSD))
!      IBC = 0
!      do i = 1,NNODZ
!         read(meshf,*) (IBC(i,j), j = 1, 3)
!      end do

c...  Read in Nodal displacements for inhomogeneous Dirichlet nodes.
!      allocate(DIR_BC(NNODZ,NSD))
!      DIR_BC = 0d+0 
!      do i = 1, NNODZ
!         read (meshf,*) (DIR_BC(i,j), j = 1, 3)
!      end do

      

c...  Read in boundary condition indicator on each edge, in each direction
c     0 - nothing, 1 - constraint, 2 - load
!      allocate(IBC_EDJ(NEDJ,NSD))
!      IBC_EDJ = 0
!      do i = 1,NEDJ
!         read (meshf,*) (IBC_EDJ(i,j), j = 1, 3)
!      end do


c...  Read in actual load data on each edge
c      This assumes (for the time being) that the load is contant
c      across each edge. This will be modified in the future!
!      allocate (LD_EDJ(NEDJ,NSD))
!      LD_EDJ = 0d+0          
!      do i = 1,NEDJ
!         read (meshf,*) (LD_EDJ(i,j), j = 1, 3)
!      end do


     
c
c...  Read in body force loads on elements. This assumes constant
c       force across each element. We can modify this in the future.
!      allocate(LD_FACE(NEL,NSD))
!      LD_FACE = 0d+0
!      do i = 1,NEL
!         read (meshf,*) (LD_FACE(i,j), j = 1, 3)
!      end do




      
c
c...  Read in material data
c

c...  Young's Modulus in each element
!      allocate(EY(NEL))
!      do i = 1,NEL
!         read (meshf,*) EY(i)
!      enddo

c... Poisson's ratio in each element
!      allocate(NU(NEL))
!      do i = 1,NEL
!         read (meshf,*) NU(i)
!      enddo
         




      return
      end
