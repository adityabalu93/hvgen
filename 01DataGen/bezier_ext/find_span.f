
      subroutine find_span(pl,ml,u,u_knotl,i)


c purpose: This function calculates the index of the span in which the parameter
c             u resides. That is, if u exists in [u_i, u_i+1) then find_span 
c             will return the parameter i. The major exception is if u is equal
c             to the least upper bound of the last nontrivial interval, in which
c             case the returned index i indicates u is in the closed interval
c             [u_i, u_i+1].
c
c
c Algorithm from Piegl, Les. "The NURBS Book". Springer-Verlag: 
c    Berlin 1995; p. 68.
c
c
c June 17, 2003
c J. Austin Cottrell
c CES Graduate Student
c Texas Institute for Computational Engineering Science
c University of Texas at Austin


      integer pl, ml, i, low,mid, high
      real(8) u, u_knotl(pl+ml+1)


      if (u.eq.u_knotl(ml+pl+1)) then
         i = ml              ! special case, u in [u_i, u_i+1]
      else
         low = pl
         high = ml + 1
         mid = (low + high)/2
         do
            if ((u.ge.u_knotl(mid)).and.(u.lt.u_knotl(mid + 1))) exit
            if (u.lt.u_knotl(mid)) then
               high = mid
            else
               low = mid
            endif
            mid = (low + high)/2
         enddo
         
         i = mid
      endif
      

      return
      end
