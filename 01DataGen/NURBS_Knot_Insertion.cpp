#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mex.h"

#define dim  4 // dimension

// Algorithm A2.1 (page 68)
int FindSpan(int n, int p, double u, double *U)
{
      if (u == U[n+1])
            return n; // Special case
      int low  = p;
      int high = n+1; 
      // Do binary search
      int mid = (low + high)/2;
      while (u < U[mid] || u >= U[mid+1]){
            if (u < U[mid])
                  high = mid;
            else
                  low = mid;
            mid = (low + high)/2;
      }
      return mid;
}

// // Algorithm A2.2 (page 70)
// void BasisFuns(int i, double u, int p, double* U, double* N)
// {
//       double *left  = new double [p+1];
//       double *right = new double [p+1];
//       double saved, temp;
//       N[0] = 1.0;
//       for (int j = 1; j <= p; j += 1){
//             left[j] = u-U[i+1-j];
//             right[j] = U[i+j]-u;
//             saved = 0.0;
//             for (int r = 0; r < j; r += 1){
//                   temp = N[r]/(right[r+1] + left[j-r]);
//                   N[r] = saved + right[r+1]*temp;
//                   saved = left[j-r]*temp;
//             }
//             N[j] = saved;
//       }
//       
//       delete[] left;
//       delete[] right;
// }
// 
// // Algorithm A3.1 (page 82)
// void CurvePoint(int n, int p, double* U, double *P, double u, double *C)
// {
//       double *N = new double [p+1];
//       int span = FindSpan(n, p, u, U);
//       BasisFuns(span, u, p, U, N);
//       
//       for (int d = 0; d < dim; d += 1){
//             C[d] = 0;
//             for (int i = 0; i <= p; i += 1){
//                 C[d] += N[i]*P[(span-p+i)*dim + d];  
//             }
//       }
//       delete[] N;
// }

// Compute u, k, s, r for "CurveKnotIns"
void Compute_uksr(int n, int p, double* UP, double tor, double &u, int uk, int &k, int &s, int &r)
{
      int mp = n + p + 1;
      int mininx;
      double minv = 1e10, temp;
      
      s = 0;
      
      for (int i = 0; i <= mp; i += 1){
            temp = fabs(UP[i]-u);
            if (minv >  temp){
                  minv =  temp;
                  mininx = i;
            }
      }
      if (tor > fabs(UP[mininx]-u)){
            u = UP[mininx];
            s += 1;
            
            for (int i = mininx-1; i >= 0; i -= 1){
                  if (tor > fabs(UP[i]-u)){
                        s += 1;
                  }
            }
            for (int i = mininx + 1; i <= mp; i += 1){
                  if (tor > fabs(UP[i]-u)){
                        s += 1;
                  }
            }
      }
      
      k = FindSpan(n, p, u, UP);
      
      // r+s <= p
      r = uk;
      if (r + s > p)
            r = p - s;

}

void CurveKnotIns(int np, int p, double* UP, double* Pw, double u, int k, int s, int r, int &nq, double *UQ, double *Qw)
{
      int mp = np + p + 1;
      nq = np + r;
      
      double *Rw = new double [(nq+1)*dim];
      
      // Load new knot vector
      for (int i = 0; i <= k; i += 1)
            UQ[i] = UP[i];
      for (int i = 1; i <= r; i += 1)
            UQ[k+i] = u;
      for (int i = k+1; i <= mp; i += 1)
            UQ[i+r] = UP[i];
      // Save unaltered control points
      for (int d = 0; d < dim; d += 1){
            for (int i = 0; i <= k-p; i += 1)
                  Qw[i*dim + d] = Pw[i*dim + d];
            for (int i = k-s; i <= np; i += 1)
                  Qw[(i+r)*dim + d] = Pw[i*dim + d];
            for (int i = 0; i <= p-s; i += 1)
                  Rw[i*dim + d] = Pw[(k-p+i)*dim + d];
      }
      
      int L;
      double alpha;
      
      // Insert the knot r times
      for (int j = 1; j <= r; j += 1){
            L = k-p+j;
            for (int i = 0; i <= p-j-s; i += 1){
                  alpha = (u-UP[L+i])/(UP[i+k+1]-UP[L+i]);
                  for (int d = 0; d < dim; d += 1)
                        Rw[i*dim + d] = alpha*Rw[(i+1)*dim + d] + (1.0-alpha)*Rw[i*dim + d];
            }
            for (int d = 0; d < dim; d += 1){
                  Qw[L*dim + d] = Rw[0*dim + d];
                  Qw[(k+r-j-s)*dim + d] = Rw[(p-j-s)*dim + d];
            }
      }
      // Load remaining control points
      for (int d = 0; d < dim; d += 1)
            for (int i = L+1; i < k-s; i += 1)
                  Qw[i*dim + d] = Rw[(i-L)*dim + d];
      
      delete [] Rw;
      
}

// Main funciton
void mexFunction(
		 int          nlhs,
		 mxArray      *plhs[],
		 int          nrhs,
		 const mxArray *prhs[]
		 )
{

  if (nrhs != 7) {
      mexErrMsgIdAndTxt("MATLAB:mexcpp:nargin", 
      "MEXCPP requires 7 input arguments.");
  }
  
  int     nP = (int)mxGetScalar(prhs[0]);
  int     p  = (int)mxGetScalar(prhs[1]);
  double* UP_in  = (double *) mxGetPr(prhs[2]);
  double* Pw_in  = (double *) mxGetPr(prhs[3]);
  double  tor  = (double) mxGetScalar(prhs[4]);
  double* inU  = (double *) mxGetPr(prhs[5]);
  int*    inUr  = (int *) mxGetData(prhs[6]);
  
  int n1 = mxGetN(prhs[5]);
  int n2 = mxGetN(prhs[6]);
  
  if (n1 != n2){
      mexErrMsgIdAndTxt("MATLAB:mexcpp:nargin","Length of inU and inUr are not consistant.");
  }
  
  int n = n1;
  double u, w;
  int k, s, r, nq;
  
  int maxr = 0, maxm;
  for (int i = 0; i < n; i += 1){
      maxr += inUr[i];
  }
  
  maxm = nP + p + maxr + 1;
  
  double *UQ = new double [maxm];
  double *Qw = new double [maxm * dim];
  
  double *UP = new double [maxm];
  double *Pw = new double [maxm * dim];
  
  for (int a = 0; a <= nP+p; a += 1){
      UP[a] = UP_in[a];
  }
  
  for (int a = 0; a < nP; a += 1){
        for (int d = 0; d < dim; d += 1){
            
            Pw[a*dim + d] = Pw_in[a*dim + d];
        }
        
        w = Pw[a*dim + dim-1];
        for (int d = 0; d < dim-1; d += 1){
            Pw[a*dim + d] *= w;
        }
  }
  
  
  for (int i = 0; i < n; i += 1){
        
      Compute_uksr(nP-1, p, UP, tor, inU[i], inUr[i], k, s, r);
      CurveKnotIns(nP-1, p, UP, Pw, inU[i], k, s, r, nq, UQ, Qw);
      
      nP = nq + 1;
      for (int a = 0; a <= nP+p; a += 1){
            UP[a] = UQ[a];
      }
      
      for (int a = 0; a < nP; a += 1){
            for (int d = 0; d < dim; d += 1){
                  Pw[a*dim + d] = Qw[a*dim + d];
            }
      }
  }
  
  plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
  plhs[1] = mxCreateDoubleMatrix(1,nP+p+1,mxREAL);
  plhs[2] = mxCreateDoubleMatrix(nP,dim,mxREAL);
  
  *mxGetPr(plhs[0]) = nP;
  
  for (int a = 0; a <= nP+p; a += 1){
      *(mxGetPr(plhs[1])+a) = UP[a];
  }
  
  for (int a = 0; a < nP; a += 1){
      
        w = Pw[a*dim + dim-1];
        for (int d = 0; d < dim-1; d += 1){
            Pw[a*dim + d] /= w;
        }
      
        for (int d = 0; d < dim; d += 1){
            *(mxGetPr(plhs[2]) + d*nP + a) = Pw[a*dim + d];
        }
  }
  
  delete[] Qw;
  delete[] UQ;
  delete[] UP;
  delete[] Pw;
 
  return;
}
