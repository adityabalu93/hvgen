
      subroutine input

c------------------------------------------------------------------------c
c                                                                        c
c        Subroutine to open file and read vertex, edge                   c
c        and element numbers. Initialze parameters of interest.          c
c                                                                        c
c------------------------------------------------------------------------c

      use aAdjKeep

      include "common.h"

      integer  i, j, k, meshf
      real(8) dh(3)
      meshf = 11

c
c...  Read in preliminary information
c
      open (meshf, file='mesh.dat', status='old')
      read (meshf,*)  NSD       ! number of spatial dimensions
      read (meshf,*)  P, Q      ! degree of curves in u then v direction
      read (meshf,*)  MCP, NCP  ! number of control points in each direction

c
c...  Allocate arrays for knot vectors and for control net
c
      allocate(U_KNOT(MCP+P+1))    
      allocate(V_KNOT(NCP+Q+1))
      allocate(B_NET(MCP,NCP,NSD+1))
      allocate(BW(MCP,NCP,NSD+1))

c
c...  Read knot vectors and control net
c
      read(meshf,*) (U_KNOT(i), i = 1, MCP+P+1)
      read(meshf,*) (V_KNOT(i), i = 1, NCP+Q+1)


      do j = 1, NCP
        do i = 1, MCP
          read(meshf,*) (B_NET(i,j,l), l = 1, NSD+1)
        end do
      end do
      
      read (meshf,*)  ID
      !write(*,*) "id", ID
      
      close(meshf)
      
	  !!! inch to m	  
	  !B_NET(:,:,1:3) = B_NET(:,:,1:3) * 0.0254d0

C       open (meshf, file='height.dat', status='old')
C       write(*,*) ' B_NET(2,3,:) = ',  B_NET(2,3,:)
C       read (meshf,*)  dh
C
C       !!! inch to m
C       !dh = dh * 0.0254d0
C
C       write(*,*) 'After adding dh = ', dh
C       B_NET(2,3,3) = B_NET(2,3,3) + dh(1)
C       B_NET(2,3,1) = B_NET(2,3,1) + dh(2)
C       B_NET(3,3,3) = B_NET(3,3,3) + dh(3)
C       write(*,*) 'B_NET(2,3,:) = ',  B_NET(2,3,:)
C       close(meshf)
C
C       B_NET(2,2,1:3) = 0.5d0 * (B_NET(2,3,1:3) + B_NET(2,1,1:3))
C
C       B_NET(3,2,1:3) = 0.5d0 * (B_NET(3,3,1:3) + B_NET(3,1,1:3))
	  
c...  Set number of global nodes
      NNODZ = MCP*NCP
c...  Set number of elements
      NEL = (MCP-P)*(NCP-Q)
   
c
C c...  Read in Master/Slave relationships. These arise from periodic boundary
C c       conditions, corners in the geometry, or any other case where two
C c       control points must always remain equal.
C c
C       allocate(IPER(NNODZ))
C       do i = 1, NNODZ
C          read (meshf,*)  IPER(i)
C       enddo
C
C c
C c...  Read in Boundary/Edge information
C c
C       read (meshf,*) CLOSED_U_flag, CLOSED_V_flag
C                                 ! flag determines if surface is closed
C                                 !  in either direction
C
C c...  Set number of Boundary Edges
C       NEDJ = 2*(MCP-P)*(1 - CLOSED_V_flag) + 2*(NCP-Q)*(1-CLOSED_U_flag)
C
C c...  Read in boundary condition indicator at each node, in each direction
C c     0 - nothing, 1 - Dir., 2 - Neu., 3 - Periodic
C       allocate(IBC(NNODZ,NSD))
C       IBC = 0
C       do i = 1,NNODZ
C          read (meshf,'(I2)', ADVANCE = "NO") IBC(i,1)
C          read (meshf,'(I2)', ADVANCE = "NO") IBC(i,2)
C          read (meshf,'(I2)', ADVANCE = "YES") IBC(i,3)
C       enddo
C
C c...  Read in Nodal displacements for inhomogeneous Dirichlet nodes.
C       allocate(DIR_BC(NNODZ,NSD))
C       DIR_BC = 0d+0
C       do i = 1, NNODZ
C          read (meshf,"(F13.9)", ADVANCE = "NO") DIR_BC(i,1)
C          read (meshf,"(F13.9)", ADVANCE = "NO") DIR_BC(i,2)
C          read (meshf,"(F13.9)", ADVANCE = "YES") DIR_BC(i,3)
C       enddo
C
C
C
C c...  Read in boundary condition indicator on each edge, in each direction
C c     0 - nothing, 1 - constraint, 2 - load
C       allocate(IBC_EDJ(NEDJ,NSD))
C       IBC_EDJ = 0
C       do i = 1,NEDJ
C          read (meshf,'(I2)', ADVANCE = "NO") IBC_EDJ(i,1)
C          read (meshf,'(I2)', ADVANCE = "NO") IBC_EDJ(i,2)
C          read (meshf,'(I2)', ADVANCE = "YES") IBC_EDJ(i,3)
C       enddo
C
C
C c...  Read in actual load data on each edge
C c      This assumes (for the time being) that the load is contant
C c      across each edge. This will be modified in the future!
C       allocate (LD_EDJ(NEDJ,NSD))
C       LD_EDJ = 0d+0
C       do i = 1,NEDJ
C          read (meshf,"(F13.9)", ADVANCE = "NO") LD_EDJ(i,1)
C          read (meshf,"(F13.9)", ADVANCE = "NO") LD_EDJ(i,2)
C          read (meshf,"(F13.9)", ADVANCE = "YES") LD_EDJ(i,3)
C       enddo
C
C
C
C c
C c...  Read in body force loads on elements. This assumes constant
C c       force across each element. We can modify this in the future.
C       allocate(LD_FACE(NEL,NSD))
C       LD_FACE = 0d+0
C       do i = 1,NEL
C          read (meshf,"(F13.9)", ADVANCE = "NO") LD_FACE(i,1)
C          read (meshf,"(F13.9)", ADVANCE = "NO") LD_FACE(i,2)
C          read (meshf,"(F13.9)", ADVANCE = "YES") LD_FACE(i,3)
C       enddo
C
C
C
C
C
C c
C c...  Read in material data
C c
C
C c...  Young's Modulus in each element
C       allocate(EY(NEL))
C       do i = 1,NEL
C          read (meshf,*) EY(i)
C       enddo
C
C c... Poisson's ratio in each element
C       allocate(NU(NEL))
C       do i = 1,NEL
C          read (meshf,*) NU(i)
C       enddo
         




      return
      end
