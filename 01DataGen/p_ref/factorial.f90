
      RECURSIVE SUBROUTINE factorial(n,factorial_n)

      integer, intent(in) :: n
      real*8, intent(out) :: factorial_n

      select case(n)
      case(0)
         factorial_n = 1d+0
      case(1:)
         call factorial(n-1,factorial_n)
         factorial_n = n*factorial_n
      end select

      end subroutine factorial
