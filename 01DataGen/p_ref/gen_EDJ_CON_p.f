      subroutine gen_EDJ_CON_p(EDJ_CON,new_P,new_Q,new_NEDJ,new_EDJ_OR)

      use aAdjKeep
      include "common.h"

      integer e, old_e, new_P,new_Q,new_NEDJ, ni, nj,i
      integer EDJ_CON(new_NEDJ),new_EDJ_OR(new_NEDJ)




      e = 0
      do old_e = 1, NEDJ
         if ((EDJ_OR(old_e).eq.1).or.(EDJ_OR(old_e).eq.3)) then
            ni = INN(BdryIEN(old_e,1),1)
            if (U_KNOT(ni).ne.U_KNOT(ni+1)) then
               if (ni.lt.MCP) then
                  do i = 1,new_P-P+1
                     e = e+1
                     EDJ_CON(e) = old_e
                     new_EDJ_OR(e) = EDJ_OR(old_e)
                  enddo
               else
                  e = e+1
                  EDJ_CON(e) = old_e
                  new_EDJ_OR(e) = EDJ_OR(old_e)
               endif
            else
               e = e+1
               EDJ_CON(e) = EDJ_CON(e-1)
               new_EDJ_OR(e) = EDJ_OR(old_e)
            endif
         else
            nj = INN(BdryIEN(old_e,1),2)
            if (V_KNOT(nj).ne.V_KNOT(nj+1)) then
               if (nj.lt.NCP) then
                  do i = 1,new_Q-Q+1
                     e = e+1
                     EDJ_CON(e) = old_e
                     new_EDJ_OR(e) = EDJ_OR(old_e)
                  enddo
               else
                  e = e+1
                  EDJ_CON(e) = old_e
                  new_EDJ_OR(e) = EDJ_OR(old_e)
               endif
            else
               e = e+1
               EDJ_CON(e) = EDJ_CON(e-1)
               new_EDJ_OR(e) = EDJ_OR(old_e)
            endif
         endif
      enddo
      
      return
      end
