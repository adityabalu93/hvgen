c       This subroutine generates the NEW IEN matrix, which relates element numbers
c       and local node numbers to the appropriate global node numbers. The
c       routine also generates the INN matrix, which relates global node
c       number to the "NURBS coordinates" of the node.
	
	subroutine gen_new_IEN_INN_p(new_MCP,new_NCP,new_IEN,
     &     new_INN, new_NEL, new_NNODZ,new_P,new_Q)

	
	use aAdjKeep
c       
	include "common.h"	! common file defines all variables
c       
c...    Local variables
	integer :: i, j, k, l, g, e, gtemp, ln, new_NEL, new_NNODZ,
     &       new_MCP, new_NCP, new_IEN(new_NEL,(new_P+1)*(new_Q+1)),
     &       new_INN(new_NNODZ,2),new_P,new_Q
	

c       
c...    Initialize matrices and variables
c       
	new_IEN = 0
	new_INN = 0
        g = 0
        e = 0

c       
c...    Loop through control points assigning global node
c       numbers and filling out IEN and INN as we go
c       
	do j = 1,new_NCP	! loop through control points in V direction
	   do i = 1,new_MCP	! loop through control points in U direction
	      g = g+1
	      new_INN(g,1) = i
	      new_INN(g,2) = j
	      if ((i .ge. (new_P+1)).and.(j .ge. (new_Q+1))) then
		 e = e +1
		 do l = 0,new_Q
		    do k = 0,new_P
		       gtemp = g - new_MCP*l - k
		       ln = (new_P+1)*l + k + 1
		       new_IEN(e,ln) = gtemp
		    enddo
		 enddo
	      endif
	   enddo
	enddo
        

	return
	end
