

      use aAdjKeep
      include "common.h"


!
!     Please NOTE that this refinement routine does cannot handle
!       meshes with non-zero prescribed displacements at the moment.
!       All prescribed displacements are set to zero.
!


c     -------------------------------------------------------
      integer i,dist_knots_u,dist_knots_v,t,dir,new_MCP,new_NCP,
     &     direction,u_flag,new_P,new_Q,qh, new_NNODZ,new_NEL,
     &     new_NEDJ,nh
      real*8, allocatable :: Qw(:,:,:), new_U_KNOT(:),new_V_KNOT(:),
     &     new_BW(:,:,:),Vh(:),new_B_NET(:,:,:),new_EY(:),new_NU(:),
     &     new_LD_FACE(:,:),new_LD_EDJ(:,:),new_DIR_BC(:,:)
      integer, allocatable ::  EDJ_CON(:),new_EDJ_OR(:),FACE_CON(:),
     &     new_INN(:,:),new_IEN(:,:),new_BdryIEN(:,:),new_IBC(:,:),
     &     new_IBC_EDJ(:,:),new_IPER(:)



c...  Get initial mesh data
      call input

c...  Form projective control net
      BW = B_NET
      do i = 1,NSD
         BW(:,:,i) = BW(:,:,i)*B_NET(:,:,NSD+1)
      enddo
      


c... Allow the user to specify elevation direction
      write(*,*)"Enter direction in which to degree elevate"
      write(*,*)" (1 - u direction, 2 - v direction, 3 - both):"

      if ((P < 3).and.(Q < 3)) then
        direction = 3
      else if ((P < 3).and.(Q == 3)) then
        direction = 1
      else if ((P == 3).and.(Q < 3)) then
        direction = 2
      else
        stop "degree is larger than cubic"
      end if
      write(*,*) direction


c
c... U DIRECTION ELEVATION
c
      if ((direction.eq.1).or.(direction.eq.3)) then

         dir = 1

c...  Allow user to specify degree
         write(*,*)"Enter number of degrees of elevation"
         write(*,*)"   desired in u direction:"
         t = 3 - P
         write(*,*) t
         
c...  Calculate size of new knot vector and control net
c        after refining in u
         dist_knots_u = 1
         do i = 1,MCP-P
            if (U_KNOT(P+i).ne.U_KNOT(P+i+1)) then
               dist_knots_u = dist_knots_u + 1
            endif
         enddo
         
         allocate(new_U_KNOT(MCP+P+1+dist_knots_u*t))
         
         if (direction.eq.1) then
            allocate(new_BW(MCP+(dist_knots_u-1)*t,NCP,NSD+1))
            allocate(new_V_KNOT(NCP+Q+1))
         
            call degree_elevate_surface(MCP,P,U_KNOT,NCP,Q,V_KNOT,
     &           BW,t,dir,dist_knots_u,new_BW,new_MCP,new_U_KNOT,NEW_P,
     &           new_NCP,new_V_KNOT,new_Q)
         else

            allocate(QW(MCP+(dist_knots_u-1)*t,NCP,NSD+1))
            allocate(Vh(NCP+Q+1))
            call degree_elevate_surface(MCP,P,U_KNOT,NCP,Q,V_KNOT,
     &           BW,t,dir,dist_knots_u,QW,new_MCP,new_U_KNOT,NEW_P,
     &           nh,Vh,qh)
      
         endif
      endif



c
c... V DIRECTION ELEVATION
c

      if ((direction.eq.2).or.(direction.eq.3)) then

         dir = 0

c...  Allow user to specify degree
         write(*,*)"Enter number of degrees of elevation"
         write(*,*)"   desired in v direction:"
         t = 3 - P
         write(*,*) t
         
c...  Calculate size of new knot vector and control net
c        after refining in v

         dist_knots_v = 1
         do i = 1,NCP-Q
            if (V_KNOT(Q+i).ne.V_KNOT(Q+i+1)) then
               dist_knots_v = dist_knots_v + 1
            endif
         enddo
         allocate(new_V_KNOT(NCP+Q+1+dist_knots_v*t))


         if (direction.eq.2) then
            allocate(new_BW(MCP,NCP+(dist_knots_v-1)*t,NSD+1))
            allocate(new_U_KNOT(MCP+P+1))
         
            call degree_elevate_surface(MCP,P,U_KNOT,NCP,Q,V_KNOT,
     &           BW,t,dir,dist_knots_v,new_BW,new_MCP,new_U_KNOT,NEW_P,
     &           new_NCP,new_V_KNOT,new_Q)
         else

            allocate(new_BW(MCP+(dist_knots_u-1)*t,
     &           NCP+(dist_knots_v-1)*t ,NSD+1))

            call degree_elevate_surface(new_MCP,new_P,new_U_KNOT,
     &        NCP,Q,V_KNOT,QW,t,dir,dist_knots_v,new_BW,new_MCP,
     &        new_U_KNOT,NEW_P,new_NCP,new_V_KNOT,new_Q)
            
            deallocate(QW)
            deallocate(VH)
            
          endif
        endif
        


c...  Convert projective coordinates into physical coordinates
      allocate(new_B_NET(new_MCP,new_NCP,NSD+1))
      new_B_NET = new_BW
      do i = 1,NSD
         new_B_NET(:,:,i) = new_B_NET(:,:,i)/new_B_NET(:,:,NSD+1)
      enddo

      

c
c     At this point we have a new control net and new knot vectors in each
c       direction. We must now create connectivities such that we may
c       transfer load and boundary conditions, periodicities, etc. from the
c       original mesh to the new one


      new_NNODZ = new_MCP*new_NCP
      new_NEL = (new_MCP-new_P)*(new_NCP-new_Q)
C       new_NEDJ = 2*(new_MCP-new_P)*(1 - CLOSED_V_flag) +
C      &     2*(new_NCP-new_Q)*(1-CLOSED_U_flag) ! new number of edges



c     We need the connectivities from our original mesh in order to 
c      assign old data to the correct locations in the new mesh.
C       all genIEN_INN
C       all gen_BdryIEN


c     Identify old edges with new edges to assign boundary conditions
c     EDJ_CON(new_el_num) equals the old edge number.
c     We simultaneously assign the new edge orientations.
C       allocate(EDJ_CON(new_NEDJ))
C       allocate(new_EDJ_OR(new_NEDJ))
C       call gen_EDJ_CON_p(EDJ_CON,new_P,new_Q,new_NEDJ,new_EDJ_OR)
C
C
C
C c     Generate FACE_CON matrix connecting old elements to new ones.
C c      FACE_CON(new_el_num) equals the old element number
C
C       allocate(FACE_CON(new_NEL))
C       call gen_FACE_CON_p(FACE_CON, new_NEL, new_MCP, new_NCP,
C      &     new_P,new_Q)


C c...  Create new IEN and INN arrays
C   allocate(new_INN(new_MCP*new_NCP,2))
C   allocate(new_IEN((new_MCP-new_P)*(new_NCP-new_Q),
C      &       (new_P+1)*(new_Q+1)))
C         call gen_new_IEN_INN_p(new_MCP,new_NCP,new_IEN,
C      &       new_INN,new_NEL,new_NNODZ,new_P,new_Q)
C
C
C c...  Also create new boundary connectivity arrays
C   allocate(new_BdryIEN(new_NEDJ, max(new_P+1,new_Q+1)))
C         call gen_new_BdryIEN_p(new_NEDJ, new_NNODZ,
C      &       new_BdryIEN, new_MCP, new_NCP,new_P,new_Q)



c
C c...  Create new IBC array assign edge data
C c
C       allocate(new_IBC(new_NNODZ,NSD))
C       allocate(new_IBC_EDJ(new_NEDJ,NSD))
C       allocate(new_IPER(new_NNODZ))
C       allocate(new_LD_EDJ(new_NEDJ,NSD))
C       allocate(new_DIR_BC(new_NNODZ,NSD))
C c...  FIXME prescribed displacements all zero for now...
C       new_DIR_BC = 0d+0
C       call gen_EDJ_DATA_p(new_NNODZ,new_NEDJ,EDJ_CON,new_EDJ_OR,
C      &     new_BdryIEN, new_IPER,  new_IBC, new_IBC_EDJ, new_LD_EDJ,
C      &     new_MCP,new_NCP, new_INN, new_B_NET,new_U_KNOT, new_V_KNOT,
C      &     new_P,new_Q)



c
C c...  Assign element data
C c
C       allocate(new_LD_FACE(new_NEL,NSD))
C       allocate(new_EY(new_NEL))
C       allocate(new_NU(new_NEL))
C       call gen_FACE_DATA_p(new_NEL, FACE_CON, new_LD_FACE,
C      &     new_EY, new_NU)


c     ------------------------------------------------------------------


c
c...  Now we have assembled all of the new structures, we may replace
c       the old ones and write our results to refined_mesh.dat
c

C c     clean house
C       deallocate(EDJ_CON)
C       deallocate(FACE_CON)
C       deallocate(new_INN)
C       deallocate(new_IEN)
C       deallocate(new_EDJ_OR)

c     Deallocate old structures and reallocate with new dimensions
      deallocate(U_KNOT)    
      deallocate(V_KNOT)
      deallocate(B_NET)
      deallocate(BW)
C       deallocate(IPER)
C       deallocate(IBC)
C       deallocate(DIR_BC)
C       deallocate(IBC_EDJ)
C       deallocate(LD_EDJ)
C       deallocate(LD_FACE)
C       deallocate(EY)
C       deallocate(NU)
C       deallocate(IEN)          ! for the time being, IEN and INN donot need
C       deallocate(INN) !  to be written as they are not part of mesh.dat

c...  assign common variables to new values
      NNODZ = new_NNODZ
      MCP = new_MCP
      NCP = new_NCP
C       NEDJ = new_NEDJ
      NEL = new_NEL
      P = new_P
      Q = new_Q
      
      

      allocate(B_NET(MCP,NCP,NSD+1))
      B_NET = new_B_NET
      deallocate(new_B_NET)
      allocate(BW(MCP,NCP,NSD+1))
      BW = new_BW
      deallocate(new_BW)
      allocate(U_KNOT(MCP+P+1))
      U_KNOT = new_U_KNOT
      deallocate(new_U_KNOT)
      allocate(V_KNOT(NCP+Q+1))
      V_KNOT = new_V_KNOT
      deallocate(new_V_KNOT)
C       allocate(IPER(NNODZ))
C       IPER = new_IPER
C       deallocate(new_IPER)
C       allocate(IBC(NNODZ,NSD))
C       IBC = new_IBC
C       deallocate(new_IBC)
C       allocate(DIR_BC(NNODZ,NSD))
C       DIR_BC = new_DIR_BC
C       deallocate(new_DIR_BC)
C       allocate(IBC_EDJ(NEDJ,NSD))
C       IBC_EDJ = new_IBC_EDJ
C       deallocate(new_IBC_EDJ)
C       allocate(LD_EDJ(NEDJ,NSD))
C       LD_EDJ = new_LD_EDJ
C       deallocate(new_LD_EDJ)
C       allocate(LD_FACE(NEL,NSD))
C       LD_FACE = new_LD_FACE
C       deallocate(new_LD_FACE)
C       allocate(EY(NEL))
C       EY = new_EY
C       allocate(NU(NEL))
C       NU = new_NU
C       deallocate(new_NU)
      
      call output


      end
