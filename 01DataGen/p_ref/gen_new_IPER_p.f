      subroutine gen_new_IPER_p(new_NNODZ, new_INN, new_MCP,new_NCP,
     &     new_B_NET,new_IBC,new_IPER)

      use aAdjKeep

      include "common.h"



      
      integer new_NNODZ, new_INN(new_NNODZ,2), new_IPER(new_NNODZ),
     &     i, j, new_MCP, new_NCP, ri,rj,ci,cj,new_IBC(new_NNODZ,NSD)
      real*8 tol,rP(NSD), cP(NSD), 
     &     new_B_NET(new_MCP,new_NCP,NSD+1),dist_inf



c     Initialize
      do i = 1,new_NNODZ
         new_IPER(i) = i
      enddo


c
c...  We have adopted the convention of, in all master/slave relation-
c       ships, calling the node with the higher number the master of
c       the node with the smaller number. Hence we loop backwards
c       from the higher numbered nodes downwards. Here we only check
c       to see that two nodes occupy the same point in physical space
c       and take that to be sufficient evidence of a master/slave pair.
c
c     

c     A tolerance has been hardwired in. Adjust as needed.
      tol = 1d-8

      
c     Loop over "reference node"
      do i = new_NNODZ,1,-1
            
c...     Get NURBS coordinates for reference node
         ri = new_INN(i,1)
         rj = new_INN(i,2)

c...     Get physical coordinates for reference node
         rP(1:NSD) = new_B_NET(ri,rj,1:NSD)


c...     Loop over "comparison node"
         do j = i-1,1,-1

c...        Get NURBS coordinates for comparison node
            ci = new_INN(j,1)  
            cj = new_INN(j,2)

c...        Get physical coordinates of comparison node
            cP(1:NSD) = new_B_NET(ci,cj,1:NSD)

            dist_inf = sqrt(sum((rP-cP)*(rP-cP)))

c...     Check distance between points against the tolerance
            if (dist_inf.lt.tol) then
               new_IPER(j) = new_IPER(i)
               new_IBC(j,1:NSD) = 3 ! set indicator of slave to
                                    !  denote periodic BC
            endif

         enddo
      enddo


      return
      end
   
