      subroutine gen_FACE_DATA_p(new_NEL,FACE_CON,
     &     new_LD_FACE, new_EY, new_NU)

      use aAdjKeep

      include "common.h"

      

      integer new_NEL, e
      integer FACE_CON(new_NEL)
      real*8 new_LD_FACE(new_NEL,NSD), new_EY(new_NEL), new_NU(new_NEL)





      new_LD_FACE = 0d+0
      new_EY = 0d+0
      new_NU = 0d+0



c...  Loop over faces

      do e = 1, new_NEL
         
         
c
c...  set Face Load
c
         new_LD_FACE(e,1:NSD) = LD_FACE(FACE_CON(e),1:NSD)

c
c...  set Youngs modulus
c     
         new_EY(e) = EY(FACE_CON(e))

c
c...  set Poisson's ratio
c
         new_NU(e) = NU(FACE_CON(e))

      enddo

      return
      end
