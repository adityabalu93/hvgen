
      subroutine output

c------------------------------------------------------------------------c
c                                                                        c
c        Subroutine to open file and write refined vertex, edge          c
c        and element numbers. Initialze parameters of interest.          c
c                                                                        c
c------------------------------------------------------------------------c

      use aAdjKeep
      implicit none
      include "common.h"

      integer :: i, j, k, meshref
      meshref = 12

c
c...  Write in preliminary information
c
      open(meshref, file='refined_mesh.dat', status='unknown')
      write(meshref,*) NSD       ! number of spatial dimensions
      write(meshref,"(2I4)") P, Q ! degree of curves in u then v direction
      write(meshref,"(2I6)") MCP, NCP ! number of control points
                                                  ! in each direction

c...  Read knot vectors and control net
c
      do i = 1, MCP+P+1
         if (i < MCP+P+1) then
            write(meshref,190, ADVANCE = "NO") U_KNOT(i) ! write U_KNOT
         else
            write(meshref,190, ADVANCE = "YES") U_KNOT(i) 
         endif
      enddo

      do i = 1,NCP+Q+1
         if (i < NCP+Q+1) then
            write (meshref,190, ADVANCE = "NO") V_KNOT(i) ! write V_KNOT
         else
            write (meshref,190, ADVANCE = "YES") V_KNOT(i) 
         endif
      enddo

      do j = 1,(NCP)
         do i = 1,(MCP)
            do k = 1,NSD+1
               if (k < NSD+1) then
                  write (meshref,190, ADVANCE = "NO")
     &                 B_NET(i,j,k)
               else
                  write (meshref,190, ADVANCE = "YES") 
     &                 B_NET(i,j,k)
               endif
            enddo
         enddo
      enddo
      
      !write(*,*) "id", ID

      write(meshref,*) ID, ' leaflet'

   
C c
C c...  Write in Master/Slave relationships. These arise from periodic boundary
C c       conditions, corners in the geometry, or any other case where two
C c       control points must always remain equal.
C c
C       do i = 1, NNODZ
C          write (meshref,*) IPER(i)
C       enddo
C
C c
C c...  Write in Boundary/Edge information
C c
C       write (meshref,'(2I3)') CLOSED_U_flag, CLOSED_V_flag
C
C
C c...  Write in boundary condition indicator at each node, in each direction
C c     0 - nothing, 1 - Dir., 2 - Neu., 3 - Periodic
C       do i = 1,NNODZ
C          write (meshref,'(3I2)') IBC(i,1:3)
C       end do
C
C c...  Write Dirichlet Condition array.
C       do i = 1,NNODZ
C          write (meshref,190, ADVANCE = "NO") DIR_BC(i,1)
C          write (meshref,190, ADVANCE = "NO") DIR_BC(i,2)
C          write (meshref,190, ADVANCE = "YES") DIR_BC(i,3)
C       enddo
C
C
C c...  Write in boundary condition indicator on each edge, in each direction
C c     0 - nothing, 1 - applied load
C       do i = 1,NEDJ
C          write (meshref,'(I2)', ADVANCE = "NO") IBC_EDJ(i,1)
C          write (meshref,'(I2)', ADVANCE = "NO") IBC_EDJ(i,2)
C          write (meshref,'(I2)', ADVANCE = "YES") IBC_EDJ(i,3)
C       enddo
C
C
C c...  Write in actual load data on each edge
C c      This assumes (for the time being) that the load is contant
C c      across each edge. This will be modified in the future!
C       do i = 1,NEDJ
C          write (meshref,190, ADVANCE = "NO") LD_EDJ(i,1)
C          write (meshref,190, ADVANCE = "NO") LD_EDJ(i,2)
C          write (meshref,190, ADVANCE = "YES") LD_EDJ(i,3)
C       enddo
C
C
C
C c
C c...  Write in body force loads on elements. This assumes constant
C c       force across each element. We can modify this in the future.
C       do i = 1,NEL
C          write (meshref,190, ADVANCE = "NO") LD_FACE(i,1)
C          write (meshref,190, ADVANCE = "NO") LD_FACE(i,2)
C          write (meshref,190, ADVANCE = "YES") LD_FACE(i,3)
C       enddo
C
C
C
C
C
C c
C c...  Write in material data
C c
C
C c...  Young's Modulus in each element
C       do i = 1,NEL
C          write (meshref,*) EY(i)
C       enddo
C
C c... Poisson's ratio in each element
C       do i = 1,NEL
C          write (meshref,*) NU(i)
C       enddo
         

  190 format (F19.14)

      return
      end
