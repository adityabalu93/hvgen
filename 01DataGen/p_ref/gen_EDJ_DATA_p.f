      subroutine gen_EDJ_DATA_p(new_NNODZ,new_NEDJ,EDJ_CON,new_EDJ_OR, 
     &     new_BdryIEN, new_IPER,  new_IBC, new_IBC_EDJ, new_LD_EDJ,
     &     new_MCP, new_NCP, new_INN, new_B_NET,new_U_KNOT, new_V_KNOT,
     &     new_P,new_Q)

      use aAdjKeep
      include "common.h"

      integer edj, new_NEDJ, EDJ_CON(new_NEDJ),new_NNODZ, nshb,
     &     new_P,new_Q,
     &     new_EDJ_OR(new_NEDJ), i,j, new_BdryIEN(new_NEDJ,
     &     max(new_P+1,new_Q+1)),new_INN(new_NNODZ,2),
     &     new_IPER(new_NNODZ), new_MCP, new_NCP
      real*8 new_LD_EDJ(new_NEDJ,NSD),new_B_NET(new_MCP,new_NCP,NSD+1)
      integer  new_IBC(new_NNODZ,NSD), new_IBC_EDJ(new_NEDJ,NSD)
      real*8 new_U_KNOT(new_MCP+new_P+1), new_V_KNOT(new_NCP+new_Q+1)




c...  Allocations and initialization


      new_IBC = 0
      new_IBC_EDJ = 0
      new_LD_EDJ = 0d+0




c
c...  First set periodicities. These may arise from a closed net
c       or from corners and sharp edges in physical space. As this
c       process is a bit involved, it has been placed in a separate
c       subroutine


      call gen_new_IPER_p(new_NNODZ, new_INN,
     &     new_MCP, new_NCP, new_B_NET, new_IBC, new_IPER)


c
c...  Now loop over edges and check boundary information
c


c...  loop over edges
      do edj = 1, new_NEDJ

c     check edge orientation and set local shape functions
         if ((new_EDJ_OR(edj).eq.1).or.(new_EDJ_OR(edj).eq.3)) then
            nshb = new_P+1
         else
            nshb = new_Q+1
         endif


c...  Check for Neumann conditions. This is purely an edge phenomenon,
c      so we do not need modify new_IBC, only new_IBC_EDJ.
         
         do i = 1, NSD
            
            if (IBC_EDJ(EDJ_CON(edj),i).eq.2) then
               new_IBC_EDJ(edj,i) = 2
            endif
            
         enddo
         

c...  Now check for Dirichlet conditions. Due to the fact that
c      basis functions are not completely localized, this may
c      declare some nodes constrained which were formerly set
c      to be periodic. This is okay as the condition of being 
c      constrained takes precedence over any other conditions.

c...  Check if Dirichlet conditions apply in j^th direction.

         do j = 1, NSD
            if (IBC_EDJ(EDJ_CON(edj),j).eq.1) then
               new_IBC_EDJ(edj,j) = 1
               do i = 1,nshb
                  new_IBC(new_BdryIEN(edj,i),j) = 1
                  new_IBC(new_IPER(new_BdryIEN(edj,i)),j) = 1
               ! if a slave is constrained, the master
               !    must be as well.
               enddo
            endif
         enddo

c
c...  While looping over boundary edges, assign load data.
c
         new_LD_EDJ(edj,1:NSD) = LD_EDJ(EDJ_CON(edj),1:NSD)


      enddo

c
c...  Lastly, we must loop through the nodes a final time to finish
c       our new_IBC array. At this stage it is possible that some
c       "master" nodes are constrained, while their "slaves" are only
c       labeled as periodic. We must identify these slaves as constrained
c       if we are to generate the appropriate stiffness matrices during 
c       analysis. 
c     
      do i = 1, new_NNODZ
         do j = 1, NSD
            if (new_IBC(new_IPER(i),j).eq.1) then
               new_IBC(i,j) = 1
            endif
         enddo
      enddo




      return
      end





