      subroutine degree_elevate_surface(ml,pl,u_knotl,nl,ql,v_knotl,
     &     Pw,t,dir,dist_knots,Qw,mh,Uh,ph,nh,Vh,qh)


c
c purpose: This function degree elevates by t a given NURBS surface and
c             refines projective control points Pw such that the surface
c             does not change shape.
c
c
c adapted from algorithm in Piegl, Les. "The NURBS Book". Springer-Verlag
c    Berlin 1995; p. 209.
c
c
c November 3, 2003
c J. Austin Cottrell
c CES Graduate Student
c Texas Institute for Computational Engineering Science
c University of Texas at Austin


      use aAdjKeep
      
      include "common.h"
      
      
      integer ml,pl,nl,ql,t,dir,mh,ph,nh,qh,dist_knots,U_direction,
     &     V_direction,icount,i,j,k
      real*8  u_knotl(ml+pl+1),v_knotl(nl+ql+1),
     &     Uh(ml+pl+1+dir*(dist_knots*t)),
     &     Vh(nl+ql+1+(1-dir)*(dist_knots*t)),Pw(ml,nl,NSD+1),
     &     Qw(ml+dir*(dist_knots-1)*t,nl+(1-dir)*(dist_knots-1)*t,NSD+1)
      
      real*8, allocatable :: Pwtemp(:,:),Qwtemp(:,:)


 


      U_direction = 1
      V_direction = 0

      if (dir.eq.U_direction)then
        mh = ml + (dist_knots - 1)*t
        Qw = 0.0
        allocate(Pwtemp(ml,NSD+1))
        allocate(Qwtemp(ml+(dist_knots-1)*t,NSD+1))

        do i = 1,nl
           do j = 1,ml
              do k = 1,NSD+1
                 Pwtemp(j,k) = Pw(j,i,k)
              enddo
           enddo
           
           call  degree_elevate_curve(ml,pl,u_knotl,Pwtemp,dist_knots,
     &          t,Qwtemp,Uh,ph)
      
           Qw(:,i,:) = Qwtemp
        enddo
        qh = ql
        Vh = v_knotl
        nh = nl
    
      elseif (dir.eq.V_direction) then

        nh = nl + (dist_knots - 1)*t
        Qw = 0.0
        allocate(Pwtemp(nl,NSD+1))
        allocate(Qwtemp(nl+(dist_knots-1)*t,NSD+1))

        do i = 1,ml
           do j = 1,nl
              do k = 1,NSD+1
                 Pwtemp(j,k) = Pw(i,j,k)
              enddo
           enddo
           
           call  degree_elevate_curve(nl,ql,v_knotl,Pwtemp,dist_knots,
     &          t,Qwtemp,Vh,qh)
      
           Qw(i,:,:) = Qwtemp
        enddo
        ph = pl
        Uh = u_knotl
        mh = ml
      endif

      return
      end
