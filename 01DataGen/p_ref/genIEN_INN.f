c       This subroutine generates the IEN matrix, which relates element numbers
c       and local node numbers to the appropriate global node numbers. The
c       routine also generates the INN matrix, which relates global node
c       number to the "NURBS coordinates" of the node.
	
	subroutine genIEN_INN


	use aAdjKeep		! this f90 module contains all allocatables
c       
	include "common.h"	! common file defines all variables
c       
c...    Local variables
	integer :: i, j, k, l, g, e, gtemp, ln
	
	allocate(INN(NNODZ,2))
	allocate(IEN(NEL,(P+1)*(Q+1)))
c       
c...    Initialize matrices and variables
c       
	IEN = 0
	INN = 0
        g = 0
        e = 0

c       
c...    Loop through control points assigning global node
c       numbers and filling out IEN and INN as we go
c       
	do j = 1,NCP		! loop through control points in V direction
	   do i = 1,MCP		! loop through control points in U direction
	      g = g+1
	      INN(g,1) = i
	      INN(g,2) = j
	      if ((i .ge. (P+1)).and.(j .ge. (Q+1))) then
		 e = e +1
		 do l = 0,q
		    do k = 0,p
		       gtemp = g - MCP*l - k
		       ln = (P+1)*l + k + 1
		       IEN(e,ln) = gtemp
		    enddo
		 enddo
	      endif
	   enddo
	enddo


	return
	end
