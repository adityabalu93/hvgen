c
c       This subroutine generates the NEW BdryIEN matrix, which relates edge 
c       numbers and local node numbers to the appropriate global node numbers. 
c       
c
c
c       November 08, 2003
c
c       J. Austin Cottrell
c       CAM Graduate Student
c       Institute for Computational Engineering Science
c       The University of Texas at Austin

 
	
	subroutine gen_new_BdryIEN_p(new_NEDJ, new_NNODZ,
     &     new_BdryIEN, new_MCP, new_NCP,new_P,new_Q)
	
	
	use aAdjKeep	
c       
	include "common.h"	! common file defines all variables
c       
c...    Local variables
	integer i, j, edj, new_NEDJ, new_NNODZ, new_MCP, new_NCP
	integer new_BdryIEN(new_NEDJ, max((new_P+1),(new_Q+1))), 
     &       new_P,new_Q
	

c       
c...    Initialize matrices and variables
c       
	new_BdryIEN = 0
        edj = 0
	
c       
c...    Loop through edges assigning numbers and filling out matrices
c         Edge numbers will be assigned sequentially while traversing
c         the boundary in a counter clockwise direction
c       
	if (CLOSED_V_flag.ne.1) then
	   do i = 1,new_MCP-new_P
	      edj = edj + 1
	      do j = 0,new_P
		 new_BdryIEN(edj,j+1) = edj+new_P-j
	      enddo
	   enddo
	   if (CLOSED_U_flag.ne.1) then
	      edj = edj+(new_NCP-new_Q)
	   endif
	   do i = 1,new_MCP-new_P
	      edj = edj+1
	      do j = 0,new_P
		 new_BdryIEN(edj,j+1) = new_NNODZ - i - j + 1
	      enddo
	   enddo
	   edj = new_MCP - new_P
	endif

	if (CLOSED_U_flag.ne.1) then
	   do i = 1,new_NCP-new_Q
	      edj = edj+1
	      do j = 0,new_Q
		 new_BdryIEN(edj,j+1) = (i-j+new_Q)*new_MCP 
	      enddo 
	   enddo
	   if (CLOSED_V_flag.ne.1) then
	      edj = edj+(new_MCP-new_P)
	   endif
	   do i = 1,new_NCP-new_Q
	      edj = edj+1
	      do j = 0,new_Q
		 new_BdryIEN(edj,j+1) = new_NNODZ-new_MCP+1 -
     &                (i+j-1)*new_MCP
	      enddo
	   enddo
	endif
	
	
	return
	end
