      subroutine degree_elevate_curve(ml,pl,u_knotl,Pw,dist_knots,
     &     t,Qw,Uhl,ph)


c
c purpose: This function degree elevates by t a given NURBS curve and
c             refines projective control points Pw such that the curve
c             does not change shape.
c
c
c adapted from algorithm in Piegl, Les. "The NURBS Book". Springer-Verlag:
c    Berlin 1995; p. 206.
c
c
c October 3, 2003
c J. Austin Cottrell
c CES Graduate Student
c Texas Institute for Computational Engineering Science
c University of Texas at Austin


      use aAdjKeep
      
      include "common.h"
      
      
      integer ml,mll,pl,t,ph, ph2, i, j, mpi, mh, dist_knots,
     &     kind, cind, a,b, mul, r, oldr, tr, first, last,
     &     kj,s,save,lbz,rbz,zrbz,k
      real*8  u_knotl(ml+pl+1), Pw(ml,NSD+1), bezalfs(pl+t+1,pl+1),
     &     bpts(pl+1,NSD+1), ebpts(pl+t+1,NSD+1), Nextbpts(pl-1,NSD+1),
     &      alphas(pl-1),alfj, alf,Qw(ml+(dist_knots-1)*t,NSD+1),
     &     Uhl(ml+pl+1+dist_knots*t), inv, ua, ub,binn, binn2,binn3,
     &     numer



 
      mll = ml+pl
      ph = pl+t
      ph2 = ph/2                ! note that this takes the floor(ph/2)

      
      bezalfs(1,1) = 1.0
      bezalfs(ph+1,pl+1) = 1.0
      
      do i = 1,ph2
         call FUBin(ph,i,binn)
         inv = 1.0/binn
         mpi = min(pl,i)
         do j = max(0,i-t),mpi
            call FUBin(pl,j,binn2)
            call FUBin(t,i-j,binn3)
            bezalfs(i+1,j+1) = inv*binn2*binn3
         enddo
      enddo

      do i = ph2+1,ph-1
         mpi = min(pl,i)
         do j = max(0,i-t),mpi
            bezalfs(i+1,j+1) = bezalfs(ph-i+1,pl-j+1)
         enddo
      enddo

      mh = ph
      ua = u_knotl(1)
      

      do i = 0,ph
         Uhl(i+1) = ua
      enddo

      kind = ph + 1
      Qw(1,:) = Pw(1,:)
      cind = 1
      lbz = 1
      r = -1

      do i = 0,pl
         bpts(i+1,:) = Pw(i+1,:)
      enddo

      a = pl
      b = pl+1
      
C       write(*,*) b
C       write(*,*) u_knotl(b+1),u_knotl(b+2)     


      do
         if (.not.(b.lt.mll)) exit
         i = b
         do
            if (.not.((b.lt.mll).and.(u_knotl(b+1).eq.u_knotl(b+2))))
     &           exit
            b = b+1
         enddo
         mul = b-i+1
         mh = mh + mul + t
         ub = u_knotl(b+1)
         oldr = r
         r = pl-mul
         if (oldr.gt.0) then
            lbz = (oldr+2)/2
         else
            lbz = 1
         endif
         if (r.gt.0) then
            rbz = ph-(r+1)/2
         else
            rbz = ph
         endif
         if (r>0) then
            numer = ub - ua
            do k = pl,mul+1,-1
               alphas(k-mul) = numer/(u_knotl(a+k+1)-ua)
            enddo
            do j = 1,r
               save = r-j
               s = mul+j
               do k = pl,s,-1
                  alf = alphas(k-s+1)
                  bpts(k+1,:) = alf*bpts(k+1,:)+(1-alf)*bpts(k,:)
               enddo
               Nextbpts(save+1,:) = bpts(pl+1,:)
            enddo
         endif
         if (r.gt.0) then
            zrbz = rbz+1
         else
            zrbz = rbz
         endif
         do i = lbz,zrbz
            ebpts(i+1,:) = 0.0
            mpi = min(pl,i)
            do j = max(0,i-t),mpi
               ebpts(i+1,:) = ebpts(i+1,:) + 
     &              bezalfs(i+1,j+1)*bpts(j+1,:)
            enddo
         enddo
         
         if (oldr.gt.1) then
            if (oldr.gt.2) then
               alfj = (ua-Uhl(kind))/(ub-Uhl(kind))
            endif
            first = kind-2
            last = kind
            do tr = 1,(oldr-1)
               i = first
               j = last
               kj = j-kind+1
               do
                  if (.not.((j-i).gt.tr)) exit
                  if (i.lt.cind) then
                     alf = (ua-Uhl(i+1))/(ub-Uhl(i+1))
                     Qw(i+1,:) = (Qw(i+1,:)-(1-alf)*Qw(i,:))/alf
                  endif
                  if (kj.ge.lbz) then
                     ebpts(kj+1,:) = ebpts(kj+1,:) -
     &                    alfj*ebpts(kj+2,:)/(1-alfj)
                  endif
                  i = i+1
                  j = j-1
                  kj = kj-1
               enddo
               first = first-1
               last = last+1
            enddo
         endif
         
         if (a.ne.pl) then
            do i = 0,(ph-oldr-1)
               Uhl(kind+1) = ua
               kind = kind+1
            enddo
         endif
         do j = lbz,rbz
            Qw(cind+1,:) =  ebpts(j+1,:)
            cind = cind +1
         enddo
         if (b.lt.mll) then
            lbz = 1
            do j = 0,r-1
               bpts(j+1,:) = Nextbpts(j+1,:)
            enddo
            do j = r,pl
               bpts(j+1,:) = Pw(b-pl+j+1,:)
            enddo
            a = b
            b = b+1
            ua = ub
         else
            do i = 0,ph
               Uhl(kind+i+1) = ub
            enddo
         endif
      enddo
         
      return
      end         

 
c -----------------------------------------------------------------------             
      subroutine FUBin(n,i,binn)
      
      integer n,i
      real*8 binn,tempn,tempi,tempni

c     helper function to compute binomial coefficients
      call factorial(n, tempn)
      call factorial(i, tempi)
      call factorial(n-i, tempni)
      
      binn = tempn/(tempi*tempni)
      
      return
      end
