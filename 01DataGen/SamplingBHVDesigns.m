clear
rbound = [0.8, 1.5];
htbound = [1.0, 1.7];
cfhtrbound = [0.8, 0.9];
cfrfrbound = [0.85, 0.95];
cfdzbound = [-0.5, -0.1];
cfdrbound = [0.2, 1.0];
chdhbound = [-0.1, 0.5];
thicknessbound = [-3, -1.3010]; % thickness variation from 0.001 to 0.05 in logscale
Mt_coeff1bound = [0.1, 2];
Mt_coeff2bound = [-3, 1]; % Mt_coeff2 variation from 0.001 to 10 in logscale
Mt_coeff3bound = [0.001, 1];
pressurebound = [1.0, 100.0];
numdesigns = 100000;
%     design = rowexch(3,numdesigns,'quadratic','tries',10, 'levels', [4, 4, 3]);
design = lhsdesign(numdesigns, 12, 'criterion','correlation', 'iterations',10);

bounds = [rbound; htbound; cfhtrbound; cfrfrbound; cfdzbound; cfdrbound;...
    chdhbound; thicknessbound; Mt_coeff1bound; Mt_coeff2bound; ...
    Mt_coeff3bound; pressurebound];


% mkdir("../designs_" + num2str(numdesigns));

% module = py.importlib.import_module('setup_params');

designparams = zeros(size(design));
for j = 1:size(design,2) % Convert coded values to real-world units
    zmax = max(design(:,j));
    zmin = min(design(:,j));
    designparams(:,j) = interp1([zmin zmax],bounds(j,:),design(:,j));
end

designparams(:,8) = 10.^designparams(:,8);
designparams(:,9) = 1e5.*designparams(:,9);
designparams(:,10) = 1e4*10.^designparams(:,10);
designparams(:,11) = 1e2.*designparams(:,11);

save(['../designs_',num2str(numdesigns),'.mat'],'designparams');

% 
% for d = 1:numdesigns
%     HV_design(designparams(d,7), designparams(d,6), designparams(d,5),...
%         0.0, designparams(d,4), designparams(d,3), designparams(d,1),...
%         designparams(d,2), "../designs_" + num2str(numdesigns) + "/" + int2str(d));
%     module.setup_param_mat("../designs_" + num2str(numdesigns) + "/" + int2str(d), ...
%         designparams(d,8), designparams(d,9), designparams(d,10),...
%         designparams(d,11), designparams(d,12));
%     close all;
%     fclose all;
% end





