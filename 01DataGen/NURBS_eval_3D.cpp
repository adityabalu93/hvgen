#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include "mex.h"

#define dim  3 // dimension
#define eps 1.0e-8
#define pmax 10

// Algorithm A2.1 (page 68)
int FindSpan(int n, int p, double u, double *U)
{
      //if (u == U[n+1])
      //      return n; // Special case
      if (fabs(u - U[n+1]) < eps)
            return n; // Special case
         
      int low  = p;
      int high = n+1; 
      // Do binary search
      int mid = (low + high)/2;
      while (u < U[mid]-eps || u >= U[mid+1]+eps){
            if (u < U[mid]-eps)
                  high = mid;
            else
                  low = mid;
            mid = (low + high)/2;
      }
      return mid;
}


// Algorithm A2.2 (page 70)
void BasisFuns(int i, double u, int p, double* U, double* N)
{
      double *left  = new double [p+1];
      double *right = new double [p+1];
      double saved, temp;
      N[0] = 1.0;
      for (int j = 1; j <= p; j += 1){
            left[j] = u-U[i+1-j];
            right[j] = U[i+j]-u;
            saved = 0.0;
            for (int r = 0; r < j; r += 1){
                  temp = N[r]/(right[r+1] + left[j-r]);
                  N[r] = saved + right[r+1]*temp;
                  saved = left[j-r]*temp;
            }
            N[j] = saved;
      }
      
      delete[] left;
      delete[] right;
}

// Algorithm A2.3 (page 72)
void DersBasisFuns(int i, double u, int p, int n,  double* U, double ders[][pmax+1])
{
//       double *left  = new double [p+1];
//       double *right = new double [p+1];
      double left[pmax];
      double right[pmax];
      double saved, temp;
      double ndu[pmax+1][pmax+1];
      double a[2][pmax+1];
      double d;
      int s1, s2, r, j1, j2, rk, pk, jtmp;
      
      ndu[0][0] = 1.0;
      for (int j = 1; j <= p; j += 1){
            left[j] = u-U[i+1-j];
            right[j] = U[i+j]-u;
            saved = 0.0;
            for (int r = 0; r < j; r += 1){
                  
                  // Lower triangle
                  ndu[j][r] = right[r+1] + left[j-r];
                  temp = ndu[r][j-1]/ndu[j][r];
                  
                  // Upper triangle
                  ndu[r][j] = saved+right[r+1]*temp;
                  saved = left[j-r]*temp;
            }
            ndu[j][j] = saved;
      }
      
      // Load the basis functions
      for (int j = 0; j <= p; j+=1){
            ders[0][j] = ndu[j][p];
      }
      
      // ----- This section computes the derivatives (Eq. [2.9]) -----
      // Loop over function index
      for (int r = 0; r <= p; r += 1){
            // Alternate rows in array "a"
            s1 = 0;
            s2 = 1;
            a[0][0] = 1.0;
            // Loop to compute kth derivative
            for (int k = 1; k <= n; k += 1){
                  d = 0.0;
                  rk = r-k;
                  pk = p-k;
                  if (r >= k){
                        a[s2][0] = a[s1][0]/ndu[pk+1][rk];
                        d = a[s2][0]*ndu[rk][pk];
                  }
                  
                  if (rk >= -1)
                        j1 = 1;
                  else
                        j1 = -rk;
                  if (r-1 <= pk)
                        j2 = k-1;
                  else
                        j2 = p-r;
                  
                  for (int j = j1; j <= j2; j += 1){
                        a[s2][j] = (a[s1][j]-a[s1][j-1])/ndu[pk+1][rk+j];
                        d += a[s2][j]*ndu[rk+j][pk];
                  }
                  if (r <= pk){
                        a[s2][k] = -a[s1][k-1]/ndu[pk+1][r];
                        d += a[s2][k]*ndu[r][pk];
                  }
                  ders[k][r] = d;
                  jtmp = s1;
                  s1 = s2;
                  s2 = jtmp; // Switch rows
            }
      }
      
      // Multiply through by the correct factors (Eq. [2.9])
      r = p;
      for (int k = 1; k <= n; k += 1){
            for (int j = 0; j <= p; j += 1)
                  ders[k][j] *= (double)r;
            r *= (p-k);
      }
      
      
//       delete[] left;
//       delete[] right;
}


// Algorithm A3.1 (page 82)
void CurvePoint(int n, int p, double* U, double *P, double u, double *C)
{
      double *N = new double [p+1];
      double w, x, tmp;
      int span = FindSpan(n, p, u, U);
      BasisFuns(span, u, p, U, N);
      
      for (int d = 0; d < dim; d += 1){
            C[d] = 0;
            tmp  = 0;
            for (int i = 0; i <= p; i += 1){
                w = P[(span-p+i)*(dim+1) + dim];
                x = P[(span-p+i)*(dim+1) + d];
                C[d] += N[i]*w*x;  
                tmp  += N[i]*w;
            }
            C[d] /= tmp;
      }
      delete[] N;
}

// Algorithm A3.1 (page 82)
void CurvePoint_der(int n, int p, double* U, double *P, double u, double *C, double *dC, double *ddC)
{
      double N[pmax+1][pmax+1];
      double w, x, tmp;
      int span = FindSpan(n, p, u, U);
      //BasisFuns(span, u, p, U, N);
      
      int d = 3; // derivatives of S(u,v) up to and including order d
      int du = std::min(d, p);
      DersBasisFuns(span, u, p, du, U, N);
      
      //printf("dN = %lf, %lf, %lf, %lf\n", N[1][0], N[1][1],N[1][2],N[1][3]);
      
      for (int d = 0; d < dim; d += 1){
            C[d] = 0;
            tmp  = 0;
            for (int i = 0; i <= p; i += 1){
                w = P[(span-p+i)*(dim+1) + dim];
                x = P[(span-p+i)*(dim+1) + d];
                C[d] += N[0][i]*w*x;  
                tmp  += N[0][i]*w;
            }
            C[d] /= tmp;
      }
      // derivatives
      for (int d = 0; d < dim; d += 1){
            dC[d] = 0;
            tmp  = 0;
            for (int i = 0; i <= p; i += 1){
                w = P[(span-p+i)*(dim+1) + dim];
                x = P[(span-p+i)*(dim+1) + d];
                dC[d] += N[1][i]*w*x;  
                tmp   += N[0][i]*w;
            }
            dC[d] /= tmp;
      }
      //printf("tmp = %lf\n", tmp);
      //printf("dC = %lf, %lf, %lf\n", dC[0], dC[1], dC[2]);
      
      for (int d = 0; d < dim; d += 1){
            ddC[d] = 0;
            tmp  = 0;
            for (int i = 0; i <= p; i += 1){
                w = P[(span-p+i)*(dim+1) + dim];
                x = P[(span-p+i)*(dim+1) + d];
                ddC[d] += N[2][i]*w*x;  
                tmp   += N[0][i]*w;
            }
            ddC[d] /= tmp;
      }
}


// Main funciton to call PointOnBezierCurve
void mexFunction(
		 int          nlhs,
		 mxArray      *plhs[],
		 int          nrhs,
		 const mxArray *prhs[]
		 )
{

  if (nrhs != 5) {
      mexErrMsgIdAndTxt("MATLAB:mexcpp:nargin", 
      "MEXCPP requires 5 input arguments.");
  }
  
  double  nP = mxGetScalar(prhs[0]);
  double* P  = (double *) mxGetPr(prhs[1]);
  double  u  = mxGetScalar(prhs[2]);
  double* U  = (double *) mxGetPr(prhs[3]);
  int     p  = mxGetScalar(prhs[4]);
  
  plhs[0]   = mxCreateDoubleMatrix(1,dim,mxREAL);
  plhs[1]   = mxCreateDoubleMatrix(1,dim,mxREAL);
  plhs[2]   = mxCreateDoubleMatrix(1,dim,mxREAL);
  
  if (u < 0) u = 0.0;
  if (u > 1) u = 1.0;
  
  CurvePoint_der(nP-1, p, U, P, u, mxGetPr(plhs[0]), mxGetPr(plhs[1]) , mxGetPr(plhs[2]) );
  
//   plhs[0]   = mxCreateDoubleMatrix(1,dim,mxREAL);
//   
//   if (u < 0) u = 0.0;
//   if (u > 1) u = 1.0;
//   
//   CurvePoint(nP-1, p, U, P, u, mxGetPr(plhs[0]) );
 
  return;
}
