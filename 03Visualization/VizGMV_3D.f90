
  subroutine VizGMV_3D(iproc,numproc,istep,intep)
  
  
  use aAdjKeep
  use commonvar
  implicit none 
  
  integer, intent(in) :: iproc, numproc, istep, intep

  ! Local variables
  integer :: i, j, k, aa, ct, iln, ni, nj, nk, kk, bb, cs, t, ps
  integer :: mp_nel
  real(8) :: dist, distmin, summ, nor(NSD)
  real(8) :: tmp1, tmp2
  real(8) :: F1t, F2t, F1c, F2c, F6
  real(8) :: TsaiWu1, TsaiWu2, H1, H2, H11, H22, H12, H66, maxTsaiWu1, maxTsaiWu2
  real(8) :: tmpx, tmpy, ang, pi, rotx1, rotx2, rotx3, rotd1, rotd2, rotd3

  integer, allocatable :: IEN_TMP(:,:)
  
  integer, allocatable :: repNote(:), repNoteEle(:,:,:), isbdsave(:), bdxp_ix(:,:), &
                          bd_elem(:,:), ele_bd_note(:,:), ele_bd_N(:), npbdZone(:)
  integer :: isBoundary, ix, i1, i2, j1, j2, isfound, bix1, bix2, bix3, ifrun, saveCt
  integer :: j1t, j2t, trycases(4,2), npbd, Nzone, NzoneMax
  real(8) :: xp(3), mind
  real(8), allocatable :: bdxp(:,:), bdxpD(:,:)

  character(len=30) :: fname
  character(len=10) :: cname, cname2
  
  integer :: ifil, lstep
  
  real(8) :: MtCoefs(100)
  
!   MtCoefs(1) = 4.400000e+05
!   MtCoefs(2) = 1.428419e+05
!   MtCoefs(3) = 3.576620e+01
!   MtCoefs(4) = 6.573974e+01
!   MtCoefs(5) = 9.981490e-01

! no fiber
! MtCoefs(1) = 4.400000e+05
! MtCoefs(2) = 1.450000e+05
! MtCoefs(3) = 3.740000e+01
! MtCoefs(4) = 0.0d0
! MtCoefs(5) = 1.0d0

MtCoefs(1) = 6.760546875e+05
MtCoefs(2) = 0.885651636e+05
MtCoefs(3) = 0.424308856e+00
MtCoefs(4) = 0.729140092e+00
MtCoefs(5) = 0.998338341e+00
  

  if (intep == 0) then
    allocate(QuantLin(NEL,4,NVAR+3))
    call postpro
  else if (intep == 1) then
    allocate(QuantLin(NEL*4,4,NVAR+3))
    call postpro_int
  else if (intep > 1) then
    allocate(QuantLin(NEL*(intep-1)**2,4,NVAR+3))
    call postpro_int_n(intep)  
  end if

  write(*,*) 'out'

  call genstruct(iproc,numproc,intep)
  call applystruct(iproc,numproc)

  nnodz_glob  = nnodz_loc(iproc) + nnodz_glob  
  NEL_GLOB    = NEL_GLOB  + NEL_LOC  

  call Fix_tsp_elements
  
  ! hack !!!!!!!!!!!!!!!!!!!!!!!!!
  y_glob(:, 13) = MtCoefs(5)        *exp(MtCoefs(3)*y_glob(:, 11)) - MtCoefs(5)
  y_glob(:, 14) = (1.0d0-MtCoefs(5))*exp(MtCoefs(4)*y_glob(:, 12)) - (1.0d0-MtCoefs(5))
  y_glob(:, 16) = 0.5d0*MtCoefs(2)*(y_glob(:, 13)+y_glob(:, 14))* 1.0d-4 ! kPa = 10^3Pa = 10^4dyn/cm^2
  
  !y_glob(:,17) = y_glob(:,16)/(y_glob(:,15)+y_glob(:,16))
  !y_glob(:,17) = y_glob(:,14)/(y_glob(:,13)+y_glob(:,14))
  y_glob(:,17) = 0.5d0*MtCoefs(2)*y_glob(:,14)* 1.0d-4/(y_glob(:,15)+y_glob(:,16))
  
  do i = 1, nnodz_glob
      if (y_glob(i,15)+y_glob(i,16) < 1.0d-10) then
          y_glob(i,17) = 0.0d0
      end if
  end do
  
  
  !   INFOS(:,3) = MtCoefs(5)*exp(MtCoefs(3)*INFOS(:,1))
  !   INFOS(:,4) = (1.0d0-MtCoefs(5))*exp(MtCoefs(4)*INFOS(:,2))
  !
  !   INFOS(:,6) = 0.5d0*MtCoefs(2)*(INFOS(:,3)+INFOS(:,4)-1.0d0)* 1.0d-4 ! kPa = 10^3Pa = 10^4dyn/cm^2
  
  
  rot_deg = 0.0d0
  
  ifil = 98

  if (iproc == numproc) then

    ifil = 98
    write(cname,'(I8)') istep
    fname = 'tsol.'//trim(adjustl(cname))//'.tec'

    if (if_over_write) then
        open(ifil, file=fname, status='replace', form='formatted')
    else
        open(ifil, file=fname, status='old', position='APPEND')
    end if
    !write(ifil,*) 'VARIABLES = "X" "Y" "Z" "dx" "dy" "dz" "|d|" "MPb" "MPt"'
    write(ifil,*) 'VARIABLES = "X" "Y" "Z" "dx" "dy" "dz" "|d|" "MPb" "MPt" "I1m3sq" "I4m1sq" "dte1" "idte4" "NH" "exp" "rsexp"'
    !write(ifil,*) 'VARIABLES = "X" "Y" "Z" "dx" "dy" "dz" "|d|" "MPb" "MPt" "fx" "fy" "fz"'
    write(ifil,*) 'ZONE T="', ptype ,'", N=', nnodz_glob, ', E=', NEL_GLOB
    write(ifil,*) 'DATAPACKING=POINT, ZONETYPE=FEQUADRILATERAL'


    ! rotate back to reference and plot relative velocity
    rot_deg = 0.0d0

    do i = 1, nnodz_glob
      ang = -rot_deg
      tmpx  = x_glob(i,1)+y_glob(i,1)
      tmpy  = x_glob(i,2)+y_glob(i,2)
      rotx3 = x_glob(i,3)+y_glob(i,3)
      rotx1 = cos(ang)*tmpx - sin(ang)*tmpy
      rotx2 = sin(ang)*tmpx + cos(ang)*tmpy

      tmpx  = y_glob(i,1)
      tmpy  = y_glob(i,2)
      rotd3 = y_glob(i,3)
      rotd1 = cos(ang)*tmpx - sin(ang)*tmpy
      rotd2 = sin(ang)*tmpx + cos(ang)*tmpy
      
      write(ifil,'(16E14.6)') rotx1, rotx2, rotx3, &
                             rotd1, rotd2, rotd3, &
                             sqrt(rotd1**2+rotd2**2+rotd3**2),&
                             y_glob(i,4), y_glob(i,7), y_glob(i,11:17)

!       write(ifil,'(12E14.6)') rotx1, rotx2, rotx3, &
!                              rotd1, rotd2, rotd3, &
!                              sqrt(rotd1**2+rotd2**2+rotd3**2),&
!                              y_glob(i,4), y_glob(i,7), y_glob(i,10:12)

    end do

    do i = 1, NEL_GLOB

      write(ifil,'(4I8)') (IEN_GLOB(i,j), j = 1, 4)
    end do

  endif

  close(ifil)
  
  
  
  
  
  
  deallocate(QuantLin)
  deallocate(IEN_GLOB, x_glob, y_glob)
  
  if_over_write = .false.
  
  !deallocate(ELEpart, partELE)
  
  
  !===================== determine boundary for ploting =============
  
 !  NzoneMax = 20
!
!
!   allocate(repNote(nnodz_glob), repNoteEle(nnodz_glob, 4, 2))
!   allocate(bdxp(nnodz_glob,3), bdxpD(nnodz_glob, nnodz_glob), &
!            isbdsave(nnodz_glob), bdxp_ix(NzoneMax,nnodz_glob), bd_elem(nnodz_glob,4))
!
!   allocate(ele_bd_note(NEL_GLOB, 4), ele_bd_N(NEL_GLOB), npbdZone(NzoneMax))
!
!   repNote = 0
!
!   do i = 1, NEL_GLOB
!       do j = 1, 4
!           repNote(IEN_GLOB(i,j)) = repNote(IEN_GLOB(i,j)) + 1
!           repNoteEle(IEN_GLOB(i,j), repNote(IEN_GLOB(i,j)), 1) = i
!           repNoteEle(IEN_GLOB(i,j), repNote(IEN_GLOB(i,j)), 2) = j
!       end do
!   end do
!
!   trycases(1,:) = (/1, 1/)
!   trycases(2,:) = (/1, -1/)
!   trycases(3,:) = (/-1, 1/)
!   trycases(4,:) = (/-1, -1/)
!
!   bd_elem = 0
!
!   npbd = 0
!
!   do i = 1, nnodz_glob
!       isBoundary = 0
!       if (repNote(i) == 1) then ! must be boundary
!           isBoundary = 1
!
!           i1 = repNoteEle(i, 1, 1)
!           i2 = i1
!           j1 = repNoteEle(i, 1, 2)
!           j2 = j1
!       else if (repNote(i) == 2) then
!           !isBoundary = 1
!           i1 = repNoteEle(i, 1, 1)
!           j1 = repNoteEle(i, 1, 2)
!           i2 = repNoteEle(i, 2, 1)
!           j2 = repNoteEle(i, 2, 2)
!
!           do k = 1, 4
!               j1t = MODULO(j1 + trycases(k,1) - 1, 4) + 1
!               j2t = MODULO(j2 + trycases(k,2) - 1, 4) + 1
!
!               if (IEN_GLOB(i1, j1t) == IEN_GLOB(i2, j2t)) CYCLE
!
!               if (repNote(IEN_GLOB(i1, j1t)) <= 2 .and. repNote(IEN_GLOB(i2, j2t)) <= 2) then
!                   !write(*,* ) repNote(IEN_GLOB(i1, j1t)), repNote(IEN_GLOB(i2, j2t)), j2t,  j2, trycases(k,2)
!                   isBoundary = 1
!                   EXIT
!               end if
!           end do
!
!
!
!       end if
!
!       if (isBoundary == 1) then
!           npbd = npbd + 1
!           ix = IEN_GLOB(repNoteEle(i, 1, 1),repNoteEle(i, 1, 2))
!           xp = x_glob(ix,1:3)+y_glob(ix,1:3)
!           !write(ifil,'(3E14.6)') xp
!           bdxp(npbd, : ) = xp
!           bd_elem(npbd, 1) = i1
!           bd_elem(npbd, 2) = i2
!           bd_elem(npbd, 3) = j1
!           bd_elem(npbd, 4) = j2
!       end if
!   end do
!
!   ele_bd_N = 0
!   ele_bd_note = 0
!   do i = 1, npbd
!       do j = 1, 2
!           !ele_bd_N(bd_elem(i, j)) = ele_bd_N(bd_elem(i, j)) + 1
!           !ix = ele_bd_N(bd_elem(i, j))
!           !ele_bd_note(bd_elem(i, j), ix, 1) = i
!           !ele_bd_note(bd_elem(i, j), ix, 2) = bd_elem(i, j + 2)
!           ele_bd_note(bd_elem(i, j), bd_elem(i, j + 2)) = i
!           if (bd_elem(i, 1) == bd_elem(i, 2)) EXIT
!       end do
!   end do
!
!   !do i = 1,NEL_GLOB
!   !  write(*,*) ele_bd_note(i, :), IEN_GLOB(i,:)
!   !end do
!
! !   bdxp_ix = 0
! !   isbdsave = 0
! !   isbdsave(1) = 1
! !   bdxp_ix(1) = 1
! !   ix = 1
! !   do i = 2, npbd
! !       isfound = 0
! !       do j = 1, npbd
! !           if (isbdsave(j) == 1) CYCLE
! !
! !           do aa = 1, 2
! !               if (isfound == 1) EXIT
! !               do bb = 1, 2
! !                   if (isfound == 1) EXIT
! !                   if (bd_elem(ix,aa) == bd_elem(j,bb)) then
! !
! !                       k = bd_elem(j,bb+2)
! !                       j1 = MODULO(k + 1 - 1, 4) + 1
! !                       j2 = MODULO(k - 1 - 1, 4) + 1
! !
! !                       ! current
! !                       bix3 = ele_bd_note(bd_elem(ix,aa), bd_elem(ix,aa+2))
! !                       ! new
! !                       bix1 = ele_bd_note(bd_elem(j,bb), j1)
! !                       bix2 = ele_bd_note(bd_elem(j,bb), j2)
! !
! !                       if (bix3 == bix1 .or. bix3 == bix2) isfound = 1
! !                   end if
! !               end do
! !           end do
! !
! !
! !           if (isfound == 1) then
! !
! !               isbdsave(j) = 1
! !               bdxp_ix(i) = j
! !               ix = j
! !               EXIT
! !           end if
! !
! !       end do
! !
! !       if (isfound == 0) then
! !           EXIT
! !       end if
! !   end do
!
!   saveCt = 0
!   bdxp_ix = 0
!   isbdsave = 0
!   do cs = 1, NzoneMax
!       ifrun = 0
!       do t = 1,npbd
!           if (isbdsave(t) == 0) then
!               Nzone = cs
!               ix = t
!               isbdsave(ix) = 1
!               saveCt = 1
!               bdxp_ix(cs,saveCt) = ix
!
!               ifrun = 1
!               EXIT
!           end if
!       end do
!
!       if (ifrun == 0) EXIT
!
!       do i = 1, npbd
!           isfound = 0
!           do j = 1, npbd
!               if (isbdsave(j) == 1) CYCLE
!
!               do aa = 1, 2
!                   if (isfound == 1) EXIT
!                   do bb = 1, 2
!                       if (isfound == 1) EXIT
!                       if (bd_elem(ix,aa) == bd_elem(j,bb)) then
!
!                           k = bd_elem(j,bb+2)
!                           j1 = MODULO(k + 1 - 1, 4) + 1
!                           j2 = MODULO(k - 1 - 1, 4) + 1
!
!                           ! current
!                           bix3 = ele_bd_note(bd_elem(ix,aa), bd_elem(ix,aa+2))
!                           ! new
!                           bix1 = ele_bd_note(bd_elem(j,bb), j1)
!                           bix2 = ele_bd_note(bd_elem(j,bb), j2)
!
!                           if (bix3 == bix1 .or. bix3 == bix2) isfound = 1
!                       end if
!                   end do
!               end do
!
!
!               if (isfound == 1) then
!
!                   isbdsave(j) = 1
!
!                   saveCt = saveCt + 1
!                   bdxp_ix(cs,saveCt) = j
!                   npbdZone(cs) = saveCt
!                   ix = j
!                   EXIT
!               end if
!
!           end do
!
!           if (isfound == 0) then
!               EXIT
!           end if
!       end do
!
!   end do
  
  
  
  !====== old
  
!   do i = 1, npbd
!       do j = 1, npbd
!           bdxpD(i, j) = sum((bdxp(i,:)-bdxp(j,:))**2)
!       end do
!   end do
!
!   isbdsave = 0
!   isbdsave(1) = 1
!   bdxp_ix(1) = 1
!   do i = 2, npbd
!       mind = 1.0d10
!
!       do j = 1, npbd
!           if (isbdsave(j) == 1) CYCLE
!
!           if (bdxpD(bdxp_ix(i-1), j) < mind) then
!               mind = bdxpD(bdxp_ix(i-1), j)
!               ix = j
!           end if
!       end do
!
!       isbdsave(ix) = 1
!       bdxp_ix(i) = ix
!   end do
 
!   fname = 'boundary.'//trim(adjustl(cname))//'.tec'
!
!   open(ifil, file=fname, status='replace', form='formatted')
!   write(ifil,*) 'TITLE = "bounary"'
!   write(ifil,*) 'VARIABLES = "X", "Y", "Z"'
!   !write(ifil,*) 'ZONE T="direction", F=POINT'
!
!   do cs = 1, Nzone
!       write(ifil,*) 'ZONE T="bounary', cs, '", F=POINT'
!       do i = 1, npbdZone(cs)
!           write(ifil,'(3E14.6)') bdxp(bdxp_ix(cs,i), :)
!       end do
!       write(ifil,'(3E14.6)') bdxp(bdxp_ix(cs,1), :)
!   end do
!
!   deallocate(repNote, repNoteEle, bdxp, isbdsave, bdxp_ix, bdxpD, bd_elem)
!   deallocate(ele_bd_note, ele_bd_N, npbdZone)
  
!   close(ifil)
 
  190 format (A8,F12.3,F12.3)
  180 format (A8,F12.3)
  290 format (4(F9.3,A),F9.3)
  280 format (5(F6.3,A),F6.3)
  end subroutine VizGMV_3D
