!--------------------------------------------------------------------
! program to extract the shell mesh
!--------------------------------------------------------------------
      subroutine shell_input_tspline

      use aAdjKeep
      use commonvar
      implicit none

      integer :: i, j, ier, mf
  
      character(len=80) :: tmp
      character(len=30) :: fname

      NSD = 3

      mf = 12
      fname = trim('tmesh.0.dat')
      open(mf, file = fname, status = 'old')
         
      read(mf,*) 
	  
	  P_TS = 3; Q_TS = 3

      read(mf,*) tmp, NNODZ_TS
      read(mf,*) tmp, NEL_TS
      read(mf,*) tmp, maxNSHL_TS

      maxNSHL_BZ = (P_TS+1)*(Q_TS+1)

      allocate(B_NET_TS(NNODZ_TS,NSD+1))
      B_NET_TS   = 0.0d0
      do i = 1, NNODZ_TS
        read(mf,*) tmp, B_NET_TS(i,1:NSD+1)
      end do

      allocate(P_BZ(NEL_TS), Q_BZ(NEL_TS), 
     &         NSHL_BZ(NEL_TS), NSHL_TS(NEL_TS),
     &         IEN_TS(NEL_TS,maxNSHL_TS))
      P_BZ = 0; Q_BZ = 0; NSHL_BZ = 0; NSHL_TS = 0; IEN_TS = 0

      allocate(BZext(NEL_TS,maxNSHL_TS,maxNSHL_BZ))
      BZext = 0.0d0

      do i = 1, NEL_TS
        read(mf,*) tmp, NSHL_TS(i), P_BZ(i), Q_BZ(i)

        NSHL_BZ(i) = (P_BZ(i)+1)*(Q_BZ(i)+1)

        ! check maxNSHL_TS and maxNSHL_BZ
        if (NSHL_TS(i) > maxNSHL_TS) then
          write(*,*) "ERROR: maxNSHL_TS is wrong!!! Should be:", 
     &               NSHL_TS(i), maxNSHL_TS
          stop
        end if

        if (NSHL_BZ(i) > maxNSHL_BZ) then
          write(*,*) "ERROR: maxNSHL_BZ is wrong!!! Should be:", 
     &               NSHL_BZ(i), maxNSHL_BZ
          stop
        end if

        read(mf,*) IEN_TS(i,1:NSHL_TS(i))
        IEN_TS(i,1:NSHL_TS(i)) = IEN_TS(i,1:NSHL_TS(i)) + 1

        do j = 1, NSHL_TS(i)
          read(mf,*) BZext(i,j,1:NSHL_BZ(i))
        end do

      end do


!      ! array for BC
!      allocate(IBC_SH(NNODZ_TS,NSD), stat=ier) 
!      if (ier /= 0) stop 'Allocation Error: IBC'
!      IBC_SH = 0
!
!      do i = 1, NNODZ_TS
!        if ((B_NET_TS(i,2)+48.0d0)<1.0d-9) then
!          IBC_SH(i,:) = 1
!        end if
!      end do

      end subroutine shell_input_tspline
