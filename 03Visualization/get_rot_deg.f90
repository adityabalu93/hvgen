subroutine get_rot_deg(istep, rot_deg)
  
  implicit none
  
  integer, intent(in) :: istep
  real(8), intent(out):: rot_deg
  integer :: i, j, k, kk, g, sol_file, ierr, iproc, &
             itmp1, itmp2, nnodz
  character(len=30) :: fname, cname(2)

  sol_file = 12

  iproc = 20

  write(cname(1),'(I8.2)') iproc
  fname = 'smesh.'//trim(adjustl(cname(1)))//'.dat'
  open(sol_file, file=fname, status='old', iostat=ierr)
  read(sol_file,*)
  read(sol_file,*)
  read(sol_file,*) itmp1, itmp2

  NNODZ = itmp1*itmp2

  close(sol_file)


  write(cname(1),'(I8.2)') iproc
  write(cname(2),'(I8)')   istep
  fname = 'sh_rest_tsp/sh.rest.'//trim(adjustl(cname(2)))//'.'//trim(adjustl(cname(1)))
  open(sol_file, file=fname, status='old', iostat=ierr)

    ! read in the solution data
    do i = 1, NNODZ
      read(sol_file,*)
    end do
    ! skip ush
    do i = 1, NNODZ
      read(sol_file,*)
    end do
    ! skip ash
    do i = 1, NNODZ
      read(sol_file,*)
    end do
    ! skip 
    read(sol_file,*)
    read(sol_file,*)
    ! read rotation degree
    read(sol_file,*) rot_deg

  write(*,*) 'rot_deg=', rot_deg

  close(sol_file)

end subroutine get_rot_deg
