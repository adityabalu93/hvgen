!------------------------------------------------------------------------
! Main routine to call all the subroutines 
!------------------------------------------------------------------------
  
Program Viz
  
  use aAdjKeep
  use commonvar
  implicit none
  
  integer :: so, sf, count, icnt, i, ni, nj, nk, istep, iel, j
  integer :: numproc, nstart, nend, nskip, intep
  integer :: ipt, p_nel, NNODZ_ALL, NEL_ALL, ct, ix
  integer, allocatable :: pnode(:), pnodeIX(:)
  character(len=10) :: inp

  rflag = 0

  intep = 0
  sfact = 1.0d0

  ! get main input
  i = iargc()
  if (i == 1) then
    call getarg(1,inp); read(inp,*) nstart

    ! Scaling factor
    sfact = 1.0d0
    
  else if (i == 2) then
    call getarg(1,inp); read(inp,*) nstart
    call getarg(2,inp); read(inp,*) intep
    
  else
    write(*,*) "Wrong Input"
    !write(*,*) "nstep, sfact"
    write(*,*) "nstep, intep"
    call exit
  endif

  ! get main input  
  istep = nstart



  ! only 1 patch for t-spline
  numproc = 1
 
!  ! interpolation is not working for t-spline for now
!  intep = 0

  allocate(nnodz_loc(numproc))

!!!  call get_rot_deg(istep, rot_deg)

  !nnodz_glob = 0
  !NEL_GLOB   = 0
  
  if_over_write = .true.

  ni = 1

    call shell_input_tspline

    NNODZ = NNODZ_TS
    NEL   = NEL_TS

    ! number of solution variables
    NVAR = 20    
    allocate(yg(NNODZ,NVAR))

    yg = 0.0d0    
    if (istep > 0) call results_3D(istep)
    
    allocate(yg_ALL(NNODZ,NVAR))
    allocate(pnode(NNODZ_TS))
    
    allocate(B_NET_TS_ALL(NNODZ_TS,NSD+1))
    
    allocate(P_BZ_ALL(NEL_TS), Q_BZ_ALL(NEL_TS), &
             NSHL_BZ_ALL(NEL_TS), NSHL_TS_ALL(NEL_TS), &
             IEN_TS_ALL(NEL_TS,maxNSHL_TS))
    
    allocate(BZext_ALL(NEL_TS,maxNSHL_TS,maxNSHL_BZ))
    
    ! save variables to *_ALL
    
    NEL_ALL   = NEL
    NNODZ_ALL = NNODZ
    
    yg_ALL = yg
    B_NET_TS_ALL = B_NET_TS
    P_BZ_ALL = P_BZ
    Q_BZ_ALL = Q_BZ
    NSHL_BZ_ALL = NSHL_BZ
    NSHL_TS_ALL = NSHL_TS
    IEN_TS_ALL = IEN_TS
    BZext_ALL = BZext
    
    
    do ipt = 1, Npats
        NEL   = partELE(ipt, 1)
        ptype = partELE(ipt, 2)
        
        if (ptype == -1) CYCLE ! fixed nodes

        NNODZ_TS = NEL
        
        pnode = 0
        
        do iel = 1, NEL
            ix = partELE(ipt, 2+iel)
            do j = 1, NSHL_TS_ALL(ix)
                pnode(IEN_TS_ALL(ix, j)) = 1
            end do
        end do 
        
        
        ct = 0
        do i = 1, NNODZ_ALL
            if (pnode(i) == 1) then
                ct = ct + 1
                pnode(i) = ct
                B_NET_TS(ct,:) = B_NET_TS_ALL(i,:)
                yg(ct,:) = yg_ALL(i,:) 
            end if
        end do
        NNODZ = ct
        NNODZ_TS = NNODZ
        
        write(*,*) ptype, NEL, NNODZ
        
        do iel = 1, NEL
            ix = partELE(ipt, 2 + iel)
            P_BZ(iel) = P_BZ_ALL( ix ) 
            Q_BZ(iel) = Q_BZ_ALL( ix ) 
            NSHL_BZ(iel) = NSHL_BZ_ALL( ix ) 
            NSHL_TS(iel) = NSHL_TS_ALL( ix ) 
            IEN_TS(iel,:) = IEN_TS_ALL( ix, :) 
            BZext(iel,:,:) = BZext_ALL( ix, :, :) 
        end do
        
        do iel = 1, NEL
            do j = 1, NSHL_TS(iel)
                IEN_TS(iel, j) = pnode( IEN_TS(iel, j) )
            end do 
        end do
        
        
        nnodz_glob = 0
        NEL_GLOB   = 0
        
        call VizGMV_3D(ni,numproc,istep,intep)
        
        !if (ptype == 2) stop
        !stop
        
    end do
    
    
    !call VizGMV_3D(ni,numproc,istep,intep)
        
    deallocate(yg)
    
    deallocate(yg_ALL)   
    deallocate(B_NET_TS, P_BZ, Q_BZ, NSHL_BZ, NSHL_TS, IEN_TS, BZext) 
    deallocate(B_NET_TS_ALL, P_BZ_ALL, Q_BZ_ALL, NSHL_BZ_ALL, &
               NSHL_TS_ALL, IEN_TS_ALL, BZext_ALL) 
    
end program Viz


