!--------------------------------------------------------------
! Main routine to call all the subroutines        
!--------------------------------------------------------------
program main  

  use aAdjKeep
  use mpi
  use aaOutBC
  use commonvar
  use commonvar_contact
  use commonpar
  use types_im
  use types_shell
  use commonvar_shell

  implicit none

  ! background mesh
  type(imtype) :: IM

  ! coarse background mesh for contact problem
  type(contactCells) :: contactGrid

  ! surface for weak BCs
  type(shell)   :: SHL
  type(mesh_mp) :: mNRB, mTSP, mBEZ
  type(mesh)    :: NRB, TSP, BEZ

  !type(smesh)  :: SUR

  integer, allocatable :: row(:), col(:), rowt(:), colt(:)
  
  integer :: i, j, k, ii, ct, nf1, ierr, iel, &
             icntu, icntp, istep, Rstep, restep, ip
  real(8) :: time, rtime, Rper, maxthetd, &
             Rmat(3,3), Rdot(3,3), Rddt(3,3), &
             RmatOld(3,3), RdotOld(3,3), RddtOld(3,3)
  LOGICAL :: exts1, exts2

  character(len=30) :: fname(2), fnum(3)
  
  integer :: itmp
  
  ! Initialize MPI
  call MPI_INIT(                               mpi_err)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, numnodes, mpi_err)
  call MPI_COMM_RANK(MPI_COMM_WORLD, myid    , mpi_err)
  ismaster = myid.eq.mpi_master
  
  != get main input =========================
  ! Read Displacemet Data
  != get main input =========================
  call getparam(SHL)

  allocate(SHL%Nnewt(NS_NL_itermax))
  SHL%Nnewt = 1

  != get shell input ==================================================
  ! most of the shell routines, including reduced node are written here
  call input_shell(NSD, SHL, mTSP, mBEZ, NRB, TSP, BEZ)

  ! Allocate solution variables for SHELL (Tspline for now)
  allocate(TSP%dsh(TSP%NNODE,NSD), TSP%dshOld(TSP%NNODE,NSD), &
           TSP%ush(TSP%NNODE,NSD), TSP%ushOld(TSP%NNODE,NSD), &
           TSP%ash(TSP%NNODE,NSD), TSP%ashOld(TSP%NNODE,NSD), &
           TSP%iscontact(TSP%NNODE), TSP%contactarea(TSP%NNODE))

  ! initialize and allocate the contact background grid
  call contactCells_init(contactGrid, &
       (/ contact_lox, contact_loy, contact_loz /), & ! low corner
       (/ contact_hix, contact_hiy, contact_hiz /), & ! high corner
       (/ contact_nx, contact_nz, contact_ny /), & ! numbers of cells
       TSP)

  !-------------------------------
  ! read in the step file
  !-------------------------------
  Rstep = 0
  rtime = 0.0d0
  time  = 0.0d0
  open(22, file='step.dat', status='old', iostat=ierr)
  if (ierr == 0) then
    read (22,*) Rstep
    close(22)
  end if  

  if (Rstep == 0) then    
!
    ! angular solution variables
    thetaOld = 0.0d0; thetdOld = 0.0d0; theddOld = 0.0d0

    ! old solution variables for shell
    TSP%dshOld = 0.0d0; TSP%ushOld = 0.0d0; TSP%ashOld = 0.0d0

    ! output the initial condition
    if (ismaster)  call write_sol_tsp(Rstep, time, rtime, TSP)
    
  else
    ! read restart solutions
    call read_sol_tsp(Rstep, time, rtime, TSP)
  end if
  
  
  !TSP%IBC = 0
  ! ======= fix the stent ========
  do iel = 1, TSP%NEL
      if (TSP%PTYPE(iel) == 4) then
          do i = 1, TSP%NSHL(iel)
              TSP%IBC( TSP%IEN(iel,i), :) = 1
          end do
      end if
  end do
  ! ==============================
  
  ! ======== For computing area ========
  N_ar = TSP%NEL * TSP%NGAUSS * TSP%NGAUSS 
  allocate(Nodes_local(N_ar,3), Nodes_all(N_ar,3))
  ! ====================================
  
  
  !call plot_fine_tsp_tec(TSP, BEZ, nsd)
  !stop
  
  if(ismaster) write(*,*) 'assign_fiber_dir'
  call assign_fiber_dir(TSP, BEZ, nsd)
  if(ismaster) write(*,*) 'assign_fiber_dir finish'
  
  !call plot_fiber_dir2(TSP, BEZ, nsd)
  !TSP%dsh = 0.0d0
  !call write_sol_tsp(1, time, rtime, TSP)   
  !stop


  !== START COMPUTING =================================
  ! Loop over time steps
  !----------------------------------------------------
  do istep = Rstep+1, Nstep
     
    rtime = rtime + Delt     ! real time    
    time  =  time + Delt     ! t^n+1 (t+dt)

    ! rewind 'tim' variable
    do
      if (time > Tper) then
        time = time - Tper
      else
        exit
      end if
    end do

    if (ismaster)  then  
      write(*,'(60("="))')
      write(*,'(1X,A,I8,2F13.6)') "Time Step Number:", istep, time, rtime
      write(*,'(60("="))')
    end if
!
    ! for stationary case
    theta = 0.0d0
    thetd = 0.0d0
    thedd = 0.0d0
    
    != Predictor ======================================
    ! Assume u_n+1 = u_n
    if((rhoinf < 0.0d0).or.(rhoinf > 1.0d0)) then ! backward Euler

      TSP%ash = 0.0d0
      TSP%ush = TSP%ushOld
      TSP%dsh = TSP%dshOld + Delt*TSP%ushOld

      thetd = thetdOld
      thedd = 0.0d0
      theta = thetaOld + Delt*thetdOld

    else    ! generalized alpha

      !-------------------------------------------------------------------------
      ! no rotation should be occuring; no mesh motion should be occuring
      !-------------------------------------------------------------------------

      !! Algorithm for rotation.
      !! apply the exact rotation to the predictor, then solve
      !! for the deflection.
      call get_Rmat(theta, thetd, thedd, Rmat, Rdot, Rddt)

      call get_Rmat(thetaold, thetdold, theddold, RmatOld, &
                    RdotOld, RddtOld)

      ! The blade motion has to be the same as the corresponding
      ! fluid mesh, i.e. previous paragraph

      ! (this should not do anything; just here for consistency)
      ! MH: this has to be here for the shell prediction
      forall (ct = 1:TSP%NNODE, ii = 1:3)
         TSP%ush(ct,ii) = TSP%ushold(ct,ii)+  &
              sum((Rdot(ii,:)-RdotOld(ii,:))*TSP%B_NET(ct,1:3))
         
         TSP%ash(ct,ii) = sum(Rddt(ii,:)*TSP%B_NET(ct,1:3)) + &
              (gami-1.0d0)/gami*(TSP%ashold(ct,ii) - &
              sum(RddtOld(ii,:)*TSP%B_NET(ct,1:3)))
         
         TSP%dsh(ct,ii) = TSP%dshold(ct,ii) + &
              sum((Rmat(ii,:)-RmatOld(ii,:))*TSP%B_NET(ct,1:3)) + &
              Delt*(TSP%ushold(ct,ii)-sum(RdotOld(ii,:)*TSP%B_NET(ct,1:3)))+ &
              Delt*Delt/2.0d0*((1.0d0-2.0d0*beti)* &
              (TSP%ashold(ct,ii)-sum(RddtOld(ii,:)*TSP%B_NET(ct,1:3))) + &
              2.0d0*beti*(TSP%ash(ct,ii)-sum(Rddt(ii,:)*TSP%B_NET(ct,1:3))))
      end forall

    end if

    != Advance FSI system from n to n+1 ==============
    call solflowGMRES(IM, SHL, TSP, BEZ, contactGrid, row, col, rowt, colt, & 
                      icntu, istep, Rstep, time, rtime)
    
    !call Compute_Area(istep, ifq)			  
    != Write Fields ==================================
    if (mod(istep,ifq) == 0 .and. ismaster) then 
      call write_sol_tsp(istep, time, rtime, TSP)       
          
    end if

    if (mod(istep,ifq) == 0 .and. ismaster) then
      open( 66, file='step.dat', status='replace')
      write(66,*) istep
      close(66)
    end if

    TSP%dshOld = TSP%dsh
    TSP%ushOld = TSP%ush
    TSP%ashOld = TSP%ash
     
    thetaOld = theta
    thetdOld = thetd
    theddOld = thedd
    
  end do
  != end of looping through time steps ----------------
  
  
  deallocate(Nodes_local, Nodes_all)
  
  ! Finalize MPI
  call MPI_FINALIZE(mpi_err)
  
  end program main
