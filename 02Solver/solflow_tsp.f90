!======================================================================
!
!======================================================================
subroutine solflowGMRES(IM, SHL, TSP, BEZ, contactGrid, row, col, rowt, colt, icntu, istep, Rstep, tim, rtim)
                        
  use aAdjKeep
  use mpi
  use aaOutBC
  use commonvar
  use commonvar_shell
  use commonpar
  use types_im
  use types_shell

  implicit none

  type(imtype), intent(inout) :: IM
  type(mesh),  intent(inout) :: TSP, BEZ
  type(shell), intent(inout) :: SHL
  type(contactCells), intent(inout) :: contactGrid

  integer, intent(in) :: istep, icntu, Rstep
  real(8), intent(in) :: tim, rtim
  
  real(8) :: dgAlpha(NNODZu,NSD), ugAlpha(NNODZu,NSD), &
             ugmAlpha(NNODZu,NSD), acgAlpha(NNODZu,NSD), &
             acgmAlpha(NNODZu,NSD), pgAlpha(NNODZu), &
             momres, meshres, conres, momresl, meshresl, &
             conresl, momres0, conres0, fact2, FlRate, momres_fl, momres_sl, &
             momres_fsbd, momresl_fl, momresl_sl, &
             momresl_fsbd, xloc(NSD)
  
  integer :: row(NNODZu*8*(Pu+1)*(Qu+1)*(Ru+1)), &
             col(NNODZu+1), colp(NNODZu+1), &
             rowt(NNODZu*8*(Pu+1)*(Qu+1)*(Ru+1)), &
             colt(NNODZu+1), mf, ier, &
             inewt, i, j, k, ii, ct, itask, is, lenseg, &
             a, b, c, n, locn, iel

  real(8) :: Rmat(NSD,NSD), Rdot(NSD,NSD), Rddt(NSD,NSD), &
             dshAlpha(TSP%NNODE,NSD), ushAlpha(TSP%NNODE,NSD), &
             ashAlpha(TSP%NNODE,NSD)
             
  integer :: ip, inewt_SH, iincr_SH, Nincr_SH, inewt_FL
  real(8) :: thetares1, thetares2, thetares3, &
             thetaAlpha, thetdAlpha, theddAlpha, Rper, &
             RHSFact_SH

  real(8) :: ddef(NNODZu,NSD), ddefold(NNODZu,NSD), &
             udef(NNODZu,NSD), adef(NNODZu,NSD)

  real(8), allocatable :: r22tmp1(:,:), r22tmp2(:,:)
  
  real(8) :: start_time, end_time
   
  real(8) :: r, mmHg, timpr
  real(8) :: maxDisp, def_maxDisp
  
  integer :: n_info
  real(8), allocatable :: INFOS(:,:), mg_SH(:)

  character(len=30) :: fname
  character(len=10) :: cname
  character(len=80) :: fomt
  
  n_info = 6
  
  allocate(INFOS(TSP%NNODE, n_info), mg_SH(TSP%NNODE))
  
  ! ========= for output infos only ============
  
 !  ashAlpha = 0.0d0
!   ushAlpha = 0.0d0
!   dshAlpha = 0.0d0
!
!   TSP%B_NET_D(:,1:3) = TSP%B_NET(:,1:3) + TSP%dshOld
!
!   call shell_gp_info_tsp(TSP, BEZ, ushAlpha, ashAlpha, NSD)
!   call computeContactForces(tsp,bez,contactGrid,shl,ushAlpha)
!
!   call shell_allocate_lrhs(TSP,nsd,shl)
!
!   call IntElmAss_shell(TSP, BEZ, SHL%icnt, SHL%col, SHL%row, &
!        NSD, SHL%G_fact, SHL%RHSG, SHL%RHSG_GRA, SHL%RHSG_EXT, &
!        SHL%LHSK, n_info, INFOS, mg_SH, ashAlpha, ushAlpha, alfi, beti, almi, gami, Delt)
!
!
!   ! L2-Projection: divded by the lumped mass (area of influence)
!   do i = 1, n_info
!     INFOS(:,i) = INFOS(:,i)/mg_SH(:)
!   end do
!
!  !  INFOS(:,3) = MtCoefs(5)*exp(MtCoefs(3)*INFOS(:,1))
! !   INFOS(:,4) = (1.0d0-MtCoefs(5))*exp(MtCoefs(4)*INFOS(:,2))
! !
! !   INFOS(:,6) = 0.5d0*MtCoefs(2)*(INFOS(:,3)+INFOS(:,4)-1.0d0)* 1.0d-4 ! kPa = 10^3Pa = 10^4dyn/cm^2
! !
! !   INFOS(:,6) = INFOS(:,6)/(INFOS(:,5) + INFOS(:,6))
!
!   do i = 1, TSP%NNODE
!       if (TSP%IBC(i,1) == 1 .or. TSP%IBC(i,2) == 1 .or. TSP%IBC(i,3) == 1) then
!           INFOS(i,:) = 0.0d0
!       end if
!   end do
!
!   if (ismaster) then
!       ! output INFO
!       call write_shell_INFOs(TSP%NNODE, n_info, INFOS, istep-1)
!
!   end if
!
!   call shell_deallocate_lrhs(TSP,nsd,shl)
!
!   write(*,*) myid, 'fin'
!   call MPI_BARRIER(MPI_COMM_WORLD, mpi_err)
!
!   ! Finalize MPI
!   call MPI_FINALIZE(mpi_err)
!
!   stop
  
  ! ========= for output infos only ============ <end>
  
  
  def_maxDisp = 10.d0

  acgAlpha  = 0.0d0
  acgmAlpha = 0.0d0
  ugAlpha   = 0.0d0
  ugmAlpha  = 0.0d0
  dgAlpha   = 0.0d0
  pgAlpha   = 0.0d0

  dshAlpha = 0.0d0
  ushAlpha = 0.0d0
  ashAlpha = 0.0d0

  != Newton's (Iteration) Method ======================
  !  for solving Nonlinear System
  !----------------------------------------------------
  do inewt = 1, NS_NL_itermax

	  ! closing
	  if (Mode == 0) then
	      mmHg = InitPress    
		  Flrate = -0.133322368d0*mmHg
	  ! dynamic
	  else if(Mode == 1) then
		  timpr = tim + T_START
		  if (timpr > Tper) then
		     timpr = timpr - Tper
	      end if
		  call get_BC_const(timpr, FlRate)   ! fix the flowrate		
	  else if (Mode == 2) then
		  FlRate = 0.418033
	  else
		  flrate = 0
	  end if
	  
	  ! to change the pressure slowly in the beginning	
	  !FlRate = FlRate*min(200.0*rtim,1.0)
      FlRate = FlRate*min(rtim/100.0d-4,1.0)

    ! the input flowrate is kPa = 10^3Pa = 10^4dyn/cm^2
    FlRate = FlRate*1.0d4

    
    if (inewt==1 .and. ismaster) write(*,*) "Pressure =", FlRate


    !===============================================================
    ! Begin SHELL analysis
    !===============================================================

    ! converge geometrical nonlinearity
    do inewt_SH = 1, SHL%Nnewt(inewt)

      ! Get quantities at alpha levels:
      ashAlpha = TSP%ashOld + almi*(TSP%ash-TSP%ashOld)
      ushAlpha = TSP%ushOld + alfi*(TSP%ush-TSP%ushOld)
      dshAlpha = TSP%dshOld + alfi*(TSP%dsh-TSP%dshOld)       
      
      ! update the deformed shell geometry
      TSP%B_NET_D(:,1:3) = TSP%B_NET(:,1:3) + dshAlpha(:,:)

      ! get updated positions and normals for the gauss points, to use
      ! in the contact computation
      call shell_gp_info_tsp(TSP, BEZ, ushAlpha, ashAlpha, NSD)

! get a pressure load and put it in the shaft on nacelle and blade tip
!       TSP%gp%traction = -flrate*(TSP%gp%nor)
      TSP%gp%traction = -flrate*(TSP%gp%nor)


      ! compute the contact forces (stored in TSP%gp)
      call computeContactForces(tsp,bez,contactGrid,shl,ushAlpha)

      ! come up with a new sparse structure using the contact ien
    
      if (ismaster) write(*,*) "Generating sparse structure for shell..."
     

      ! allocate and initialize space for shell matrix problem
!      call shell_allocate_lrhs(NRB,nsd,shl)
      call shell_allocate_lrhs(TSP,nsd,shl)

      SHL%RHSG     = 0.0d0
      SHL%RHSG_EXT = 0.0d0
      SHL%RHSG_GRA = 0.0d0
      SHL%LHSK     = 0.0d0

      call genSparStruc_shell(TSP%NEL*((TSP%ngauss)**2), TSP%NNODE, &
          TSP%maxNSHL, TSP%gp%cIEN, &
          TSP%gp%cNSHL, SHL%col, SHL%row, SHL%icnt)

      
       if (ismaster) write(*,*) "Finished with shell sparse structure..."
     
      
	  call cpu_time(start_time)
      ! assemble shell eqns
      
!       call IntElmAss_shell_temp(TSP, BEZ, SHL%icnt, SHL%col, SHL%row, &
!            NSD, SHL%G_fact, SHL%RHSG, SHL%RHSG_GRA, SHL%RHSG_EXT, &
!            SHL%LHSK, ashAlpha, ushAlpha, alfi, beti, almi, gami, Delt)
      
      call IntElmAss_shell(TSP, BEZ, SHL%icnt, SHL%col, SHL%row, &
           NSD, SHL%G_fact, SHL%RHSG, SHL%RHSG_GRA, SHL%RHSG_EXT, &
           SHL%LHSK, n_info, INFOS, mg_SH, ashAlpha, ushAlpha, alfi, beti, almi, gami, Delt, rtim, Mode, sum(TSP%contactarea))
      
	  call cpu_time(end_time)     
      if (ismaster) write(*,*) "Finished with IntElmAss_shell...", end_time - start_time, "s"

      call cpu_time(start_time)
      ! add contact forces
      call IntElmAss_shell_contact(tsp,bez, shl%icnt, shl%col, shl%row, &
           nsd, shl%RHSG_EXT, shl%LHSK, ashAlpha, ushAlpha, &
           alfi, beti, almi, gami, Delt)
       
	   call cpu_time(end_time)       
       if (ismaster) write(*,*) "Finished with IntElmAss_shell_contact...", end_time - start_time, "s"

      ! (all RHS BCs are accounted for in IntElmAss_shell)

      ! sum different contributions to the shell RHS
      SHL%RHSG = SHL%RHSG + SHL%RHSG_EXT + SHL%RHSG_GRA
      
      !=========================================
      ! Solve Kx = f
      !=========================================
      SHL%RHSGNorm = sqrt(sum(SHL%RHSG_EXT(:,1)**2 + &
                              SHL%RHSG_EXT(:,2)**2 + &
                              SHL%RHSG_EXT(:,3)**2 ))
      
        fomt = "(a,x,ES13.6)"
      if (ismaster)   write(*,fomt) " *) External Force Norm:", SHL%RHSGNorm
      
      SHL%RHSGNorm = sqrt(sum(SHL%RHSG(:,1)**2 + &
                              SHL%RHSG(:,2)**2 + &
                              SHL%RHSG(:,3)**2 ))
     
      
        fomt = "(I2,a,x,ES13.6)"
      if (ismaster)   write(*,fomt) inewt_SH, ") Shell Residual Norm Equals:", SHL%RHSGNorm
      

!      if (SHL%RHSGNorm < SHL%RHSGtol) exit
      if (SHL%RHSGNorm == 1.0d-6) exit

      call cpu_time(start_time)
      ! Increment for the geometric nonlinearity
      ashAlpha = 0.0d0
      call SparseCG_BDIAG_shell(TSP%NNODE, TSP%maxNSHL, NSD, &
                                SHL%icnt, SHL%col, SHL%row, &
                                SHL%LHSK, SHL%RHSG, &
                                ashAlpha, SHL%CGTol, 1)

	  call cpu_time(end_time)
	  if (ismaster) write(*,*) "Finished with SparseCG_BDIAG_shell:", end_time - start_time, "s"

!              write(*,*) ashAlpha(1,:)
!              write(*,*) ashAlpha(2,:)
!              write(*,*) ashAlpha(3,:)
!              write(*,*) ashAlpha(4,:)
!              write(*,*) ashAlpha(5,:)
!              write(*,*) ashAlpha(6,:)
!              write(*,*) ashAlpha(7,:)
!              write(*,*) ashAlpha(8,:)
!              write(*,*) beti*Delt*Delt
      
	  ! free matrix problem memory
!      call shell_deallocate_lrhs(nrb,nsd,shl)
      call shell_deallocate_lrhs(TSP,nsd,shl)
      
!       maxDisp = maxval(abs(ashAlpha))
!
!       if (maxDisp > def_maxDisp) then
!           ashAlpha = ashAlpha*def_maxDisp/maxDisp
!       end if
      
      ! update current values for shell unknowns
      TSP%ash(:,:) = TSP%ash(:,:) + ashAlpha(:,:)
      TSP%ush(:,:) = TSP%ush(:,:) + ashAlpha(:,:)*gami*Delt
      TSP%dsh(:,:) = TSP%dsh(:,:) + ashAlpha(:,:)*beti*Delt*Delt

   enddo ! end local newton iteration for shell

   ! (no projection of displacements in immersed method)
   ! (no mesh solve for static background mesh)

end do    ! end loop of Newton's method


! L2-Projection: divded by the lumped mass (area of influence)
! do i = 1, n_info
!   INFOS(:,i) = INFOS(:,i)/mg_SH(:)
! end do
!
! if (ismaster) then
!     ! output INFO
!     call write_shell_INFOs(TSP%NNODE, n_info, INFOS, istep)
!
! end if
if (ismaster) then
  open(12, file='coaptation' , position= 'append')
  write(12,*) sum(TSP%contactarea)
  close(12)
end if

deallocate(INFOS, mg_SH)

  
end subroutine solflowGMRES

!======================================================================
!
!======================================================================
! subroutine solflowGMRES(IM, SHL, TSP, BEZ, contactGrid, row, col, rowt, colt, icntu, istep, Rstep, tim, rtim)
!
!   use aAdjKeep
!   use mpi
!   use aaOutBC
!   use commonvar
!   use commonpar
!   use types_im
!   use types_shell
!
!   implicit none
!
!   type(imtype), intent(inout) :: IM
!   type(mesh),  intent(inout) :: TSP, BEZ
!   type(shell), intent(inout) :: SHL
!   type(contactCells), intent(inout) :: contactGrid
!
!   integer, intent(in) :: istep, icntu, Rstep
!   real(8), intent(in) :: tim, rtim
!
!   real(8) :: dgAlpha(NNODZu,NSD), ugAlpha(NNODZu,NSD), &
!              ugmAlpha(NNODZu,NSD), acgAlpha(NNODZu,NSD), &
!              acgmAlpha(NNODZu,NSD), pgAlpha(NNODZu), &
!              momres, meshres, conres, momresl, meshresl, &
!              conresl, momres0, conres0, fact2, FlRate, momres_fl, momres_sl, &
!              momres_fsbd, momresl_fl, momresl_sl, &
!              momresl_fsbd, xloc(NSD)
!
!   integer :: row(NNODZu*8*(Pu+1)*(Qu+1)*(Ru+1)), &
!              col(NNODZu+1), colp(NNODZu+1), &
!              rowt(NNODZu*8*(Pu+1)*(Qu+1)*(Ru+1)), &
!              colt(NNODZu+1), mf, ier, &
!              inewt, i, j, k, ii, ct, itask, is, lenseg, &
!              a, b, c, n, locn, iel
!
!   real(8) :: Rmat(NSD,NSD), Rdot(NSD,NSD), Rddt(NSD,NSD), &
!              dshAlpha(TSP%NNODE,NSD), ushAlpha(TSP%NNODE,NSD), &
!              ashAlpha(TSP%NNODE,NSD)
!
!   integer :: ip, inewt_SH, iincr_SH, Nincr_SH, inewt_FL
!   real(8) :: thetares1, thetares2, thetares3, &
!              thetaAlpha, thetdAlpha, theddAlpha, Rper, &
!              RHSFact_SH
!
!   real(8) :: ddef(NNODZu,NSD), ddefold(NNODZu,NSD), &
!              udef(NNODZu,NSD), adef(NNODZu,NSD)
!
!   real(8), allocatable :: r22tmp1(:,:), r22tmp2(:,:)
!
!   real(8) :: start_time, end_time
!
!   real(8) :: r, mmHg, timpr
!
!   character(len=30) :: fname
!   character(len=10) :: cname
!   character(len=80) :: fomt
!
!   acgAlpha  = 0.0d0
!   acgmAlpha = 0.0d0
!   ugAlpha   = 0.0d0
!   ugmAlpha  = 0.0d0
!   dgAlpha   = 0.0d0
!   pgAlpha   = 0.0d0
!
!   dshAlpha = 0.0d0
!   ushAlpha = 0.0d0
!   ashAlpha = 0.0d0
!
!   != Newton's (Iteration) Method ======================
!   !  for solving Nonlinear System
!   !----------------------------------------------------
!   do inewt = 1, NS_NL_itermax
!
!       ! closing
!       if (Mode == 0) then
!           !mmHg = 45.0d0
!           mmHg = 80.0d0
!           Flrate = -0.133322368d0*mmHg
!       ! dynamic
!       else if(Mode == 1) then
!           timpr = tim + T_START
!           if (timpr > Tper) then
!              timpr = timpr - Tper
!           end if
!           call get_BC_const(timpr, FlRate)   ! fix the flowrate
!       else if (Mode == 2) then
!           !FlRate = 0.66661184
!           FlRate = 0.418033
!       ! else if (Mode == 3) then
!           ! if (rtim <= 2.60d-02) then
!             ! mmHg = 80.0d0
!             ! Flrate = -0.133322368d0*mmHg
!           ! else if (rtim > 2.60d-02) then
!             ! FlRate = 0.418033
!           ! end if
!       else
!           flrate = 0
!       end if
!
!       ! to change the pressure slowly in the beginning
!      FlRate = FlRate*min(rtim/100.0d-4,1.0)
!
!
!     ! if (rtim <= 1.60d-02) then
!       ! FlRate = FlRate*min(rtim/100.0d-4,1.0)
!     ! else if (rtim > 1.60d-02 .and. rtim <= 2.60d-02) then
!       ! FlRate = FlRate*(4.15168d-02/rtim-1.5958d-02)
!     ! else
!       ! FlRate = FlRate*min(rtim/820.0d-4,1.0)
!     ! end if
!
!
!     ! the input flowrate is kPa = 10^3Pa = 10^4dyn/cm^2
!     FlRate = FlRate*1.0d4
!
!
!     if (inewt==1 .and. ismaster) write(*,*) "Pressure =", FlRate
!
!
!     !===============================================================
!     ! Begin SHELL analysis
!     !===============================================================
!
!     ! converge geometrical nonlinearity
!     do inewt_SH = 1, SHL%Nnewt(inewt)
!
!       ! Get quantities at alpha levels:
!       ashAlpha = TSP%ashOld + almi*(TSP%ash-TSP%ashOld)
!       ushAlpha = TSP%ushOld + alfi*(TSP%ush-TSP%ushOld)
!       dshAlpha = TSP%dshOld + alfi*(TSP%dsh-TSP%dshOld)
!
!       ! update the deformed shell geometry
!       TSP%B_NET_D(:,1:3) = TSP%B_NET(:,1:3) + dshAlpha(:,:)
!
!       ! get updated positions and normals for the gauss points, to use
!       ! in the contact computation
!       call shell_gp_info_tsp(TSP, BEZ, ushAlpha, ashAlpha, NSD)
!
! ! get a pressure load and put it in the shaft on nacelle and blade tip
! !       TSP%gp%traction = -flrate*(TSP%gp%nor)
!       TSP%gp%traction = -flrate*(TSP%gp%nor)
!
!
!       ! compute the contact forces (stored in TSP%gp)
!       call computeContactForces(tsp,bez,contactGrid,shl,ushAlpha)
!
!       ! come up with a new sparse structure using the contact ien
!
!       if (ismaster) write(*,*) "Generating sparse structure for shell..."
!
!
!       ! allocate and initialize space for shell matrix problem
! !      call shell_allocate_lrhs(NRB,nsd,shl)
!       call shell_allocate_lrhs(TSP,nsd,shl)
!
!       SHL%RHSG     = 0.0d0
!       SHL%RHSG_EXT = 0.0d0
!       SHL%RHSG_GRA = 0.0d0
!       SHL%LHSK     = 0.0d0
!
!       call genSparStruc_shell(TSP%NEL*((TSP%ngauss)**2), TSP%NNODE, &
!           TSP%maxNSHL, TSP%gp%cIEN, &
!           TSP%gp%cNSHL, SHL%col, SHL%row, SHL%icnt)
!
!
!        if (ismaster) write(*,*) "Finished with shell sparse structure..."
!
!
!       call cpu_time(start_time)
!       ! assemble shell eqns
!       call IntElmAss_shell_temp(TSP, BEZ, SHL%icnt, SHL%col, SHL%row, &
!            NSD, SHL%G_fact, SHL%RHSG, SHL%RHSG_GRA, SHL%RHSG_EXT, &
!            SHL%LHSK, ashAlpha, ushAlpha, alfi, beti, almi, gami, Delt)
!
!
!       call cpu_time(end_time)
!       if (ismaster) write(*,*) "Finished with IntElmAss_shell...", end_time - start_time, "s"
!
!       call cpu_time(start_time)
!       ! add contact forces
!       call IntElmAss_shell_contact(tsp,bez, shl%icnt, shl%col, shl%row, &
!            nsd, shl%RHSG_EXT, shl%LHSK, ashAlpha, ushAlpha, &
!            alfi, beti, almi, gami, Delt)
!
!        call cpu_time(end_time)
!        if (ismaster) write(*,*) "Finished with IntElmAss_shell_contact...", end_time - start_time, "s"
!
!       ! (all RHS BCs are accounted for in IntElmAss_shell)
!
!       ! sum different contributions to the shell RHS
!       SHL%RHSG = SHL%RHSG + SHL%RHSG_EXT + SHL%RHSG_GRA
!
!       !=========================================
!       ! Solve Kx = f
!       !=========================================
!       SHL%RHSGNorm = sqrt(sum(SHL%RHSG_EXT(:,1)**2 + &
!                               SHL%RHSG_EXT(:,2)**2 + &
!                               SHL%RHSG_EXT(:,3)**2 ))
!
!         fomt = "(a,x,ES13.6)"
!       if (ismaster)   write(*,fomt) " *) External Force Norm:", SHL%RHSGNorm
!
!       SHL%RHSGNorm = sqrt(sum(SHL%RHSG(:,1)**2 + &
!                               SHL%RHSG(:,2)**2 + &
!                               SHL%RHSG(:,3)**2 ))
!
!
!         fomt = "(I2,a,x,ES13.6)"
!       if (ismaster)   write(*,fomt) inewt_SH, ") Shell Residual Norm Equals:", SHL%RHSGNorm
!
!
! !      if (SHL%RHSGNorm < SHL%RHSGtol) exit
!       if (SHL%RHSGNorm == 1.0d-6) exit
!
!       call cpu_time(start_time)
!       ! Increment for the geometric nonlinearity
!       ashAlpha = 0.0d0
!       call SparseCG_BDIAG_shell(TSP%NNODE, TSP%maxNSHL, NSD, &
!                                 SHL%icnt, SHL%col, SHL%row, &
!                                 SHL%LHSK, SHL%RHSG, &
!                                 ashAlpha, SHL%CGTol, 1)
!
!       call cpu_time(end_time)
!       if (ismaster) write(*,*) "Finished with SparseCG_BDIAG_shell:", end_time - start_time, "s"
!
!       ! free matrix problem memory
! !      call shell_deallocate_lrhs(nrb,nsd,shl)
!       call shell_deallocate_lrhs(TSP,nsd,shl)
!
!       ! update current values for shell unknowns
!       TSP%ash(:,:) = TSP%ash(:,:) + ashAlpha(:,:)
!       TSP%ush(:,:) = TSP%ush(:,:) + ashAlpha(:,:)*gami*Delt
!       TSP%dsh(:,:) = TSP%dsh(:,:) + ashAlpha(:,:)*beti*Delt*Delt
!
!    enddo ! end local newton iteration for shell
!
!    ! (no projection of displacements in immersed method)
!    ! (no mesh solve for static background mesh)
!
! end do    ! end loop of Newton's method
!
! end subroutine solflowGMRES


