!-----------------------------------------------------------------------
! test for symmetry
!-----------------------------------------------------------------------
subroutine testSymmetry(nsd, nshl, xKebe)
  implicit none  
  integer, intent(in)    :: nsd, nshl
  real(8), intent(inout) :: xKebe(NSD*NSD,NSHL,NSHL)
  real(8) :: test(NSD*NSD,NSHL,NSHL)
  integer :: aa, bb, cc

  real(8) :: tol, diff

  tol = 1.0d-17
  
  ! loop through local nodes 
  do aa = 1, NSHL

     do bb = 1, NSHL   ! (X-dir)     
        test(1,aa,bb) = xKebe(1,aa,bb) - xKebe(1,bb,aa)
        test(2,aa,bb) = xKebe(2,aa,bb) - xKebe(4,bb,aa)
        test(3,aa,bb) = xKebe(3,aa,bb) - xKebe(7,bb,aa)
     end do

     do bb = 1, NSHL   ! (Y-dir)     
        test(4,aa,bb) = xKebe(4,aa,bb) - xKebe(2,bb,aa)
        test(5,aa,bb) = xKebe(5,aa,bb) - xKebe(5,bb,aa)
        test(6,aa,bb) = xKebe(6,aa,bb) - xKebe(8,bb,aa)
     end do

     do bb = 1, NSHL   ! (Y-dir)     
        test(7,aa,bb) = xKebe(7,aa,bb) - xKebe(3,bb,aa)
        test(8,aa,bb) = xKebe(8,aa,bb) - xKebe(6,bb,aa)
        test(9,aa,bb) = xKebe(9,aa,bb) - xKebe(9,bb,aa)
     end do

  end do

  do cc=1,9
     do aa=1,nshl
        do bb=1,nshl
           if(abs(test(cc,aa,bb)) > tol) then
              write(*,*) 'asymmetry found!', abs(test(cc,aa,bb))
              stop
           endif
        enddo
     enddo
  enddo

end subroutine testSymmetry


!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
  subroutine IntElmAss_shell(TSP, BEZ, icnt, col, row, nsd, G_fact, &
                             SRHSG_SH, SRHSG_GRA_SH, SRHSG_EXT_SH, &
                             SLHSK_SH, n_info, SRHSG_info, Smg_SH, ashAlpha, ushAlpha, &
                             alfi, beti, almi, gami, Delt, rtim, Mode, contactarea)

  use mpi
  use types_shell
  use commonvar_shell
  use commonvar_contact

  implicit none

  type(mesh), intent(inout) :: TSP, BEZ
  
  integer, intent(in) :: n_info
  
  integer, intent(in) :: icnt, nsd, &
                         col(TSP%NNODE+1), &
                         row(TSP%NNODE*overflow*TSP%maxNSHL)
  real(8), intent(in) :: G_fact(nsd), &
                         ashAlpha(TSP%NNODE,NSD), &
                         ushAlpha(TSP%NNODE,NSD), &
                         alfi, beti, almi, Delt, gami

  real(8) :: fact1, fact2, fact3, dampM, dampK
  
  real(8), intent(out) :: SRHSG_info(TSP%NNODE,n_info), Smg_SH(TSP%NNODE)

  real(8), intent(out) :: SRHSG_SH(TSP%NNODE,NSD), &
                          SRHSG_GRA_SH(TSP%NNODE,NSD),&
                          SRHSG_EXT_SH(TSP%NNODE,NSD),&
                          SLHSK_SH(NSD*NSD,icnt)
  real(8), intent(in) :: rtim
  real(8), intent(in) :: contactarea
  integer, intent(in) :: Mode

  !  Local variables
  
  real(8), allocatable :: RHSG_SH(:,:), &
                          RHSG_GRA_SH(:,:),&
                          RHSG_EXT_SH(:,:),&
                          LHSK_SH(:,:)
						  
  integer :: p, q, nshl, nshb, nuk, nvk, ptype, iel, igauss, jgauss, &
             i, j, ni, nj, aa, bb, igp, Material

  real(8) :: gp(TSP%NGAUSS), gw(TSP%NGAUSS), gwt, da, VVal, &
             DetJb_r, DetJb, nor(NSD), thi, Dm(3,3), Dc(3,3), Db(3,3),&
             xu(NSD), xd(NSD), dxdxi(NSD,2), ddxddxi(nsd,3), &
             dens, bvec(NSD), bscale, cnor(nsd), cforcenorm, kappa, mu, Emod,nu, detJb_a , &
             m(NGauss_TH_Shell,2)

  integer, allocatable :: lIEN(:)
  real(8), allocatable :: shl(:), shgradl(:,:), shhessl(:,:), shlc(:),&
                          Rhs(:,:), Rhs_gra(:,:), Rhs_ext(:,:), Rhsgp(:,:), &
                          xMebe(:,:), xKebe(:,:,:), xKebegp(:,:,:), &
                          xDebe(:,:), &
                          acl(:,:), ul(:,:)

  
  
  real(8), allocatable :: Rhs_info(:,:), RHSG_info(:,:), lmass(:), mg_SH(:), infos(:)

  
  integer :: ifwrite, ifinfo
  
  ifwrite = 0
  ifinfo  = 0
  
  if (ifwrite == 1) then
        open(unit=1, file='direction.dat', status='replace')

        write(1,*) 'TITLE = "direction"'
        write(1,*) 'VARIABLES = "X", "Y", "Z"'
        write(1,*) 'ZONE T="direction", F=POINT'
  end if
  
  if (ifinfo == 1) then
      allocate(RHSG_info(TSP%NNODE, n_info), mg_SH(TSP%NNODE), infos(n_info))
      RHSG_info = 0.0d0
      mg_SH     = 0.0d0
  end if
  
  allocate(RHSG_SH(TSP%NNODE,NSD), &
                            RHSG_GRA_SH(TSP%NNODE,NSD),&
                            RHSG_EXT_SH(TSP%NNODE,NSD),&
                            LHSK_SH(NSD*NSD,icnt))

  RHSG_SH = 0
  RHSG_GRA_SH = 0
  RHSG_EXT_SH = 0
  LHSK_SH = 0
  
  fact1 = almi
  fact2 = alfi*gami*Delt
  fact3 = alfi*beti*Delt*Delt

  ! damping coeff C = (a*M + b*K)
  ! for linear viscoelastic, a should be zero..
  ! only stiffness damping, no mass damping
  if (Mode == 0) then
    dampM = Damping_a + (contactarea/Thickness_Shell)*5.0d2
    ! if (ismaster) write(*,*) "Damping", Damping_a, rtim, dampM, contactarea, Thickness_Shell
  ! dynamic
  else if(Mode == 1) then
    dampM = Damping_a
  end if

  dampK = Damping_b

  ! get Gaussian points and weights
  gp = 0.0d0; gw = 0.0d0
  call genGPandGW_shell(gp, gw, TSP%NGAUSS) 

  ! loop over elements
  do iel = 1, TSP%NEL
	  
	  if (mod(iel, numnodes) == myid) then 
    
    p = BEZ%P(iel); nshl = TSP%NSHL(iel); ptype = TSP%PTYPE(iel)
    q = BEZ%Q(iel); nshb = BEZ%NSHL(iel)
    
      if (ifinfo == 1) allocate(Rhs_info(n_info, nshl), lmass(nshl))
                    
      allocate(shl(nshl), shgradl(nshl,2), shlc(nshl), &
               shhessl(nshl,3), lIEN(nshl), &
               Rhs(NSD,nshl), Rhs_gra(NSD,nshl), Rhs_ext(NSD,nshl), &
               xMebe(nshl,nshl),  &
               xDebe(nshl,nshl),  &
               xKebe(NSD*NSD,nshl,nshl),  &
               xKebegp(NSD*NSD,nshl,nshl), &
               Rhsgp(NSD,nshl), acl(nshl,nsd), ul(nshl,nsd))

      lIEN = -1
      do i = 1, nshl
        lIEN(i) = TSP%IEN(iel,i)
      end do      

      ! Get local solution arrays            
      do i = 1, nshl
        acl(i,:) = ashAlpha(TSP%IEN(iel,i),:)
        ul(i,:)  = ushAlpha(TSP%IEN(iel,i),:)
      end do

      ! initialization 
      xMebe   = 0.0d0     
      xDebe   = 0.0d0
      xKebe   = 0.0d0      ! initialize local stiffness matrix
      Rhs     = 0.0d0      ! initialize local load vector
      Rhs_gra = 0.0d0
      Rhs_ext = 0.0d0
      
      if (ifinfo == 1) then
         Rhs_info = 0.0d0
         lmass    = 0.0d0
      end if
      
      ! counter for gauss points in this element
      igp = 1
	  
      ! Loop over integration points (NGAUSS in each direction) 
      do jgauss = 1, TSP%NGAUSS
        do igauss = 1, TSP%NGAUSS
        
          ! Get Element Shape functions and their gradients
          shl = 0.0d0; shgradl = 0.0d0; shhessl = 0.0d0
          xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0; ddxddxi = 0.0d0
          nor = 0.0d0

!           call eval_SHAPE_shell(gp(igauss), gp(jgauss),  &
!                                 shl, shgradl, shhessl, nor,  &
!                                 xu, xd, dxdxi, ddxddxi,  &
!                                 p, q, nsd, nshl, &
!                                 lIEN, TSP%NNODE, &
!                                 TSP%B_NET_U, TSP%B_NET_D, DetJb_SH, &
!                                 ni, nj, nuk, nvk, &
!                                 TSP%U_KNOT(iel,1:nuk), &
!                                 TSP%V_KNOT(iel,1:nvk))

         call eval_SHAPE_bez_sh(gp(igauss), gp(jgauss), &
                                shl, shgradl, shhessl, nor, &
                                xu, xd, dxdxi, ddxddxi,  &
                                p, q, nsd, nshl, nshb,  &
                                lIEN, TSP%NNODE, &
                                TSP%B_NET_U, TSP%B_NET_D, DetJb_r, DetJb, &
                                BEZ%Ext(iel,1:nshl,1:nshb))


		  Nodes_local((iel-1)*TSP%NGAUSS*TSP%NGAUSS + igp,1:3) = xd(1:3)
		  
		  gwt = gw(igauss)*gw(jgauss)
         
          ! Kirchhoff-Love Shell
          xKebegp = 0.0d0
          Rhsgp   = 0.0d0

          if (ptype == 4) then
            dens = Density_Stent
            thi  = Thickness_Stent
            Emod = E_Stent
            nu   = nu_Stent
			Material = Material_Stent
          else
            dens = Density_Shell 
            thi  = Thickness_Shell
            Emod = E_Shell
            nu   = nu_Shell
			Material = Material_Shell
          end if
          
          kappa = Emod/(3.0d0*(1.0d0-2.0d0*nu))
          mu    = Emod/(2.0d0*(1.0d0+nu))  
          
          !m = m_contrav((iel-1)*TSP%NGAUSS**2+igp,1:2)
          m = m_contrav((iel-1)*TSP%NGAUSS**2+igp,1:NGauss_TH_Shell,1:2)
                  
          call e3LRhs_KLShell(shgradl, shhessl, nshl, NSD, TSP%NNODE, &
					                    lIEN, Material, TSP%B_NET, &
					                    TSP%B_NET_D, mu, kappa, &
					                    thi, NGauss_TH_Shell, xKebegp, Rhsgp, detJb_a,&
										Coef_a, Coef_b, Coef_c, MtCoefs, m)

          xKebe = xKebe + xKebegp*gwt
          Rhs   = Rhs   + Rhsgp*gwt
         
          
          ! end Kirchhoff-Love Shell
          
          !write(*,*) iel, igp, xu, gwt, detJb_a
          
          if (ifinfo == 1) then
             call INFO_Rhs_KLShell(shgradl, shhessl, nshl, NSD, TSP%NNODE, lIEN, &
                             Material, TSP%B_NET, TSP%B_NET_D, mu, kappa, &
                             thi, NGauss_TH_Shell, MtCoefs, m, n_info, infos)

             do aa = 1,nshl
                lmass(aa) = lmass(aa) + shl(aa)*DetJb*gwt
                Rhs_info(:,aa) = Rhs_info(:,aa) + &
                             shl(aa)*infos* DetJb*gwt
             enddo
          end if

          ! Build Mass
          do aa = 1, nshl
            do bb = 1, nshl
              xMebe(aa,bb) = xMebe(aa,bb) + &
                             thi*dens*shl(aa)*shl(bb)*DetJb_r*gwt
            end do
          end do

          ! for damping
          do aa = 1, nshl
            do bb = 1, nshl
                xDebe(aa,bb) = xDebe(aa,bb) + &
                               shl(aa)*shl(bb)*DetJb_r*gwt
              ! xDebe(aa,bb) = xDebe(aa,bb) + &
!                              shl(aa)*shl(bb)*DetJb*gwt
            end do
          end do

!           ! Gravity effect
!           bvec = g_fact*dens*9.81d0
!           do aa = 1, nshl
!              Rhs_gra(:,aa) = Rhs_gra(:,aa) + &
!                   shl(aa)*bvec(:)*thi*DetJb_SH*gwt
!           end do

          ! external traction forces from fluid
          do aa = 1,nshl
             Rhs_ext(:,aa) = Rhs_ext(:,aa) + &
                             shl(aa)*(TSP%gp%traction(iel,igp,:)) &
                             *TSP%gp%gw(iel,igp) &
                             *TSP%gp%detJ(iel,igp)
          enddo
          

          ! linearization of FSI penalty for weak bcs
!          do bb = 1, NSHL
!             do aa = 1, NSHL
!                xDebe(aa,bb) = xDebe(aa,bb) + &
!                     shl(aa)*( &
!                     fact2*2.0*( 1.0d2 ) &  !TODO: use variable
!                     )*shl(bb)*DetJb_SH*gwt
!             end do
!          end do


          ! increment 1d gauss point counter
          igp = igp+1

        end do
      end do  ! end loop gauss points


      ! Dynamic part (RHS)
      do aa = 1, nshl
        do bb = 1, nshl

          Rhs(1,aa) = Rhs(1,aa) - &
               xMebe(aa,bb)*acl(bb,1) - &
               dampM*xDebe(aa,bb)*ul(bb,1) - &
               dampK*sum(xKebe(1:3,aa,bb)*ul(bb,:))

          Rhs(2,aa) = Rhs(2,aa) - &
               xMebe(aa,bb)*acl(bb,2) - &
               dampM*xDebe(aa,bb)*ul(bb,2) - &
               dampK*sum(xKebe(4:6,aa,bb)*ul(bb,:))

          Rhs(3,aa) = Rhs(3,aa) - &
               xMebe(aa,bb)*acl(bb,3) - &
               dampM*xDebe(aa,bb)*ul(bb,3) - &
               dampK*sum(xKebe(7:9,aa,bb)*ul(bb,:))
        end do
      end do

      
      xKebe = fact3*xKebe + fact2*dampK*xKebe
      

      ! Dynamic part (LHS)
      xKebe(1,:,:) = xKebe(1,:,:) + fact1*xMebe(:,:) + fact2*dampM*xDebe(:,:)
      xKebe(5,:,:) = xKebe(5,:,:) + fact1*xMebe(:,:) + fact2*dampM*xDebe(:,:)
      xKebe(9,:,:) = xKebe(9,:,:) + fact1*xMebe(:,:) + fact2*dampM*xDebe(:,:)


       ! fix the frame
!       if (ptype == 4) then
!         do i = 1, nshl
!           TSP%IBC(lIEN(i),:) = 1
!         end do
!       end if

      call BCLhs_3D_shell(nsd, nshl, TSP%NNODE, lIEN, TSP%IBC, xKebe)
      call BCRhs_3D_shell(nsd, nshl, TSP%NNODE, lIEN, TSP%IBC, Rhs)
      call BCRhs_3D_shell(nsd, nshl, TSP%NNODE, lIEN, TSP%IBC, Rhs_gra)       
      call BCRhs_3D_shell(nsd, nshl, TSP%NNODE, lIEN, TSP%IBC, Rhs_ext)
      
      ! do aa = 1, NSHL
!           if (TSP%IBC(lIEN(aa),1) == 1) then
!               Rhs_info(:,aa) = 0.0d0
!           end if
!       end do
      

      ! debug
      !call testSymmetry(nsd, nshl, xKebe)


      ! Assemble load vector         
      ! Assemble thickness and lump mass
      ! LocaltoGlobal_3D is removed..
      do aa = 1, NSHL
        ! internal 
        RHSG_SH(lIEN(aa),:) = RHSG_SH(lIEN(aa),:) + Rhs(:,aa)
        ! gravity force
        RHSG_GRA_SH(lIEN(aa),:) = RHSG_GRA_SH(lIEN(aa),:) + Rhs_gra(:,aa)
        ! external traction
        RHSG_EXT_SH(lIEN(aa),:) = RHSG_EXT_SH(lIEN(aa),:) + Rhs_ext(:,aa)      
        
        ! info
        if (ifinfo == 1) then
           RHSG_info(lIEN(aa),:) = RHSG_info(lIEN(aa),:) + Rhs_info(:,aa)
           mg_SH(lIEN(aa)) = mg_SH(lIEN(aa)) + lmass(aa) 
        end if
      end do

      call FillSparseMat_3D_shell(nsd, nshl, lIEN, TSP%NNODE, &
                                  TSP%maxNSHL, icnt, col, row, &
                                  xKebe, LHSK_SH)
      
      if (ifinfo == 1) deallocate(Rhs_info, lmass)
      
      deallocate(shl, shlc, shgradl, shhessl, Rhs, Rhs_gra, Rhs_ext,  &
                 xMebe, xDebe, xKebe, &
                 xKebegp, Rhsgp, lIEN, acl, ul)
				 
	  end if ! end if (mod(iel, numnodes) == myid)

  end do    ! end loop elements
  
  CALL MPI_ALLREDUCE(RHSG_SH, SRHSG_SH, TSP%NNODE*NSD, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
  CALL MPI_ALLREDUCE(RHSG_GRA_SH, SRHSG_GRA_SH, TSP%NNODE*NSD, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
  CALL MPI_ALLREDUCE(RHSG_EXT_SH, SRHSG_EXT_SH, TSP%NNODE*NSD, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
  CALL MPI_ALLREDUCE(LHSK_SH, SLHSK_SH, NSD*NSD*icnt, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
  
  if (ifinfo == 1) then
      
     CALL MPI_ALLREDUCE(RHSG_info, SRHSG_info, TSP%NNODE*n_info, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
     CALL MPI_ALLREDUCE(mg_SH,     Smg_SH,     TSP%NNODE,        MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
  
     deallocate(RHSG_info, mg_SH, infos)
  end if
  
  deallocate(RHSG_SH, RHSG_GRA_SH, RHSG_EXT_SH, LHSK_SH)
  
  if (ifwrite == 1) then
      close(1)
  end if

  end subroutine IntElmAss_shell
  
  !-----------------------------------------------------------------------
  !
  !-----------------------------------------------------------------------
    subroutine IntElmAss_shell_temp(TSP, BEZ, icnt, col, row, nsd, G_fact, &
                               SRHSG_SH, SRHSG_GRA_SH, SRHSG_EXT_SH, &
                               SLHSK_SH, ashAlpha, ushAlpha, &
                               alfi, beti, almi, gami, Delt)

    use mpi
    use types_shell
    use commonvar_shell
    use commonvar_contact

    implicit none

    type(mesh), intent(inout) :: TSP, BEZ

    integer, intent(in) :: icnt, nsd, &
                           col(TSP%NNODE+1), &
                           row(TSP%NNODE*overflow*TSP%maxNSHL)
    real(8), intent(in) :: G_fact(nsd), &
                           ashAlpha(TSP%NNODE,NSD), &
                           ushAlpha(TSP%NNODE,NSD), &
                           alfi, beti, almi, Delt, gami

    real(8) :: fact1, fact2, fact3, dampM, dampK

    real(8), intent(out) :: SRHSG_SH(TSP%NNODE,NSD), &
                            SRHSG_GRA_SH(TSP%NNODE,NSD),&
                            SRHSG_EXT_SH(TSP%NNODE,NSD),&
                            SLHSK_SH(NSD*NSD,icnt)

    !  Local variables
  
    real(8), allocatable :: RHSG_SH(:,:), &
                            RHSG_GRA_SH(:,:),&
                            RHSG_EXT_SH(:,:),&
                            LHSK_SH(:,:)
						  
    integer :: p, q, nshl, nshb, nuk, nvk, ptype, iel, igauss, jgauss, &
               i, j, ni, nj, aa, bb, igp, Material

    real(8) :: gp(TSP%NGAUSS), gw(TSP%NGAUSS), gwt, da, VVal, &
               DetJb_r, DetJb, nor(NSD), thi, Dm(3,3), Dc(3,3), Db(3,3),&
               xu(NSD), xd(NSD), dxdxi(NSD,2), ddxddxi(nsd,3), &
               dens, bvec(NSD), bscale, cnor(nsd), cforcenorm, kappa, mu, Emod,nu, detJb_a

    integer, allocatable :: lIEN(:)
    real(8), allocatable :: shl(:), shgradl(:,:), shhessl(:,:), shlc(:),&
                            Rhs(:,:), Rhs_gra(:,:), Rhs_ext(:,:), Rhsgp(:,:), &
                            xMebe(:,:), xKebe(:,:,:), xKebegp(:,:,:), &
                            xDebe(:,:), &
                            acl(:,:), ul(:,:)

    allocate(RHSG_SH(TSP%NNODE,NSD), &
                              RHSG_GRA_SH(TSP%NNODE,NSD),&
                              RHSG_EXT_SH(TSP%NNODE,NSD),&
                              LHSK_SH(NSD*NSD,icnt))

    RHSG_SH = 0
    RHSG_GRA_SH = 0
    RHSG_EXT_SH = 0
    LHSK_SH = 0
  
    fact1 = almi
    fact2 = alfi*gami*Delt
    fact3 = alfi*beti*Delt*Delt

    ! damping coeff C = (a*M + b*K)
    ! for linear viscoelastic, a should be zero..
    ! only stiffness damping, no mass damping
    dampM = Damping_a
    dampK = Damping_b

    ! get Gaussian points and weights
    gp = 0.0d0; gw = 0.0d0
    call genGPandGW_shell(gp, gw, TSP%NGAUSS) 

    ! loop over elements
    do iel = 1, TSP%NEL
	  
  	  if (mod(iel, numnodes) == myid) then 
    
      p = BEZ%P(iel); nshl = TSP%NSHL(iel); ptype = TSP%PTYPE(iel)
      q = BEZ%Q(iel); nshb = BEZ%NSHL(iel)
                    
        allocate(shl(nshl), shgradl(nshl,2), shlc(nshl), &
                 shhessl(nshl,3), lIEN(nshl), &
                 Rhs(NSD,nshl), Rhs_gra(NSD,nshl), Rhs_ext(NSD,nshl), &
                 xMebe(nshl,nshl),  &
                 xDebe(nshl,nshl),  &
                 xKebe(NSD*NSD,nshl,nshl),  &
                 xKebegp(NSD*NSD,nshl,nshl), &
                 Rhsgp(NSD,nshl), acl(nshl,nsd), ul(nshl,nsd))

        lIEN = -1
        do i = 1, nshl
          lIEN(i) = TSP%IEN(iel,i)
        end do      

        ! Get local solution arrays            
        do i = 1, nshl
          acl(i,:) = ashAlpha(TSP%IEN(iel,i),:)
          ul(i,:)  = ushAlpha(TSP%IEN(iel,i),:)
        end do

        ! initialization 
        xMebe   = 0.0d0     
        xDebe   = 0.0d0
        xKebe   = 0.0d0      ! initialize local stiffness matrix
        Rhs     = 0.0d0      ! initialize local load vector
        Rhs_gra = 0.0d0
        Rhs_ext = 0.0d0

        ! counter for gauss points in this element
        igp = 1
	  
        ! Loop over integration points (NGAUSS in each direction) 
        do jgauss = 1, TSP%NGAUSS
          do igauss = 1, TSP%NGAUSS
        
            ! Get Element Shape functions and their gradients
            shl = 0.0d0; shgradl = 0.0d0; shhessl = 0.0d0
            xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0; ddxddxi = 0.0d0
            nor = 0.0d0

  !           call eval_SHAPE_shell(gp(igauss), gp(jgauss),  &
  !                                 shl, shgradl, shhessl, nor,  &
  !                                 xu, xd, dxdxi, ddxddxi,  &
  !                                 p, q, nsd, nshl, &
  !                                 lIEN, TSP%NNODE, &
  !                                 TSP%B_NET_U, TSP%B_NET_D, DetJb_SH, &
  !                                 ni, nj, nuk, nvk, &
  !                                 TSP%U_KNOT(iel,1:nuk), &
  !                                 TSP%V_KNOT(iel,1:nvk))

           call eval_SHAPE_bez_sh(gp(igauss), gp(jgauss), &
                                  shl, shgradl, shhessl, nor, &
                                  xu, xd, dxdxi, ddxddxi,  &
                                  p, q, nsd, nshl, nshb,  &
                                  lIEN, TSP%NNODE, &
                                  TSP%B_NET_U, TSP%B_NET_D, DetJb_r, DetJb, &
                                  BEZ%Ext(iel,1:nshl,1:nshb))


  		  Nodes_local((iel-1)*TSP%NGAUSS*TSP%NGAUSS + igp,1:3) = xd(1:3)
		  
  		  gwt = gw(igauss)*gw(jgauss)
         
            ! Kirchhoff-Love Shell
            xKebegp = 0.0d0
            Rhsgp   = 0.0d0

            if (ptype == 4) then
              dens = Density_Stent
              thi  = Thickness_Stent
              Emod = E_Stent
              nu   = nu_Stent
  			Material = Material_Stent
            else
              dens = Density_Shell 
              thi  = Thickness_Shell
              Emod = E_Shell
              nu   = nu_Shell
  			Material = Material_Shell
            end if
          
            kappa = Emod/(3.0d0*(1.0d0-2.0d0*nu))
            mu    = Emod/(2.0d0*(1.0d0+nu))  
                  
            call e3LRhs_KLShell(shgradl, shhessl, nshl, NSD, TSP%NNODE, &
  					                    lIEN, Material, TSP%B_NET, &
  					                    TSP%B_NET_D, mu, kappa, &
  					                    thi, NGauss_TH_Shell, xKebegp, Rhsgp, detJb_a,&
  										Coef_a, Coef_b, Coef_c)

            xKebe = xKebe + xKebegp*gwt
            Rhs   = Rhs   + Rhsgp*gwt
            ! end Kirchhoff-Love Shell
          
 
            ! Build Mass
            do aa = 1, nshl
              do bb = 1, nshl
                xMebe(aa,bb) = xMebe(aa,bb) + &
                               thi*dens*shl(aa)*shl(bb)*DetJb_r*gwt
              end do
            end do

            ! for damping
            do aa = 1, nshl
              do bb = 1, nshl
                  xDebe(aa,bb) = xDebe(aa,bb) + &
                                 shl(aa)*shl(bb)*DetJb_r*gwt
                ! xDebe(aa,bb) = xDebe(aa,bb) + &
  !                              shl(aa)*shl(bb)*DetJb*gwt
              end do
            end do

  !           ! Gravity effect
  !           bvec = g_fact*dens*9.81d0
  !           do aa = 1, nshl
  !              Rhs_gra(:,aa) = Rhs_gra(:,aa) + &
  !                   shl(aa)*bvec(:)*thi*DetJb_SH*gwt
  !           end do

            ! external traction forces from fluid
            do aa = 1,nshl
                Rhs_ext(:,aa) = Rhs_ext(:,aa) + TSP%gp%traction(iel,igp,:)
               !Rhs_ext(:,aa) = Rhs_ext(:,aa) + &
               !                shl(aa)*(TSP%gp%traction(iel,igp,:)) &
                !               *TSP%gp%gw(iel,igp) &
               !                *TSP%gp%detJ(iel,igp)
            enddo
          

            ! linearization of FSI penalty for weak bcs
  !          do bb = 1, NSHL
  !             do aa = 1, NSHL
  !                xDebe(aa,bb) = xDebe(aa,bb) + &
  !                     shl(aa)*( &
  !                     fact2*2.0*( 1.0d2 ) &  !TODO: use variable
  !                     )*shl(bb)*DetJb_SH*gwt
  !             end do
  !          end do


            ! increment 1d gauss point counter
            igp = igp+1

          end do
        end do  ! end loop gauss points


        ! Dynamic part (RHS)
        do aa = 1, nshl
          do bb = 1, nshl

            Rhs(1,aa) = Rhs(1,aa) - &
                 xMebe(aa,bb)*acl(bb,1) - &
                 dampM*xDebe(aa,bb)*ul(bb,1) - &
                 dampK*sum(xKebe(1:3,aa,bb)*ul(bb,:))

            Rhs(2,aa) = Rhs(2,aa) - &
                 xMebe(aa,bb)*acl(bb,2) - &
                 dampM*xDebe(aa,bb)*ul(bb,2) - &
                 dampK*sum(xKebe(4:6,aa,bb)*ul(bb,:))

            Rhs(3,aa) = Rhs(3,aa) - &
                 xMebe(aa,bb)*acl(bb,3) - &
                 dampM*xDebe(aa,bb)*ul(bb,3) - &
                 dampK*sum(xKebe(7:9,aa,bb)*ul(bb,:))
          end do
        end do

      
        xKebe = fact3*xKebe + fact2*dampK*xKebe
      

        ! Dynamic part (LHS)
        xKebe(1,:,:) = xKebe(1,:,:) + fact1*xMebe(:,:) + fact2*dampM*xDebe(:,:)
        xKebe(5,:,:) = xKebe(5,:,:) + fact1*xMebe(:,:) + fact2*dampM*xDebe(:,:)
        xKebe(9,:,:) = xKebe(9,:,:) + fact1*xMebe(:,:) + fact2*dampM*xDebe(:,:)


         ! fix the frame
  !       if (ptype == 4) then
  !         do i = 1, nshl
  !           TSP%IBC(lIEN(i),:) = 1
  !         end do
  !       end if

        call BCLhs_3D_shell(nsd, nshl, TSP%NNODE, lIEN, TSP%IBC, xKebe)
        call BCRhs_3D_shell(nsd, nshl, TSP%NNODE, lIEN, TSP%IBC, Rhs)
        call BCRhs_3D_shell(nsd, nshl, TSP%NNODE, lIEN, TSP%IBC, Rhs_gra)       
        call BCRhs_3D_shell(nsd, nshl, TSP%NNODE, lIEN, TSP%IBC, Rhs_ext)

        ! debug
        !call testSymmetry(nsd, nshl, xKebe)


        ! Assemble load vector         
        ! Assemble thickness and lump mass
        ! LocaltoGlobal_3D is removed..
        do aa = 1, NSHL
          ! internal 
          RHSG_SH(lIEN(aa),:) = RHSG_SH(lIEN(aa),:) + Rhs(:,aa)
          ! gravity force
          RHSG_GRA_SH(lIEN(aa),:) = RHSG_GRA_SH(lIEN(aa),:) + Rhs_gra(:,aa)
          ! external traction
          RHSG_EXT_SH(lIEN(aa),:) = RHSG_EXT_SH(lIEN(aa),:) + Rhs_ext(:,aa)       
        end do

        call FillSparseMat_3D_shell(nsd, nshl, lIEN, TSP%NNODE, &
                                    TSP%maxNSHL, icnt, col, row, &
                                    xKebe, LHSK_SH)
  
        deallocate(shl, shlc, shgradl, shhessl, Rhs, Rhs_gra, Rhs_ext,  &
                   xMebe, xDebe, xKebe, &
                   xKebegp, Rhsgp, lIEN, acl, ul)
				 
  	  end if ! end if (mod(iel, numnodes) == myid)

    end do    ! end loop elements
  
    CALL MPI_ALLREDUCE(RHSG_SH, SRHSG_SH, TSP%NNODE*NSD, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
    CALL MPI_ALLREDUCE(RHSG_GRA_SH, SRHSG_GRA_SH, TSP%NNODE*NSD, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
    CALL MPI_ALLREDUCE(RHSG_EXT_SH, SRHSG_EXT_SH, TSP%NNODE*NSD, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
    CALL MPI_ALLREDUCE(LHSK_SH, SLHSK_SH, NSD*NSD*icnt, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)

    end subroutine IntElmAss_shell_temp


! separate assembly routine for contact
  subroutine IntElmAss_shell_contact(tsp,bez, icnt, col, row, nsd, &
       RHSG_EXT_SH, &
       LHSK_SH, ashAlpha, ushAlpha, &
       alfi, beti, almi, gami, Delt)
  
  use types_shell
  use commonvar_shell
  use commonvar_contact

  implicit none

  type(mesh), intent(inout) :: tsp, bez

  integer, intent(in) :: icnt, nsd, &
       col(tsp%NNODE+1), &
       row(tsp%NNODE*overflow*tsp%maxNSHL)
  real(8), intent(in) :: ashAlpha(tsp%NNODE,NSD), &
       ushAlpha(tsp%NNODE,NSD), &
       alfi, beti, almi, Delt, gami
  
  real(8) :: fact1, fact2, fact3, dampM, dampK

  real(8), intent(out) :: RHSG_EXT_SH(tsp%NNODE,NSD),&
       LHSK_SH(NSD*NSD,icnt)

  !  Local variables
  integer :: p, q, nshl, nuk, nvk, ptype, iel, igauss, jgauss, &
             i, j, ni, nj, aa, bb, igp, gpindex, totalnshl, cp,cq,cnuk,cnvk,&
             cptype,ciel,cni,cnj,cnshl,cnshb,nshb

  real(8) :: gp(tsp%NGAUSS), gw(tsp%NGAUSS), gwt, da, VVal, &
             DetJb_SH_r, DetJb_SH, nor(NSD), thi, Dm(3,3), Dc(3,3), Db(3,3),&
             xu(NSD), xd(NSD), dxdxi(NSD,2), ddxddxi(nsd,3), &
             dens, bvec(NSD), bscale, cnor(nsd), cforcenorm

  integer, allocatable :: lIEN(:),clien(:),biglien(:)
  real(8), allocatable :: shl(:), shgradl(:,:), shhessl(:,:), &
       Rhs_ext(:,:), xCebe(:,:,:), xCebeVel(:,:,:)

  real(8), allocatable :: cshl(:), cshgradl(:,:), cshhessl(:,:), allshl(:)

  fact1 = almi
  fact2 = alfi*gami*Delt
  fact3 = alfi*beti*Delt*Delt

  ! get Gaussian points and weights
  gp = 0.0d0; gw = 0.0d0
  call genGPandGW_shell(gp, gw, tsp%NGAUSS) 
  tsp%iscontact = 0
  tsp%contactarea = 0.0d0
  ! write(*,*) "IntElmAss_shell_contact Debug...", sum(TSP%contactarea)
  ! loop over elements
  do iel = 1, tsp%NEL
    
     p = BEZ%P(iel); nshl = tsp%NSHL(iel); ptype = tsp%PTYPE(iel)
     q = BEZ%Q(iel); nshb = BEZ%NSHL(iel)

     ! counter for gauss points in this element
     igp = 1

     ! Loop over integration points (NGAUSS in each direction) 
     do jgauss = 1, tsp%NGAUSS
        do igauss = 1, tsp%NGAUSS

           ! get a linear index into all gauss points on the shell
           gpindex = (iel-1)*((tsp%ngauss)**2) + igp
           ciel = tsp%gp%celem(gpindex)

           ! if this gauss point is contacting something, add contributions
           if(ciel > 0) then
               do i = 1, tsp%NSHL(iel)
                                tsp%iscontact(tsp%IEN(iel,i)) = 1
               end do
               
!               area_contact = area_contact + tsp%gp%gw(iel,igp) &
!                              *tsp%gp%detJ(iel,igp)

              cp = BEZ%P(iel); cnshl = TSP%NSHL(iel); cptype = TSP%PTYPE(iel)
              cq = BEZ%Q(iel); cnshb = BEZ%NSHL(iel)

              ! need two different arrays of shape functions, one for 
              ! this element, one for the contacting element

              allocate(shl(nshl), shgradl(nshl,2), &
                   shhessl(nshl,3), lIEN(nshl))
              allocate(cshl(cnshl), cshgradl(cnshl,2), &
                   cshhessl(cnshl,3), clIEN(cnshl))
              totalnshl = nshl + cnshl
              allocate(xCebe(NSD*NSD,totalnshl,totalnshl),  &
                   xCebeVel(NSD*NSD,totalnshl,totalnshl), &
                   Rhs_ext(NSD,totalnshl), allshl(totalnshl), &
                   biglien(totalnshl))

              ! have separate lien for each set of functions
              ! (needed to properly evaluate)
              lIEN = -1
              do i = 1, nshl
                 lIEN(i) = tsp%IEN(iel,i)
              end do

              clIEN = -1
              do i = 1, cnshl
                 clIEN(i) = tsp%IEN(ciel,i)
              end do

              ! define combined lien
              biglien = tsp%gp%cien(gpindex,1:totalnshl)

              ! initialization 
              xCebe   = 0.0d0
              xCebeVel   = 0.0d0
              Rhs_ext = 0.0d0

        
              ! Get Element Shape functions and their gradients at both points
              shl = 0.0d0; shgradl = 0.0d0; shhessl = 0.0d0
              xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0; ddxddxi = 0.0d0
              nor = 0.0d0

              call eval_SHAPE_bez_sh(gp(igauss), gp(jgauss), &
                   shl, shgradl, shhessl, nor, &
                   xu, xd, dxdxi, ddxddxi,  &
                   p, q, nsd, nshl, nshb,  &
                   lIEN, TSP%NNODE, &
                   TSP%B_NET_U, TSP%B_NET_D, DetJb_SH_r, DetJb_SH, &
                   BEZ%Ext(iel,1:nshl,1:nshb))

              !call eval_SHAPE_shell(gp(igauss), gp(jgauss),  &
              !     shl, shgradl, shhessl, nor,  &
              !     xu, xd, dxdxi, ddxddxi,  &
              !     p, q, nsd, nshl, &
              !     lIEN, NRB%NNODE, &
              !     NRB%B_NET_U, NRB%B_NET_D, DetJb_SH, &
              !     ni, nj, nuk, nvk, &
              !     NRB%U_KNOT(iel,1:nuk), &
              !     NRB%V_KNOT(iel,1:nvk))

              cshl = 0.0d0; cshgradl = 0.0d0; cshhessl = 0.0d0
              xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0; ddxddxi = 0.0d0
              nor = 0.0d0

              call eval_SHAPE_bez_sh(tsp%gp%cparam(iel,igp,1), &
                   tsp%gp%cparam(iel,igp,2), &
                   cshl, cshgradl, cshhessl, nor, &
                   xu, xd, dxdxi, ddxddxi,  &
                   cp, cq, nsd, cnshl, cnshb,  &
                   clIEN, TSP%NNODE, &
                   TSP%B_NET_U, TSP%B_NET_D, DetJb_SH_r, DetJb_SH, &
                   BEZ%Ext(iel,1:cnshl,1:cnshb))

              !call eval_SHAPE_shell(nrb%gp%cparam(iel,igp,1), &
              !     nrb%gp%cparam(iel,igp,2),  &
              !     cshl, cshgradl, cshhessl, nor,  &
              !     xu, xd, dxdxi, ddxddxi,  &
              !     cp, cq, nsd, cnshl, &
              !     clIEN, NRB%NNODE, &
              !     NRB%B_NET_U, NRB%B_NET_D, DetJb_SH, &
              !     cni, cnj, cnuk, cnvk, &
              !     NRB%U_KNOT(ciel,1:cnuk), &
              !     NRB%V_KNOT(ciel,1:cnvk))
              
              ! negate shape functions for contacting element
              cshl = -cshl

              ! build up master array of shape functions
              do aa = 1,nshl
                 allshl(aa) = shl(aa)
              enddo
              do aa = 1,cnshl
                 allshl(aa+nshl) = cshl(aa)
              enddo

              ! external traction force from contact
              do aa = 1,totalnshl
                 Rhs_ext(:,aa) = &
                  allshl(aa)*(tsp%gp%contactForce(iel,igp,:) &
                  + tsp%gp%contactVelForce(iel,igp,:))
                 ! (already scaled by gw and detJ for contact element)
              enddo

              ! add linearization of contact forces
              do bb = 1, totalNSHL    
                 do aa = 1, totalNSHL
                   
                   ! use lhs computed for gauss point
                   xCebe(:,aa,bb) = allshl(aa)*allshl(bb)&
                        *tsp%gp%contactLhs(iel,igp,:)

                   xCebeVel(:,aa,bb) = &
                        allshl(aa)*allshl(bb)&
                        *tsp%gp%contactLhsVel(iel,igp,:)
                end do
             end do
      
             ! scale different lhs contributions and store in xCebe
             xCebe = fact3*xCebe + fact2*xCebeVel

             call BCLhs_3D_shell(nsd, totalnshl, tsp%NNODE, &
                  biglien, &
                  tsp%IBC, xCebe)
             call BCRhs_3D_shell(nsd, totalnshl, tsp%NNODE, &
                  biglIEN, tsp%IBC, Rhs_ext)

             ! debug
             !call testSymmetry(nsd, nshl, xKebe)

             do aa = 1, totalNSHL
                ! external traction
                RHSG_EXT_SH(biglien(aa),:) = &
                     RHSG_EXT_SH(biglien(aa),:) + Rhs_ext(:,aa)
             end do

             do aa = 1, nshl
                TSP%contactarea(lien(aa)) = &
                     TSP%contactarea(lien(aa)) + shl(aa)*TSP%gp%gw(iel,igp)&
                     *TSP%gp%detJ(iel,igp)
             end do

             call FillSparseMat_3D_shell(nsd, totalnshl, biglien, tsp%NNODE, &
                  tsp%maxNSHL, icnt, col, row, &
                  xCebe, LHSK_SH)
  
             ! deallocate all of the stuff that was allocated per gp
             deallocate(shl, shgradl, &
                  shhessl, lIEN)
             deallocate(cshl, cshgradl, &
                  cshhessl, clIEN)
             deallocate(xCebe,  &
                  xCebeVel, &
                  Rhs_ext, allshl, biglien)

          endif ! end if contacting anything

          ! increment counter regardless
          igp = igp+1

       enddo
    enddo !end loops over gauss points in this element

end do    ! end loop elements

end subroutine IntElmAss_shell_contact
