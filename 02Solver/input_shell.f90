!------------------------------------------------------------------------
! Subroutine to call all the necessary shell input subroutines
!------------------------------------------------------------------------
subroutine input_shell(NSD, SHL, mTSP, mBEZ, NRB, TSP, BEZ)

  use types_shell
  use commonvar_shell
  use mpi
  implicit none

  type(shell),   intent(inout) :: SHL
  type(mesh_mp), intent(out) :: mTSP, mBEZ
  type(mesh),    intent(out) :: NRB, TSP, BEZ

  integer, intent(in) :: NSD

  integer :: so, sf, i, ni, nj, nk, ii, j, k, ip, ier, &
             sumNDZ, sumNEL, nf, &
             loop2, loop3, lnod, eloc, eglob, &
             p, q, mcp, ncp, nshl, nel, nnode, &
             maxP, maxQ, maxMCP, maxNCP, maxNNODE, maxNEL, ngpsurf, &
             maxNSHL, npatch

  character(len=30) :: fname, cname

  ! number of patches 
  npatch = SHL%NPB
  !---------------------------
  ! Preprocessing and Setup
  !---------------------------
  call shell_input_tsp_mp(npatch, NSD, maxP, maxQ, &
                          maxNNODE, TSP%maxNSHL, maxNEL, mTSP, mBEZ)

!   ! remove duplcate nodes
  call reduce_node(NSD, npatch, mTSP%NNODE, mTSP%NEL, &
                   mTSP%B_NET, maxNNODE, mTSP%MAP, &
                   TSP%NNODE, sumNDZ, sumNEL)
    
!   ! number of total elements (non-overlaping)
  TSP%NEL = sum(mTSP%NEL)
  print *, 'total elements number', TSP%NEL
  !================================================================
  ! get the t-spline mesh and bezier extraction
!   call shell_input_tsp(NSD, TSP, BEZ)
  call shell_input_tsp(NSD, npatch, mTSP, mBEZ, TSP, BEZ, maxP, maxQ)
  ! allocate space for gauss points
  TSP%NGAUSS = 4
  
!  call mesh_allocInitGptype(NRB)
  call mesh_allocInitGptype(TSP)

 if (ismaster) then
    write(*,*) "** SHELL ***************************************"
    write(*,*)
    write(*,*) "total T-Spline NNODE = ", TSP%NNODE
    write(*,*) "total T-Spline NEL   = ", TSP%NEL
    write(*,*)
    write(*,*) "TSP%NGAUSS=", TSP%NGAUSS
    write(*,*) "************************************************"
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Write a iga file for TSP
    write(*,*) "All patches data are written in tmesh.0.dat"
    call write_sol_tsp_full(NSD, TSP, BEZ)
 end if
 
end subroutine input_shell
