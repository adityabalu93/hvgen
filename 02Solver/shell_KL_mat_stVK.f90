!======================================================================
! linear elastic material, in curvilinear coordinates.
! full material tensor is computed and plane stress material matrix
! obtained via static condensation.
!======================================================================
subroutine shell_mat_stVK(nshl, G, N, kappa, mu, thi, ngauss, &
                              E_cu, dE_cu, K_cu, dK_cu, &
                              N_cu, dN_cu, M_cu, dM_cu)
  implicit none      

  integer, intent(in) :: nshl, ngauss

  real(8), intent(in) :: G(3,2), N(3), kappa, mu, thi, &
                         E_cu(3), dE_cu(3,3*NSHL), &
                         K_cu(3), dK_cu(3,3*NSHL)

  real(8), intent(out) :: N_cu(3), M_cu(3), dN_cu(3,3*NSHL), &
                          dM_cu(3,3*NSHL)

  real(8) :: lambda, Gtmp(3,3), Gij(3,3), Gconij(3,3), rtmp, &
             C(3,3,3,3), Dtemp(4,4), D(3,3), gp(ngauss), gw(ngauss), &
             zeta, eps(3), deps(3,3*NSHL), sig(3), dsig(3,3*NSHL)

  integer :: i, j, k, l, iz

  lambda = kappa - 2.0d0/3.0d0*mu

  Gij = 0.0d0; C = 0.0d0; D = 0.0d0

  Gtmp(1:3,1:2) = G(1:3,1:2)
  Gtmp(1:3,3)   = N(1:3)

  Gij = matmul(transpose(Gtmp),Gtmp)

  call mat_inverse_3x3(Gij, Gconij, rtmp)

  do i = 1, 3
    do j = 1, 3
      do k = 1, 3
        do l = 1, 3
          C(i,j,k,l) = lambda*Gconij(i,j)*Gconij(k,l) + &
                       mu*(Gconij(i,k)*Gconij(j,l)+ &
                           Gconij(i,l)*Gconij(j,k))
        end do
      end do
    end do
  end do

  Dtemp(1,:) = (/ C(1,1,1,1),C(1,1,2,2),C(1,1,1,2),C(1,1,3,3) /)
  Dtemp(2,:) = (/ C(2,2,1,1),C(2,2,2,2),C(2,2,1,2),C(2,2,3,3) /)
  Dtemp(3,:) = (/ C(1,2,1,1),C(1,2,2,2),C(1,2,1,2),C(1,2,3,3) /)
  Dtemp(4,:) = (/ C(3,3,1,1),C(3,3,2,2),C(3,3,1,2),C(3,3,3,3) /)

  do i = 1, 3
    do j = 1, 3
      D(i,j) = Dtemp(i,j)-Dtemp(i,4)*Dtemp(4,j)/Dtemp(4,4)
    end do
  end do

  N_cu  = 0.0d0
  M_cu  = 0.0d0
  dN_cu = 0.0d0
  dM_cu = 0.0d0

  call genGPandGW_shell(gp, gw, ngauss) 

  ! thickness integration loop
  do iz = 1, ngauss
    zeta  = 0.5d0*thi*GP(iz)

    eps   = E_cu + zeta*K_cu

    deps  = dE_cu + zeta*dK_cu

    sig   = matmul(D,eps)

    dsig  = matmul(D,deps)

    N_cu  = 0.5d0*thi*sig*GW(iz)       + N_cu
    dN_cu = 0.5d0*thi*dsig*GW(iz)      + dN_cu
    M_cu  = 0.5d0*thi*sig*zeta*GW(iz)  + M_cu
    dM_cu = 0.5d0*thi*dsig*zeta*GW(iz) + dM_cu
  end do

end subroutine shell_mat_stVK
