!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!  A subroutine computing the open area
!!!         by Michael C. H Wu
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine Compute_Area (istep, ifq)
	
    use mpi
    use commonvar_shell ! for N_ar and Nodes
	use commonpar ! for pi

    implicit none
	
	integer, intent(in) :: istep
	
	integer :: ifwrite, ifq
	character(len=30) :: fname, fnum(3)
	
	integer :: N, i, j, nf1
	real(8) :: dth, da, t1, t2, tmean, rmin, Area
	real(8), allocatable :: r(:), thi(:)
	
	
	
	ifwrite = 1  ! if you want to output the points
	
	if (mod(istep,ifq) /= 0) ifwrite = 0
	
	if (ifwrite == 1 .and. ismaster) then 
		nf1 = 99
	    write(fnum(2),'(I30)') istep
	    fname = 'Areas.'//trim(adjustl(fnum(2)))
	    open(nf1, file=fname, status='replace')
	end if
	
	allocate(r(N_ar), thi(N_ar))
	
	Nodes_all = 0.0d0
	CALL MPI_ALLREDUCE(Nodes_local, Nodes_all, N_ar*3, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
	
	N = 120
	dth = 2.0*pi/30.0
	da  = 2.0*pi/real(N, 8)
	
	r(:) = sqrt(Nodes_all(:,1)**2 + Nodes_all(:,2)**2) 
	thi(:) = atan2(Nodes_all(:,2), Nodes_all(:,1))
	
	thi(:) = modulo(thi(:), 2.0*pi) ! make "thi" from 0 to 2*pi
	
	Area = 0.0d0
	do i = 1, N
		t1 = (i-1)*da
		t2 = t1 + dth
		tmean = (t1 + t2)*0.5
		
		rmin = 1d8
		do j = 1, N_ar
			if (thi(j) > t1 .and. thi(j) < t2 ) then
				if (r(j) < rmin) rmin = r(j)
			end if
		end do
		
		!if (ismaster) write (*,*) t1, t2, rmin
		
		if (ifwrite == 1 .and. ismaster) then 
			write(nf1,*) rmin, tmean-da*0.5, tmean+da*0.5
		end if
		
		Area = Area + pi*rmin*rmin/real(N, 8)
	end do
	
	if (ismaster) write (*,*) "Area = ", Area
	
	deallocate(r, thi)
	
	
	if (ifwrite == 1 .and. ismaster) then 
	    close(nf1)
	end if
	
end subroutine Compute_Area